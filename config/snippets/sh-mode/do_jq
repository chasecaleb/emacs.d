# -*- mode: snippet -*-
# name: do_jq
# key: do_jq
# --

# jq is a footgun if you don't use several important options. Also its error codes are complicated.
do_jq() {
    # Exit codes 2 and 3 are true "your-jq-command-is-invalid" errors and should propagate, whereas
    # 1 and 4 are expected depending on data, e.g. when a select filter produces no results.
    #
    # From jq man page on --exit-status:
    # Sets the exit status of jq to 0 if the last output values was neither false nor  null,
    # 1  if the last output value was either false or null, or 4 if no valid result was ever
    # produced. Normally jq exits with 2 if there was any usage problem or system  error,  3
    # if there was a jq program compile error, or 0 if the jq program ran.
    if ! jq --raw-output --compact-output --exit-status "$@"; then
        jq_exit=$?
        if [[ "$jq_exit" != 1 && "$jq_exit" != 4 ]]; then
            return "$jq_exit"
        fi
    fi
}
$0