((kubel-log-popup "-f")
 (magit-fetch "--prune")
 (magit-log:magit-log-mode "-n256" "--follow" "--graph" "--color" "--decorate"))
