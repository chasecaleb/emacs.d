;;; -*- lexical-binding: t -*-
(eval-and-compile
  (defconst cc/user-cache (expand-file-name "~/.cache/emacs"))
  (defconst cc/user-source-repo (expand-file-name "~/code/emacs.d"))
  (defconst cc/user-config-repo (expand-file-name "~/code/emacs.d/config")))

(defun cc/user-directories-init ()
  (unless (file-exists-p cc/user-cache)
    (make-directory cc/user-cache t)))

(provide 'cc-user-directories)
