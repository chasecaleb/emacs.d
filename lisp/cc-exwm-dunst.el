;;; -*- lexical-binding: t; byte-compile-warnings: (not docstrings)-*-
;;; Disable docstring warnings because of Hydra.
(require 'hydra)

(defvar cc/dunst--resume-timer nil)
(defun cc/dunst-resume ()
  (interactive)
  (when (timerp cc/dunst--resume-timer)
    (cancel-timer cc/dunst--resume-timer)
    (setq cc/dunst--resume-timer nil))
  (shell-command "dunstctl set-paused false && notify-send --urgency low 'Notifications resumed'"))

(defun cc/dunst-pause-temporarily ()
  (interactive)
  (shell-command  "dunstctl set-paused true")
  ;; Clear existing timer to reset pause duration.
  (when (timerp cc/dunst--resume-timer)
    (cancel-timer cc/dunst--resume-timer))
  (let ((minutes 30))
    (run-with-timer (* minutes 60) nil #'cc/dunst-resume)
    (message "Notifications paused for %s minutes" minutes)))

(defun cc/dunst-status-message ()
  (if (string-match-p "true" (shell-command-to-string "dunstctl is-paused"))
      "Notifications are: PAUSED"
    "Notifications are: on"))

(defhydra cc/dunst-hydra
  nil
  "[%s(cc/dunst-status-message)]"
  ("SPC" (shell-command "dunstctl close") "Dismiss" :column "Action")
  ("C-/" (shell-command "dunstctl history-pop") "Show history")
  ("g" (shell-command "dunstctl action") "Go (default)" :exit t)
  ("M-g" (shell-command "dunstctl context") "Go (menu)" :exit t)
  ("z" #'cc/dunst-resume "Resume" :column "Misc")
  ("M-z" #'cc/dunst-pause-temporarily "Pause")
  ("RET" nil "Quit"))

(provide 'cc-exwm-dunst)
