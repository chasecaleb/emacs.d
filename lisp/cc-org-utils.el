;;; -*- lexical-binding: t -*-
(require 'f)
(require 's)
(require 'org-clock)
(require 'org-element)

;;; General utils
(defmacro cc/when-org-files (&rest body)
  (declare (debug t))
  `(when (f-directory-p (expand-file-name "~/org-files"))
     ,@body))

(defun cc/get-org-files (dir)
  "Return org files in DIR relative to org files root, non-recursively."
  (directory-files (expand-file-name dir "~/org-files") t (rx ".org" string-end)))

(defun cc/org-clock--get-ticket ()
  (when (org-clocking-p)
    (let ((heading-text (save-window-excursion
                          (save-excursion
                            (org-clock-goto)
                            (org-element-property :title (org-element-at-point))))))
      (when (string-match (rx "jira" (+? (not whitespace))  "/browse/"
                              (group (+ alphanumeric) "-" (+ digit)))
                          heading-text)
        (upcase (match-string 1 heading-text))))))

;; Tags
(defconst cc/org-tags-context-alist
  '(("@computer" . ?1)
    ("@reading" . ?2)
    ("@reference" . ?3)
    ("@people" . ?4)
    ("@house" . ?5)
    ("@away" . ?6)))  ; away = outside the house

(defun cc/org-tags-find-context (tags)
  (seq-filter
   (lambda (next)
     (assoc next cc/org-tags-context-alist))
   tags))

;;; Clocking
(defun cc/org-clock-ticket-p ()
  (not (s-blank-p (cc/org-clock--get-ticket))))

(defun cc/org-clock-insert-ticket (&optional suffix)
  "Insert Jira ticket ID from headline of currently clocked task.

Non-nil SUFFIX is inserted after ticket id."
  (interactive)
  (when-let ((ticket (cc/org-clock--get-ticket)))
    (insert ticket)
    (when suffix (insert suffix))))

(defun cc/org-clock-insert-ticket-url ()
  "Like `cc/org-clock-insert-ticket', but insert URL instead of plain ticket ID."
  (interactive)
  (when-let ((ticket (cc/org-clock--get-ticket)))
    (insert "https://jira.naic.org/browse/" ticket)))

(provide 'cc-org-utils)
