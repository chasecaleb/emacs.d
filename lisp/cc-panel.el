;;; -*- lexical-binding: t -*-
;; Inspired by/some parts modified based on:
;; https://github.com/daviwil/dotfiles/blob/master/Desktop.org#panel
;;
;; Polybar ref: https://github.com/polybar/polybar/wiki/Module:-ipc
(require 'exwm-workspace)
(require 'org-clock)
(require 'spaceline)

(defvar cc/panel--process nil)
(defvar cc/panel--update-time-timer nil)

(defconst cc/panel-update-hooks '(exwm-workspace-switch-hook
                                  org-clock-in-hook
                                  org-clock-out-hook
                                  org-clock-cancel-hook
                                  tracking-buffer-added-hook
                                  tracking-buffer-removed-hook
                                  tracking-mode-hook)
  "Hooks that should trigger a panel update")

(defun cc/panel--kill ()
  (when (and cc/panel--process
             (process-live-p cc/panel--process))
    (kill-process cc/panel--process))
  (setq cc/panel--process nil))

(defun cc/panel--start ()
  (cc/panel--kill)
  (setq cc/panel--process
        (start-process-shell-command "polybar" "*polybar*"
                                     "polybar panel")))

(defun cc/panel--update-time ()
  "Some modules need to be updated periodically due to showing e.g. org clock time"
  (start-process-shell-command "polybar-msg" nil "polybar-msg hook emacs-org 1"))

(defun cc/panel--update ()
  ;; This gross concatenation with sleeps is because of a polybar race condition, which I reported:
  ;;     https://github.com/polybar/polybar/issues/2504
  ;; Hopefully this gets fixed so I can remove the sleep statements.
  (let ((cmd (string-join (seq-map (lambda (module)
                                     (format "polybar-msg hook %s 1; sleep 0.5" module))
                                   '("emacs-workspace" "emacs-slack" "emacs-irc" "emacs-org"))
                          "; ")))
    (start-process-shell-command "polybar-msg" nil cmd)))

(defun cc/panel-data-workspace ()
  (format "EXWM: %s" exwm-workspace-current-index))

(defun cc/panel--tracking-buffer-is-slack-p (name)
  (string-match-p "Slack" name))

(defun cc/panel-data-slack ()
  (if (bound-and-true-p tracking-buffers) ; Slack (and thus Circe) isn't always loaded.
      (if-let* ((slack-tracking (seq-filter #'cc/panel--tracking-buffer-is-slack-p tracking-buffers))
                (names (seq-map
                        (lambda (name)
                          (thread-last name
                                       (replace-regexp-in-string (rx line-start "*Slack - " (*? any) " : ") "")
                                       ;; Use "[foo]" as thread indicator.
                                       (replace-regexp-in-string (rx (group (* any)) " Thread - " (+ (any "." digit))) "[\\1")
                                       ((lambda (it) (truncate-string-to-width it 25)))
                                       (replace-regexp-in-string (rx "[" (group (* any))) "[\\1]")))
                        slack-tracking)))
          (format "[%s] Slack: %s" (length slack-tracking) (string-join names ", "))
        "")
    ""))

(defun cc/panel-data-irc ()
  (if (bound-and-true-p tracking-buffers) ; Slack (and thus Circe) isn't always loaded.
      (if-let* ((irc-tracking (seq-filter (lambda (it)
                                            (not (cc/panel--tracking-buffer-is-slack-p it)))
                                          tracking-buffers))
                (names (seq-map
                        (lambda (name)
                          (replace-regexp-in-string (rx "@" (1+ (not "@")) line-end) "" name))
                        irc-tracking)))
          (format "[%s] IRC: %s" (length irc-tracking) (string-join names ", "))
        "")
    ""))

(defun cc/panel-data-org ()
  "Org clock data.

Includes color format tags, since they're dynamic depending on value."
  (let* ((face (if (org-clocking-p)
                   'powerline-active1
                 'spaceline-highlight-face))
         (fg (cc/panel--color-to-rgb-hex (face-attribute face :foreground)))
         (bg (cc/panel--color-to-rgb-hex (face-attribute face :background)))
         (text (if (org-clocking-p)
                   (string-trim (substring-no-properties (org-clock-get-clock-string)))
                 "No clock")))
    (format "%%{F%s}%%{B%s} %s %%{F-}%%{B-}" fg bg text)))

(defun cc/panel--color-to-rgb-hex (color)
  "Convert an Emacs color name to CSS-style hex (e.g #ffffff).

Also handles RGB hex as input, in which case this is effectively an identity
function.

P.S. Emacs, how do you not have this as a utility function already?"
  (concat "#" (string-join
               (seq-map (lambda (value)
                          (format "%02x" (round (/ value (/ 65536 255)))))
                        (color-values color)))))

(defun cc/panel-enable ()
  (interactive)
  (cc/panel--start)
  (seq-do
   (lambda (hook)
     (add-hook hook #'cc/panel--update))
   cc/panel-update-hooks)
  (when (timerp cc/panel--update-time-timer)
    (cancel-timer cc/panel--update-time-timer))
  (setq cc/panel--update-time-timer (run-with-timer 60 60 #'cc/panel--update-time)))

(provide 'cc-panel)
