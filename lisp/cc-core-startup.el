;;; -*- lexical-binding: t -*-
;;; Core startup-related functionality.

(defvar last-log-time before-init-time)
(defun cc/log-init-time (msg)
  (let ((now (current-time))
        (elapsed (float-time
                  (time-subtract (current-time) before-init-time)))
        (delta   (float-time
                  (time-subtract (current-time) last-log-time))))
    (message "[init][%06.3fs total, +%05.3fs delta] %s"
             elapsed delta msg)
    (setq last-log-time now)))

(defmacro cc/log-require (feature)
  `(progn
     (cc/log-init-time (format "Feature loading:    %s" ,feature))
     (require ,feature)
     (cc/log-init-time (format "Feature finished:   %s" ,feature))))

(defun cc/log-fn (fn &rest args)
  (cc/log-init-time (format    "Function executing: %s" fn))
  (apply fn args)
  (cc/log-init-time (format    "Function finished:  %s" fn)))

;; - One of the more rational posts about Emacs GC tuning:
;;   http://bling.github.io/blog/2016/01/18/why-are-you-changing-gc-cons-threshold/
;; - Also hlissner's (Doom maintainer) advice on size:
;;   https://github.com/hlissner/doom-emacs/issues/3108#issuecomment-627537230
;;
;; TLDR: threshold is a balance between stop-the-world pauses (too large) and constant stuttering
;; (too small).
(defvar cc/gc-cons-threshold-normal (* 16 1024 1024))
(defun cc/gc-increase ()
  (setq gc-cons-threshold most-positive-fixnum))
(defun cc/gc-restore ()
  (setq gc-cons-threshold cc/gc-cons-threshold-normal))

(defun cc/core-startup-init ()
  ;; Increase temporarily during init.
  (cc/gc-increase)
  (add-hook 'after-init-hook #'cc/gc-restore)

  (progn ; Startup/GUI things.
    (setq inhibit-startup-buffer-menu t)
    (setq inhibit-startup-screen t)
    (setq inhibit-compacting-font-caches t)
    (setq initial-buffer-choice t)
    (setq initial-scratch-message "")
    (scroll-bar-mode 0)
    (horizontal-scroll-bar-mode 0)
    (tool-bar-mode 0)
    (menu-bar-mode 0)))

(provide 'cc-core-startup)
