;;; -*- lexical-binding: t -*-
;;; Entry point to my config/init files.

(defconst cc/exwm-enabled (string= (getenv "ENABLE_EXWM") "true"))
;; unset env to prevent nested emacs instances from starting EXWM accidentally.
(setenv "ENABLE_EXWM")

(require 'cc-core-startup)
(cc/log-init-time "Started Emacs")
(cc/log-fn #'cc/core-startup-init)
(cc/log-require 'cc-nix)
(cc/log-fn #'cc/nix-init)

(let* ((cmd "cc-init-git-repos")
       (exit (call-process-shell-command cmd nil (get-buffer-create "*cc-init-git-repos*"))))
  (unless (zerop exit)
    (error "Command failed with exit code %s: %s" exit cmd)))

(cc/log-require 'cc-user-directories)
(cc/log-fn #'cc/user-directories-init)
(cc/log-require 'cc-auth)
(cc/log-fn #'cc/auth-init)
(cc/log-require 'cc-appearance)
(cc/log-fn #'cc/appearance-init)

(when cc/exwm-enabled
  (cc/log-require 'cc-exwm)
  (declare-function cc/exwm-init "cc-exwm")
  (cc/log-fn #'cc/exwm-init)

  (declare-function cc/git-sync-setup "cc-git-sync")
  (cc/log-require 'cc-git-sync)
  (cc/log-fn #'cc/git-sync-setup))

(cc/log-require 'cc-kitchen-sink)
(cc/log-require 'cc-completions)
(cc/log-fn #'cc/completions-init)
(cc/log-require 'cc-source-code)
(cc/log-fn #'cc/source-code-init)
(cc/log-require 'cc-lisp-mode)
(cc/log-fn #'cc/lisp-mode-init)
(cc/log-require 'cc-vterm)
(cc/log-fn #'cc/vterm-init)

;; Avoid loading slack config until necessary, because slack is huge and I frequently don't need it.
(defvar cc/slack-keymap) ;; Autoloaded in cc-slack.el
(global-set-key (kbd "s-m") cc/slack-keymap)

;; Native comp causes `org-offer-links-in-entry' to always pick first link from list if called from
;; start of a headline with multiple links. Sounds insane, but seriously.
;;
;; Proof I'm not the only one:
;; - https://debbugs.gnu.org/cgi/bugreport.cgi?bug=51382
;; - https://old.reddit.com/r/emacs/comments/qf7o8t/orgofferlinksinentry_is_incompatible_with/
(cc/csetq native-comp-deferred-compilation-deny-list (list (rx "/org.el" string-end)))

(cc/log-init-time (concat "Finished loading user init: " user-init-file))
(add-hook 'after-init-hook
          (lambda ()
            (cc/log-init-time "Finished after-init-hook"))
          50)

(provide 'cc-init)
