;;; -*- lexical-binding: t -*-
;;; Completion configuration. I used to use Helm, which was nice for its batteries-included
;;; functionality, but over time I've become less satisfied with its inconsistencies and how
;;; difficult it is to extend.

(require 'frame)
(require 'mode-local)
(require 'vertico)
(require 'vertico-repeat)
(require 'orderless)
(require 'marginalia)
(require 'embark)
(require 'embark-consult)
(require 'consult)
(require 'consult-projectile)
(require 'consult-yasnippet)
(require 'projectile)
(require 'corfu)
(require 'corfu-doc)
(require 'kind-icon)
(require 'svg-lib) ;; Used by kind-icon
(require 'xref)
(require 'wgrep) ;; Weird place to load this, but it's useful for consult grep -> embark export.
(require 'cc-utils)
(require 'cc-user-directories)
(require 'cc-vterm)
(require 'cc-completions-consult-sources)

(defun cc/completions--keybinds ()
  (cc/define-key-list
   global-map
   `(;; Embark
     ("C-."                      . ,#'embark-act)
     ("M-."                      . ,#'embark-dwim)
     ("C-M-."                    . ,#'embark-become)
     ([remap describe-bindings]  . ,#'embark-bindings)
     ;; File/buffer switching
     ("C-; r"                    . ,#'vertico-repeat-last)
     ("C-; C-r"                  . ,#'vertico-repeat-select)
     ("C-; f"                    . ,#'find-file)
     ("C-; C-f"                  . ,#'consult-find)
     ("C-; s"                    . ,#'consult-buffer)
     ("C-; C-s"                  . ,#'consult-projectile)
     ("C-; p"                    . ,#'consult-projectile-switch-project)
     ;; Searching and navigation
     ("C-; g"                    . ,#'consult-ripgrep)
     ("C-; i"                    . ,#'consult-imenu)
     ("C-; C-i"                  . ,#'consult-imenu-multi)
     ("C-; o"                    . ,#'cc/completions-smart-outline)
     ("M-s o"                    . ,#'consult-line)
     ("M-s M-o"                  . ,#'consult-line-multi)
     ("C-; SPC"                  . ,#'consult-mark)
     ("C-; C-SPC"                . ,#'consult-global-mark)
     ("C-; e"                    . ,#'consult-flycheck)
     ;; Registers and bookmarks
     ("C-; x x"                  . ,#'consult-register)
     ("C-; x l"                  . ,#'consult-register-load)
     ("C-; x s"                  . ,#'consult-register-store)
     ("C-; m"                    . ,#'consult-bookmark)
     ;; Other
     ("M-y"                      . ,#'consult-yank-pop)
     ("C-; k"                    . ,#'consult-kmacro)
     ("C-; a"                    . ,#'consult-apropos)
     ("C-; h"                    . ,#'man)
     ("C-; M-h"                  . ,#'consult-man)
     ("C-; M-x"                  . ,#'consult-mode-command)
     ("C-; y"                    . ,#'consult-yasnippet)
     ;; Miscellaneous built-in command overrides
     ("C-x M-:"                  . ,#'consult-complex-command)
     ("C-x r b"                  . ,#'consult-bookmark))))

(defun cc/completions-init ()
  (cc/completions--corfu)
  (cc/completions--vertico)
  (cc/completions--orderless)
  (cc/completions--marginalia)
  (cc/completions--embark)
  (cc/completions--projectile)
  (cc/completions--consult)
  (cc/completions--yasnippet)
  (cc/completions--keybinds))

(defun cc/completions--corfu-exwm-frame-hack (fn &rest args)
  "Incredibly gross hack to make corfu completions show up over EXWM buffers.

Primary scenarios where this is relevant:
1. When showing completions in a narrow enough window that they spill over onto
   a neighboring EXWM window.
2. When showing completions in minibuffer and the left-most window is EXWM.

CAVEAT: This *should* handle multiple displays, but I only use a single display
currently so this is untested.

See:
- https://old.reddit.com/r/emacs/comments/dexuab/exwm_childframes/
- https://github.com/ch11ng/exwm/issues/550"
  (let* ((parent (frame-parameter corfu--frame 'parent-frame))
         (parent-top (cdr (assoc 'top (frame-parameters parent)))))
    ;; Promoting from a child to a parent frame makes it necessary to offset position due to my
    ;; polybar panel at top of screen.
    (setf (nth 1 args) (+ parent-top (nth 1 args))))
  ;; Corfu recreates the frame if the parent doesn't match what it expects, which means it will
  ;; try to recreate the frame every single call due to my frame un-parenting hack. Doing so
  ;; performs horribly and flickers, so block it.
  (if (and (frame-live-p corfu--frame)
           ;; This *should* make corfu work when switching between frames on different displays, but
           ;; I haven't tested to verify and don't care to right now.
           (equal (cdr (assoc 'display (frame-parameters corfu--frame)))
                  (cdr (assoc 'display (frame-parameters (selected-frame))))))
      ;; Make `delete-frame' and `make-frame' into no-ops
      (cc/with-advice #'delete-frame :around (lambda (_delete-fn &rest _delete-args))
        (cc/with-advice #'make-frame :around (lambda (_make-fn &rest _make-args)
                                               corfu--frame)
          (apply fn args)))
    (apply fn args)
    ;; Removing parent frame makes corfu show up over EXWM buffers.
    (set-frame-parameter corfu--frame 'parent-frame nil)))


(defvar cc/exwm-enabled) ;; Defined in cc-init.el

;; Useful `corfu-mode' keybinds:
;;   - M-h to show help doc
;;   - M-g to show location in temporary window
(defun cc/completions--corfu ()
  (when cc/exwm-enabled
    ;; This hack breaks horribly in a nested Emacs... but that's ok, it isn't needed there.
    (advice-add #'corfu--make-frame :around #'cc/completions--corfu-exwm-frame-hack)
    (when (frame-live-p corfu--frame)
      ;; Just in case corfu was triggered before this loaded.
      (delete-frame corfu--frame)))
  (add-hook 'minibuffer-setup-hook #'cc/completions--corfu-in-minibuffer 1)
  (setq corfu-count 20)
  (setq corfu-scroll-margin 5)
  (setq corfu-cycle t)
  (setq corfu-echo-documentation t)
  ;; `corfu-echo' inherits from `italic' and `shadow' by default, but `shadow' is hard to read.
  (set-face-attribute 'corfu-echo nil :inherit '(italic))

  ;; Orderless-style auto completion
  (define-key corfu-map (kbd "M-SPC") #'corfu-insert-separator)
  ;; Don't want Corfu to interfere with next/previous line since completions show automatically.
  (define-key corfu-map [remap next-line] nil)
  (define-key corfu-map [remap previous-line] nil)
  (setq corfu-auto t) ;; Can also trigger manually with M-TAB.
  (setq corfu-auto-delay 0.05)

  (add-hook 'corfu-mode-hook #'corfu-doc-mode)
  (define-key corfu-map (kbd "M-d") #'corfu-doc-toggle)
  (define-key corfu-map (kbd "C-M-n") #'corfu-doc-scroll-up)
  (define-key corfu-map (kbd "C-M-p") #'corfu-doc-scroll-down)
  (setq corfu-doc-auto nil)
  (setq corfu-doc-max-width 80)
  (setq corfu-doc-max-height 20)

  (setq svg-lib-icons-dir (expand-file-name "svg-lib" cc/user-cache))
  (cc/csetq kind-icon-default-face 'corfu-default)
  (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter)

  (add-hook 'cc/source-code-hook #'corfu-mode))

(defun cc/completions--corfu-in-minibuffer ()
  "Enable Corfu in the minibuffer if Vertico/Mct are not active, e.g. for M-:."
  (unless (or (bound-and-true-p mct--active)
              (bound-and-true-p vertico--input))
    (corfu-mode 1)))

(defun cc/completions--vertico ()
  (vertico-mode)
  (setq vertico-count 25)
  (setq vertico-cycle t)
  ;; vertico-repeat extension
  (add-hook 'minibuffer-setup-hook #'vertico-repeat-save)
  ;; vertico-directory extension
  (cc/define-key-list vertico-map
                      `(("RET" . ,#'vertico-directory-enter)
                        ("M-d" . ,#'vertico-directory-delete-word)
                        ("M-<backspace>" . ,#'vertico-directory-delete-word)))
  (add-hook 'rfn-eshadow-update-overlay-hook #'vertico-directory-tidy)
  ;; vertico-quick extension (avy-like selection)
  (cc/define-key-list vertico-map
                      `(("M-g g" . ,#'vertico-quick-insert)
                        ("M-g M-g" . ,#'vertico-quick-insert))))

(defun cc/completions--orderless ()
  (setq completion-styles '(orderless))
  (setq completion-category-defaults nil)
  (setq completion-category-overrides
        ;; partial-completion on files is amazing, because "/r/c/sw/sh/e/s" or even "/r/c/s/s/e/s"
        ;; can complete "/run/current-system/sw/share/emacs/site-lisp/".
        '((file (styles . (partial-completion orderless)))
          (project-file (styles . (partial-completion orderless)))))

  (setq orderless-matching-styles '(orderless-prefixes
                                    orderless-regexp
                                    orderless-initialism))
  (setq orderless-style-dispatchers '(cc/completions--orderless-dispatcher-literal
                                      cc/completions--orderless-dispatcher-not
                                      cc/completions--orderless-dispatcher-initialism
                                      cc/completions--orderless-dispatcher-flex)))

(defun cc/completions--orderless-dispatcher-literal (pattern _index _total)
  (when (string-suffix-p "=" pattern)
    `(orderless-literal . ,(substring pattern 0 -1))))

(defun cc/completions--orderless-dispatcher-not (pattern _index _total)
  (cond
   ((equal "!" pattern)
    '(orderless-literal . ""))
   ((string-prefix-p "!" pattern)
    `(orderless-without-literal . ,(substring pattern 1)))))

(defun cc/completions--orderless-dispatcher-initialism (pattern _index _total)
  (when (string-suffix-p "," pattern)
    `(orderless-initialism . ,(substring pattern 0 -1))))

(defun cc/completions--orderless-dispatcher-flex (pattern _index _total)
  (when (string-suffix-p "~" pattern)
    `(orderless-flex . ,(substring pattern 0 -1))))

(defun cc/completions--marginalia ()
  (marginalia-mode)
  (define-key vertico-map (kbd "M-A") #'marginalia-cycle)
  ;; `marginalia-field-width' is an upper limit, which will be lowered on the fly based on window
  ;; width. So basically set something high here and then let marginalia adjust it to fit.
  (setq marginalia-field-width 500))

(defun cc/completions--embark ()
  (define-key embark-file-map "$" #'cc/vterm-for-file)
  (define-key embark-file-map "l" #'cc/embark-org-link-file)
  (define-key embark-buffer-map "l" #'cc/embark-org-link-buffer)
  (define-key embark-general-map ".f" #'cc/embark-firefox)
  (define-key embark-general-map ".s" #'cc/embark-firefox-search)
  ;; `embark-consult-search-map' is prefixed to C for other embark maps, so git grep easy.
  (define-key embark-consult-search-map "C" #'consult-git-grep)
  ;; Warning: `embark-prefix-help-command' doesn't work for complex menus like `lsp-mode' uses, but
  ;; I still want this because it's helpful with e.g. my slack keymaps. Hopefully this will be
  ;; improved one day, or else maybe I should work on it. See:
  ;; https://github.com/oantolin/embark/issues/383
  (setq prefix-help-command #'embark-prefix-help-command)
  (setq embark-quit-after-action '((kill-buffer . nil)
                                   (describe-symbol . nil)
                                   (t . t)))
  ;; Remove some overly-cautious nuisance confirmation prompts.
  ;; This is obviously not the most efficient solution to remove elements from a list, but it works
  ;; fine for a couple elements.
  (dolist (elt '((kill-buffer embark--confirm)
                 (embark-kill-buffer-and-window embark--confirm)))
    (setq embark-pre-action-hooks (delete elt embark-pre-action-hooks))))

(defun cc/embark-org-link-file (file)
  "Store link to file with `org-store-link'"
  (cc/embark-org-link-buffer (find-file-noselect file)))

(defun cc/embark-org-link-buffer (buffer)
  "Store link for buffer with `org-store-link'."
  (with-current-buffer buffer
    (save-excursion
      (goto-char 0) ;; Make org file links refer to the file, not wherever point happens to be.
      (org-store-link '(4) t))))

(defun cc/embark-firefox (_input)
  "Create new Firefox buffer."
  (start-process-shell-command "firefox" nil "firefox --new-window"))

(defun cc/embark-firefox-search (input)
  "New web search."
  (let ((cmd (format "firefox --search %s" (shell-quote-argument input))))
    (start-process-shell-command "firefox" nil cmd)))

(defun cc/completions--consult ()
  (cc/completions-consult-sources-init)
  (setq xref-show-xrefs-function #'consult-xref)
  (setq xref-show-definitions-function #'consult-xref)
  (setq xref-marker-ring-length 50)
  (setq register-preview-delay 0.01)
  (setq register-preview-function #'consult-register-format)
  (setq consult-narrow-key "<")
  (define-key consult-narrow-map (vconcat consult-narrow-key "?") #'consult-narrow-help)
  (setq consult-project-function (lambda (_) (projectile-project-root)))
  (advice-add #'completing-read-multiple :override #'consult-completing-read-multiple)
  ;; Advise `consult--multi' in order to fix `consult-buffer' and similar without interfering with
  ;; `consult-grep'.
  (advice-add #'consult--multi :around #'cc/completions--consult-multi-fix)
  (advice-add #'consult-yank-pop :around #'cc/completions--yank-pop-advice)
  ;; Adjust preview behavior for "expensive" (i.e. file-opening) commands.
  (consult-customize
   consult-ripgrep
   consult-git-grep
   consult-grep
   consult-xref
   :preview-key (list (kbd "M-.")
                      :debounce 0.2 'any))
  (consult-customize
   consult-bookmark
   consult--source-bookmark
   consult--source-recent-file
   consult--source-project-recent-file
   consult-recent-file
   :preview-key (kbd "M-.")))

(defun cc/completions--consult-multi-fix (fn &rest args)
  "Kludge to stop EXWM buffers from stealing focus during previews."
  (cc/with-advice
      #'consult--buffer-action :around
      (lambda (action-fn &rest action-args)
        (let ((initial major-mode))
          (apply action-fn action-args)
          (when (provided-mode-derived-p initial 'exwm-mode)
            (when-let ((mini (active-minibuffer-window)))
              (select-window (active-minibuffer-window))))))
    (apply fn args)))

(defun cc/completions-smart-outline ()
  "Intelligently calls either `consult-outline' or mode-specific equivalents."
  (interactive)
  (pcase major-mode
    ('org-mode
     (consult-org-heading))
    ('org-agenda-mode
     (consult-org-agenda))
    (_
     (consult-outline))))

(defun cc/completions--yank-pop-advice (fn &rest args)
  "Wrapper around `consult-yank-pop' to handle vterm and EXWM quirks."
  ;; Lesser-known advice use: adding an interactive spec will override the spec of the advised
  ;; function. In this case, need to modify it from "*p" in order to allow use in readonly buffers
  ;; like EXWM and vterm.
  (interactive "p")
  (pcase major-mode
    ('vterm-mode
     (let ((inhibit-read-only t)
           (yank-undo-function (lambda (_start _end) (vterm-undo))))
       (cl-letf (((symbol-function 'insert-for-yank)
                  (lambda (str) (vterm-send-string str t))))
         (apply fn args))))
    ('exwm-mode
     ;; I don't want to (require 'exwm) since I don't use it in child Emacs processes, so do this
     ;; instead to make compiler happy.
     (when (and (fboundp 'exwm-input--set-focus)
                (fboundp 'exwm--buffer->id)
                (fboundp 'exwm-input--fake-key))
       (let ((inhibit-read-only t))
         ;; Disgusting hack to send selection to X11 clipboard, since consult doesn't seem to do
         ;; this automatically (whereas Helm did, surprisingly).
         (cc/with-advice #'insert-for-yank :before
                         (lambda (&rest insert-args)
                           (if-let ((contents (car insert-args)))
                               (gui-select-text contents)))
           ;; Cycling through kill ring in EXWM buffers isn't a thing, so always show selection menu
           ;; instead. Alternatively I could bind `yank-undo-function' to send a simulated "C-z"
           ;; undo, but I don't want to deal with potential edge cases or add complexity.
           ;; Also, for whatever reason this doesn't seem to behave with `funcall-interactively'...
           ;; so I copied the interactive spec into args here.
           (consult-yank-from-kill-ring (consult--read-from-kill-ring) current-prefix-arg))
         ;; https://github.com/ch11ng/exwm/issues/413#issuecomment-386858496
         (exwm-input--set-focus (exwm--buffer->id (window-buffer (selected-window))))
         (exwm-input--fake-key ?\C-v))))
    (_ (apply fn args))))

(defun cc/completions--yasnippet ()
  (diminish 'yas-minor-mode)
  ;; Use config in repo instead of system location because I want to modify these on the fly easily.
  (setq yas-snippet-dirs `(,(expand-file-name "snippets" cc/user-config-repo)))
  (setq yas-indent-line 'fixed)
  (yas-global-mode t))

(defun cc/completions--projectile ()
  (diminish 'projectile-mode)
  (setq projectile-ignored-project-function
        (lambda (project-dir)
          ;; Non-nil return means ignore.
          (file-in-directory-p project-dir "/nix/store/")))
  ;; Yes, ignored modes list uses string instead of symbols to support regexp I guess.
  (add-to-list 'projectile-globally-ignored-modes "exwm-mode")
  (add-to-list 'projectile-globally-ignored-modes "slack-mode")
  (add-to-list 'projectile-globally-ignored-modes "slack-.*-mode")
  (add-to-list 'projectile-globally-ignored-modes "org-agenda-mode")
  (setq projectile-known-projects-file (expand-file-name "projectile-bookmarks.eld" cc/user-cache))
  (setq projectile-cache-file (expand-file-name "projectile.cache" cc/user-cache))

  ;; Check if dirs exist to avoid breakage, e.g. while building in Gitlab pipeline.
  (setq projectile-project-search-path '())
  (dolist (dir '("~/code" "~/nipr"))
    (when (file-exists-p dir)
      (add-to-list 'projectile-project-search-path dir)))

  (setq projectile-use-git-grep t)
  ;; Treat submodules as their own projects instead of recursing into them.
  (setq projectile-git-submodule-command nil)
  (projectile-mode)

  ;; `consult-projectile'
  (add-to-list 'consult-projectile-sources consult-projectile--source-projectile-recentf)
  (embark-define-keymap embark-projectile-map
    "Consult-projectile project actions."
    ("g" cc/magit-status)
    ("$" cc/vterm-for-file))
  (add-to-list 'embark-keymap-alist '(consult-projectile-project . embark-projectile-map)))

(defun cc/magit-status (project-dir)
  "Show magit status for PROJECT-DIR."
  (let ((default-directory project-dir))
    (magit-status)))

(provide 'cc-completions)
