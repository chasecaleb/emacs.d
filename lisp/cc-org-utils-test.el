;;; -*- lexical-binding: t -*-
(require 'buttercup)
(require 'cc-org-utils)

(describe (symbol-name #'cc/org-tags-find-context)
  (it "Finds context tag"
    (expect (cc/org-tags-find-context '("foo" "@house" "bar")) :to-equal '("@house")))

  (it "Returns nil when no context tag"
    (expect (cc/org-tags-find-context '("foo")) :to-be nil)
    (expect (cc/org-tags-find-context '()) :to-be nil)))
