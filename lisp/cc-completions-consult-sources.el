;;; -*- lexical-binding: t -*-
(require 'consult)
(require 'embark)
(require 'cc-exwm-utils)

(defun cc/completions--consult-mode-predicate (buffer)
  (let* ((buffer-mode (buffer-local-value 'major-mode buffer))
         ;; Some modes have a handful of child modes that should all be shown together.
         (ancestor (provided-mode-derived-p
                    major-mode
                    ;; For `magit-status-mode', `magit-diff-mode', etc
                    'magit-mode
                    ;; Include `lisp-interaction-mode' for *scratch*.
                    'emacs-lisp-mode
                    'circe-mode
                    'org-mode
                    'slack-buffer-mode
                    'comint-mode)))
    (provided-mode-derived-p buffer-mode major-mode ancestor)))

(defvar cc/completions--consult-narrowed nil)

(defvar cc/completions--new-bufffer-action-alist nil
  "Mapping of narrow key to action for `cc/completions-new-buffer-action'.")

(defun cc/completions--consult-narrowed (name narrow-key buffer-query-args &optional fallback)
  "Create a consult source for buffers matching PREDICATE.

BUFFER-QUERY-ARGS plist is passed along to `consult--buffer-query'."
  (push
   `(:name     ,name
     :narrow   (,narrow-key . ,name)
     ;; Buffers are already shown by `consult--source-buffer', so don't show this narrowed source
     ;; until explicitly narrowed.
     :hidden   t
     :category buffer
     :face     consult-buffer
     :history  buffer-name-history
     :state    ,#'consult--buffer-state
     :items
     ,(lambda ()
        (apply #'consult--buffer-query
                 :sort 'visibility
                 :as #'buffer-name
                 buffer-query-args)))
   consult-buffer-sources)
  (when fallback
    (push (cons narrow-key fallback) cc/completions--new-bufffer-action-alist)))

(defun cc/completions-new-buffer-action (_candidate)
  "Create new buffer based on Consult narrow key."
  (when-let (action (assq cc/completions--consult-narrowed cc/completions--new-bufffer-action-alist))
    (funcall (cdr action))))

(defun cc/completions--consult-narrow-listener (&rest _args)
  (let* ((mini (window-buffer (minibuffer-window)))
         (narrowed (buffer-local-value 'consult--narrow mini)))
    (setq cc/completions--consult-narrowed narrowed)))

(defun cc/completions-consult-sources-init ()
  ;; Default buffer sources, redeclared here to make this function idempotent.
  (setq consult-buffer-sources '(consult--source-hidden-buffer
                                 consult--source-buffer
                                 consult--source-recent-file
                                 consult--source-bookmark
                                 consult--source-project-buffer
                                 consult--source-project-recent-file))
  (setq cc/completions--new-bufffer-action-alist nil)

  (dolist (args '(("Major mode" ?c
                   (:predicate cc/completions--consult-mode-predicate))
                  ("EXWM" ?e
                   (:predicate (lambda (buffer)
                                 (with-current-buffer buffer
                                   (and (equal major-mode 'exwm-mode)
                                        ;; I have a separate source for Firefox buffers.
                                        (not (cc/firefox-buffer-p)))))))
                  ("Web" ?w
                   (:predicate (lambda (buffer)
                                 (cc/firefox-buffer-p buffer)))
                   (lambda ()
                     (start-process-shell-command "firefox" nil "firefox --new-window")))
                  ("Slack" ?s
                   (:mode (slack-mode slack-buffer-mode))
                   slack-select-rooms)
                  ("Org" ?o
                   (:mode org-mode))
                  ("Vterm" ?v
                   (:mode vterm-mode)
                   (lambda ()
                     (vterm t)))))
    (apply #'cc/completions--consult-narrowed args))
  ;; `embark-buffer-map' is used when point is on a buffer candidate (unsurprisingly), but
  ;; `embark-general-map' is used when not on a candidate/if there are no candidates.
  (dolist (map (list embark-general-map embark-buffer-map))
    (define-key map "n" #'cc/completions-new-buffer-action))
  (advice-add #'consult-narrow :after #'cc/completions--consult-narrow-listener))

(provide 'cc-completions-consult-sources)
