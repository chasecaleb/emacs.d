;;; -*- lexical-binding: t -*-
(require 'seq)
(require 'info)

(defun cc/nix--autoloads ()
  (dolist (profile (seq-map (lambda (it) (expand-file-name "share/emacs/site-lisp" it))
                            (reverse (split-string (or (getenv "NIX_PROFILES") "")))))
    (when (file-exists-p profile)
      (dolist (autoloads (directory-files-recursively profile (rx "-autoloads.el" eos) nil nil t))
        (load autoloads nil t)))))

(defun cc/nix--info ()
  (info-initialize) ; Populate `Info-directory-list' from $INFOPATH.
  (dolist (profile (seq-map (lambda (it) (expand-file-name "share/emacs/site-lisp" it))
                            (reverse (split-string (or (getenv "NIX_PROFILES") "")))))
    (when (file-exists-p profile)
      (dolist (info-marker (directory-files-recursively profile (rx bos "dir" eos) nil nil t))
        (push (file-name-directory info-marker) Info-directory-list)))))

(defun cc/nix-init ()
  (cc/nix--autoloads)
  (cc/nix--info)
  ;; Nix > package.el
  (setq package-enable-at-startup nil))

(provide 'cc-nix)
