;;; -*- lexical-binding: t -*-
(require 'vterm)
(require 'hl-line)
(require 'cc-utils)

(defun cc/vterm--mode-setup ()
  (setq-local global-hl-line-mode nil))

(defun cc/vterm-for-file (file)
  "Open new `vterm' in directory of FILE."
  (let ((default-directory (file-name-directory file)))
    (vterm t)))

(defun cc/vterm-init ()
  ;; I use `global-hl-line-mode', but vterm is the one place I don't want it.
  (hl-line-mode -1)
  (add-hook 'vterm-mode #'cc/vterm--mode-setup)
  (global-set-key (kbd "s-v") #'vterm)
  (cc/define-key-list vterm-mode-map
                      `(("M->" . ,#'vterm-reset-cursor-point)
                        ("C-x g" . ,#'magit-status)))
  ;; vterm-buffer-name-string nil is default, but it's here as an explicit reminder: DO NOT change
  ;; this, because changing it causes pain and isn't worth it. For example, changing this prevents
  ;; `projectile-run-vterm' from detecting existing vterm instances. Besides that, I have a
  ;; spaceline segment that displays the current directory in the modeline.
  (setq vterm-buffer-name-string nil)
  (setq vterm-max-scrollback (* 10 1000)))

(provide 'cc-vterm)
