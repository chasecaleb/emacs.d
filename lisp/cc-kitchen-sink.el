;;; -*- lexical-binding: t; byte-compile-warnings: (not noruntime docstrings) -*-
;; - Runtime byte compiler warnings disabled because it complains about almost every function I use
;;   with hydra and it has never saved me from any actual issues.
;;   TODO revisit above (i.e. un-disable noruntime) after refactoring.
;;
;; - Docstring length warnings disabled because hydras with lambdas trigger it, which is pointless.
;;   It might make sense to revisit this sometime in the future, since it was just introduced within
;;   the last few days to Emacs master branch... but I don't care about this warning because this is
;;   my init file, not a published library, and it has nothing to do with correctness.

(require 'use-package)
(setq use-package-expand-minimally t)

(require 'cc-user-directories)
(require 'cc-utils)
(require 'cc-org-gtd)

;; bind-key and diminish are used by use-package's :bind and :diminish.
(use-package bind-key
  :demand t)
(use-package diminish
  :demand t)


;;; Core Emacs

(progn ; improved keybinds - partially taken from better-defaults.el
  (autoload 'zap-up-to-char "misc" nil t)
  (global-set-key (kbd "M-z") 'zap-up-to-char)
  (global-set-key (kbd "M-/") 'hippie-expand)
  ;; Accidentally hitting this and freezing emacs is annoying, especially when Emacs is also your
  ;; window manager (EXWM).
  (define-key global-map (kbd "C-z") nil)
  (define-key global-map (kbd "C-x C-z") nil))

(progn ; miscellaneous core config
  (setq-default indent-tabs-mode nil)
  (setq-default tab-width 4) ; Emacs default is 8, which is crazy.
  (setq save-interprogram-paste-before-kill t)
  (setq require-final-newline t)
  (setq visible-bell t)
  (put 'narrow-to-region 'disabled nil)
  (setq next-screen-context-lines 10)
  (setq sentence-end-double-space nil)
  ;; Please go burn in a fire, custom file.
  (let ((dest (concat "emacs-custom-" (number-to-string (emacs-pid)))))
    (setq custom-file (expand-file-name dest temporary-file-directory)))
  ;; Because of course emacs has HTTP cookies, it turns out.
  (require 'url-cookie)
  (setq url-cookie-file (expand-file-name "url-cookies" cc/user-cache))
  ;; Make all prompts consistently y/n instead of a mix, which is unnecessary
  ;; cognitive load.
  (defalias 'yes-or-no-p 'y-or-n-p)

  ;; Recommended for lsp-mode - https://emacs-lsp.github.io/lsp-mode/page/performance/
  (setq read-process-output-max (* 1024 1024)))

;; Auto save/backup/lock files and auto reverting.
;;
;; See elisp man "27: Backups and Auto-Saving" and "18: Files", but TLDR is:
;; - Locks: prevent multiple Emacs instances editing same file.
;; - Backups: copy of original file before editing, made once when first visited. Also (typically)
;;   automatically disabled for version-controlled files.
;; - Auto saves: periodic copy of unsaved changes (`auto-save-mode', NOT `auto-save-visited-mode').
(progn
  ;; Lock files are completely useless to me and occasionally a nuisance when testing init changes
  ;; in a separate Emacs instance.
  (setq create-lockfiles nil)
  ;; Keep backup and auto save files out of the way, because I don't like them littering my working
  ;; dir, especially due to developer tools that do dumb things like trying to copy or compile them.
  (setq backup-directory-alist
        `(("." . ,temporary-file-directory)))

  ;; Tempting to put auto saves in ~/.cache for persistence across restarts, but I don't want to
  ;; thrash my SSD. The only time I'm likely to need these is if Emacs crashes, in which cases at
  ;; most a logout will be required to recover, not a restart.
  (setq auto-save-file-name-transforms
        `((".*" ,(concat temporary-file-directory) t)))
  (setq auto-save-list-file-prefix (expand-file-name "auto-save-list/saves-" cc/user-cache))
  (setq auto-save-timeout 10)

  (require 'cc-file-auto-save)
  (cc/file-auto-save-mode)
  (global-auto-revert-mode)
  ;; Modifying `revert-buffer-insert-file-contents-function' based on doc of `auto-revert-mode'.
  (setq revert-buffer-insert-file-contents-function #'revert-buffer-insert-file-contents-delicately)
  ;; Disabling `auto-revert-use-notify' eliminates delay when making changes via magit... wtf?
  (defvar auto-revert-use-notify) ;; Should probably do this the right way, but whatever.
  (setq auto-revert-use-notify nil)
  (add-hook 'dired-mode-hook #'auto-revert-mode))

(progn ; File encodings are fun and exciting said... no one.
  (prefer-coding-system 'utf-8)
  (setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING)))

;; Undo config, based on values used by Doom Emacs.
(setq undo-limit 400000           ; 400kb (default is 160kb)
      undo-strong-limit 3000000   ; 3mb   (default is 240kb)
      undo-outer-limit 48000000)  ; 48mb  (default is 24mb)

(progn ; `isearch'
  (setq isearch-allow-scroll t)
  (global-set-key (kbd "C-s") 'isearch-forward-regexp)
  (global-set-key (kbd "C-r") 'isearch-backward-regexp)
  (global-set-key (kbd "C-M-s") 'isearch-forward)
  (global-set-key (kbd "C-M-r") 'isearch-backward))

(use-package simple
  :demand t
  :bind (("M-SPC" . cycle-spacing)
         ("M-j" . #'cc/join-line))
  :hook (flycheck-error-list-mode . visual-line-mode)
  :preface
  (defun cc/join-line ()
    (interactive)
    (join-line -1))
  :config
  (setq search-ring-max 100)
  (setq kill-ring-max 200)
  (setq mark-ring-max 50)
  (setq global-mark-ring-max 250)
  (setq kill-do-not-save-duplicates t)
  (setq history-delete-duplicates t)

  ;; Truncation when doing interactive things/debugging is annoying. Note: nil means unlimited.
  (setq eval-expression-print-level 12)
  (setq eval-expression-print-length 36))

(progn
  (require 'compile)

  ;; Based on https://rtime.ciirc.cvut.cz/~sojka/blog/compile-on-save/
  (defun cc/compile-on-save--run ()
    (when-let ((b (compilation-find-buffer)))
      (unless (get-buffer-process b) ; No-op if already compiling.
        (recompile))))

  (define-minor-mode cc/compile-on-save-mode
    "Automatically call `recompile' on save."
    :group 'cc/compile-on-save
    (if cc/compile-on-save-mode
        (progn  (make-local-variable 'after-save-hook)
                (add-hook 'after-save-hook #'cc/compile-on-save--run nil t))
      (kill-local-variable 'after-save-hook)))

  (setq compilation-scroll-output nil)
  ;; These prompts get really annoying after the millionth time or so. No Emacs, I don't want to
  ;; save every single buffer all the time.
  (setq compilation-ask-about-save nil)
  (setq compilation-save-buffers-predicate '(lambda () nil)))

(use-package newcomment
  :demand t
  :hook (prog-mode . auto-fill-mode)
  :diminish auto-fill-function
  :bind (([remap comment-line] . cc/comment-line-or-duplicate))
  :preface
  (defun cc/comment-line-or-duplicate (arg)
    "Similar to `comment-line', except prefix behavior is different.

If called with any prefix, the line/region to be commented will
also be duplicated below. Inspired by IntelliJ's 'c-d' behavior."
    (interactive "P")
    (if arg
        (let* ((is-region (use-region-p))
               ;; Start/end calculation based on `comment-line'.
               (start (if is-region
                          (save-excursion
                            (goto-char (region-beginning))
                            (line-beginning-position))
                        (line-beginning-position)))
               ;; Difference from comment-line: add 1 to end to copy the line ending too.
               (end (if is-region
                        (save-excursion
                          (goto-char (region-end))
                          (line-end-position))
                      (line-end-position)))
               (contents (buffer-substring start end)))
          ;; Reminder: comment-line modifies buffer, so start/end vars are now inaccurate.
          (comment-line nil)
          ;; Go to next line (after region) to match comment-line's non-region behavior.
          (when is-region
            (goto-char (region-end))
            (goto-char (line-end-position))
            (when (equal (point) (buffer-end 1))
              (newline))
            (forward-line 1))
          (goto-char (line-beginning-position))
          (insert contents)
          (newline))
      (comment-line nil)))
  :config
  (setq comment-auto-fill-only-comments t))

(use-package dired
  :defer 1
  :bind (:map dired-mode-map
         ("C-c C-e" . #'wdired-change-to-wdired-mode))
  :config
  (setq dired-listing-switches "-Alhv")
  (setq dired-recursive-copies 'always)
  (setq dired-recursive-deletes 'always)
  ;; With two dired buffers open, dired will suggest the other buffer as rename destination.
  (setq dired-dwim-target t))

(use-package wdired
  :defer 1
  :config
  (setq wdired-allow-to-change-permissions t)
  (setq wdired-use-dired-vertical-movement t))

(use-package help
  :defer 1
  :config
  (temp-buffer-resize-mode))

(use-package help-at-pt
  :config
  (setq help-at-pt-display-when-idle t)
  (setq help-at-pt-timer-delay 0.5)
  (help-at-pt-cancel-timer)
  (help-at-pt-set-timer))

(use-package man
  :defer 1
  :config
  (setq Man-notify-method 'pushy)
  (setq Man-width-max 100))

(use-package apropos
  :defer 1
  :config
  (setq apropos-do-all t))

(use-package uniquify
  :config
  (setq uniquify-buffer-name-style 'forward))

(use-package bookmark
  :demand t
  :config
  (setq bookmark-default-file (expand-file-name "bookmarks" cc/user-cache))
  (setq bookmark-watch-bookmark-file t)
  (setq bookmark-set-fringe-mark nil))

(use-package saveplace
  :defer 1
  :config
  (setq save-place-file (expand-file-name "places" cc/user-cache))
  (save-place-mode))

(use-package savehist
  :config
  (setq savehist-file (expand-file-name "savehist" cc/user-cache))
  (savehist-mode))

(use-package recentf
  :defer 1
  :config
  (setq recentf-save-file (expand-file-name "recentf" cc/user-cache))
  (setq recentf-max-saved-items 200)
  ;; org-agenda has to open all my org files, so it spams the recent list.
  (add-to-list 'recentf-exclude "\\`[^.].*\\.org\\'")
  (recentf-mode))

(use-package transient
  :demand t
  :config
  (setq transient-history-file (expand-file-name "transient-history.el" cc/user-cache))
  (setq transient-values-file  (expand-file-name "transient-values.el" cc/user-config-repo))
  (setq transient-levels-file  (expand-file-name "transient-levels.el" cc/user-config-repo))
  ;; Because some other things trigger transient loading early. I could change my config to make
  ;; sure transient loads first, but then nothing prevents me from having the same issue in the
  ;; future again.
  (setq transient-history (transient--read-file-contents transient-history-file))
  (setq transient-values (transient--read-file-contents transient-values-file))
  (setq transient-levels (transient--read-file-contents transient-levels-file)))

(use-package browse-url
  :defer 1
  :config
  (setq browse-url-generic-program "firefox"))

(use-package calc
  :defer 1
  :config
  ;; This has a truly horrible default value, even by Emacs standards.
  (setq calc-multiplication-has-precedence nil))

(use-package tramp
  :defer t
  :config
  (setq tramp-default-method "ssh")

  ;; Don't try to use authinfo, because it's gpg-encrypted and I don't use it for tramp (besides,
  ;; the correct answer for remote authentication is to use SSH keys, *not* passwords).
  ;; References:
  ;; - https://emacs.stackexchange.com/questions/64062/how-to-avoid-using-auth-sources-when-editing-with-sudo
  ;; - https://debbugs.gnu.org/cgi/bugreport.cgi?bug=46674 (linked in stackexchange answer)
  ;; - Tramp 4.13 Reusing passwords for several connections
  ;; - Emacs info 49.2.6: Per-Connection Local Variables
  (setq tramp-completion-use-auth-sources nil) ; nil is the default, but just in case it changes.
  (connection-local-set-profile-variables 'remote-without-auth-sources '((auth-sources . nil)))
  (connection-local-set-profiles '(:application tramp) 'remote-without-auth-sources))

(use-package password-cache
  :config
  ;; Note: cache can be cleared via `password-reset' if needed for... whatever reason.
  (setq password-cache t) ; t is the default, but just to be explicit
  (setq password-cache-expiry (* 60 60 8)))

;; Why did I not know about this until now? It's amazing for exploring code.
;; Activate by switching to `read-only-mode'.
(progn ; view-mode
  (setq view-read-only t))


;;; Modeline
;; Update all modelines continuously while idle to make sure clock is accurate-ish.
(cc/run-with-idle-timer-repeating 30 #'force-mode-line-update t)

(use-package fancy-battery
  :config
  (fancy-battery-mode)
  (setq fancy-battery-show-percentage t)
  (setq battery-load-critical 20)
  (with-eval-after-load 'spaceline-segments
    (set-face-attribute 'fancy-battery-discharging nil :inherit 'powerline-inactive1)))

(use-package spaceline
  :preface
  (defun cc/powerline-selected-ignore-corfu (fn &rest args)
    "Stop powerline mode line flickering when corfu re-creates its frame."
    (unless (eq (selected-frame) (bound-and-true-p corfu--frame))
      (apply fn args)))
  :config
  (advice-add #'powerline-set-selected-window :around #'cc/powerline-selected-ignore-corfu))

(use-package spaceline-segments
  :config
  (spaceline-define-segment cc/spaceline-ace-window
    (let ((path (window-parameter (selected-window) 'ace-window-path)))
      (when path ; ace-window might not be running yet.
        (char-to-string (seq-elt path 0)))))

  ;; Note: circe/tracking used for slack.
  (spaceline-define-segment cc/spaceline-tracking-ignored
    (when (and (fboundp 'tracking-ignored-p)
               (tracking-ignored-p (current-buffer) nil))
      "[ignored]"))

  (require 'projectile)
  (spaceline-define-segment cc/spaceline-project
    ;; Don't show this for remote (e.g. tramp) buffers for performance reasons.
    (unless (file-remote-p default-directory 'host)
      ;; Modified from projectile-root segment in spaceline-segments.el
      (let ((project-name (projectile-project-name)))
        (unless (or (string= project-name "-")
                    (string= project-name (buffer-name)))
          (concat "[" project-name "]")))))

  (spaceline-define-segment cc/remote-info
    "Better version of remote-host in `spaceline-segment'"
    (when default-directory
      (when-let* ((user (file-remote-p default-directory 'user))
                  (host (file-remote-p default-directory 'host)))
        (concat user "@" host))))

  (spaceline-define-segment cc/spaceline-vterm
    (when (eq major-mode 'vterm-mode)
      (if (tramp-tramp-file-p default-directory)
          (tramp-file-name-localname (tramp-dissect-file-name default-directory))
        (abbreviate-file-name default-directory))))

  (spaceline-compile
    ;; Left side
    ;; Using 'spaceline-highlight-face directly for ace window to highlight even if inactive
    '((cc/spaceline-ace-window :priority 100 :face 'spaceline-highlight-face)

      (buffer-modified :priority 100 :skip-alternate t)
      (cc/spaceline-project :priority 30 :skip-alternate t :tight t)
      ((cc/remote-info buffer-id cc/spaceline-vterm) :priority 100)

      (cc/spaceline-tracking-ignored :priority 85)
      ;; Major mode lower priority than minor because minor modes tend to add useful info, e.g. EXWM
      ;; shows char/line mode status in minor mode.
      (major-mode :priority 40)
      (process :when active :priority 60)
      (minor-modes :when active :priority 50)
      ((flycheck-error flycheck-warning flycheck-info) :priority 85))
    ;; Right side
    '((selection-info :priority 90 :when active)
      (buffer-encoding :priority 10)
      (line-column :priority 95 :when (not (eq major-mode 'exwm-mode)))
      (buffer-position :priority 85 :when (not (eq major-mode 'exwm-mode)))))

  (setq-default mode-line-format '("%e" (:eval (spaceline-ml-main)))))


;;; Hydra - this goes early in init because I use it in so many places.
;; (It's easier to just load it early than mess with :after all over the place)
;; Also, since it's a macro I want it loaded immediately not deferred because of byte compilation.
(require 'hydra)
(setq hydra-look-for-remap t)
(defmacro cc/dynamic-hydra--create (name body docstring &rest conditional-heads)
  (declare (indent defun))
  `(progn
     (eval
      (append
       '(defhydra ,name ,body ,docstring)
       (cl-loop for ch in ',conditional-heads
                if (eval (car ch))
                collect (cdr ch))))))

(defmacro cc/dynamic-hydra (name body docstring &rest conditional-heads)
  "Hydra with heads that are conditionally evaluated/shown.

See https://github.com/abo-abo/hydra/issues/86 for context."
  (declare (indent defun))
  `(defun ,name ()
     (interactive)
     (cc/dynamic-hydra--create ,name ,body ,docstring ,@conditional-heads)
     (funcall-interactively ',(intern (concat (symbol-name name) "/body")))))


;;; Window management (Ace Window, etc)
(winner-mode 1)

(use-package transpose-frame)

(use-package ibuffer
  :bind (:map ibuffer-mode-map
         ;; Interferes with ace-window.
         ("M-o" . nil))
  :config
  (setq ibuffer-display-summary nil)
  (setq ibuffer-formats '((mark modified read-only "| "
                                (mode 16 16 :left :elide)
                                " | "
                                name)
                          (mark modified read-only " "
                                (name 40 40 :left :elide)
                                " " filename-and-process))))

(use-package ibuf-ext
  :config
  (setq ibuffer-never-show-predicates
        (list (rx "*Help*")
              (rx "*Completions*")
              (rx "*Buffer List*")
              (rx "*Disabled Command*")
              (rx "*Flycheck error messages*")
              (rx "*Slack Log -")
              (rx "*Slack Event Log -")
              (rx "slack-curl-downloader"))))

(use-package ace-window
  :demand t
  :bind ("M-o" . ace-window)
  :config
  (defun cc/delete-window-and-buffer (window)
    (aw-delete-window window t))
  (defun cc/switch-buffer-in-window (window)
    (aw-switch-to-window window)
    (call-interactively #'consult-buffer))
  (defun cc/move-buffer-kill-window (window)
    "Move current buffer to another window and kill old window"
    (let ((old-window (frame-selected-window)))
      (aw-move-window window)
      (aw-delete-window old-window)))
  (defun cc/move-window-and-point (window)
    "Like `aw-move-window', but the current buffer's point will
    be used instead of the target window if both windows are
    displaying the same buffer."
    (let ((buffer (current-buffer))
          (p (window-point)))
      (switch-to-buffer (other-buffer))
      (aw-switch-to-window window)
      (switch-to-buffer buffer)
      (set-window-point window p)))

  (defhydra cc/hydra-window-size (:color red)
    "Window size"
    ("j" (shrink-window 5)                     "Shrink vertical" :column "Vertical")
    ("J" (shrink-window 25)                    "    5x")
    ("k" (enlarge-window 5)                    "Enlarge vertical")
    ("K" (enlarge-window 25)                   "    5x")
    ("l" (enlarge-window-horizontally 5)       "Enlarge horizontal" :column "Horizontal")
    ("L" (enlarge-window-horizontally 25)      "    5x")
    ("h" (shrink-window-horizontally 5)        "Shrink horizontal")
    ("H" (shrink-window-horizontally 25)       "    5x")
    ("-" (shrink-window-if-larger-than-buffer) "Fit" :column "Misc")
    ("=" (balance-windows)                     "Balance")
    ("C-g" nil nil)
    ("RET" nil nil))

  ;; TODO: figure out some sort of layout presets
  (defhydra cc/hydra-transpose-frame (:color red)
    "Transpose frame"
    ("t" #'transpose-frame "Swap X/Y")
    ("v" #'flip-frame "Flip vertically")
    ("h" #'flop-frame "Flip horizontally")
    ("r" #'rotate-frame-clockwise "Rotate CW")
    ("R" #'rotate-frame-anticlockwise "Rotate CCW")
    ("C-g" nil nil)
    ("RET" nil nil))

  ;; EXWM can't render overlays on top of of X11 windows, so disable them
  ;; and use the mode line instead.
  (setq aw-display-mode-overlay nil)
  (ace-window-display-mode t)
  ;; KLUDGE warning: removing what ace-window-display-mode puts in the modeline
  ;; because I use a spaceline segment instead (in order to match the rest of
  ;; the mode line)... however,  the mode still needs to be active so that
  ;; ace-window will update the window parameter for my spaceline segment to
  ;; read. I did say kludge, didn't I?
  (set-default
   'mode-line-format
   (assq-delete-all
    'ace-window-display-mode
    (default-value 'mode-line-format)))

  ;; In EXWM, only a single frame is visible at a time since they are used as
  ;; workspaces.
  (setq aw-scope 'frame)

  (setq aw-keys '(?a ?s ?d ?f ?j ?k ?l ?\; ?q ?w ?e)
        aw-dispatch-always t
        aw-minibuffer-flag t
        ;; Regarding optional 2nd arg:
        ;; "If the action character is followed by a string, then ace-window
        ;; will be invoked again to select the target window for the action.
        ;; Otherwise, the current window is selected"
        aw-dispatch-alist
        '((?r cc/hydra-window-size/body)
          (?t cc/hydra-transpose-frame/body)
          (?o aw-flip-window)
          (?v aw-split-window-vert "Split: vertical")
          (?h aw-split-window-horz "Split: horizontal")
          (?x aw-delete-window "Delete window")
          (?X cc/delete-window-and-buffer "Delete window and buffer")
          (?z delete-other-windows "Maximize window")
          (?m aw-swap-window "Swap window (with current)")
          (?M cc/move-window-and-point "Move window")
          (?n cc/move-buffer-kill-window "Move and kill")
          (?c aw-copy-window "Copy window")
          (?b cc/switch-buffer-in-window "Switch buffer")
          (?? aw-show-dispatch-help)))

  ;; Refresh mode-line with config changes - otherwise the default keys are
  ;; shown immediately after startup (e.g. "1" instead of "a").
  (aw-update))


;;; General packages

(use-package outline
  :demand t
  :bind (("C-c o" . #'cc/hydra-outline/body))
  :preface
  (defhydra cc/hydra-outline (:color pink)
    ;; Hide
    ("q" #'outline-hide-sublevels "sublevels" :column "Hide")
    ("t" #'outline-hide-body "body")
    ("o" #'outline-hide-other "other")
    ("c" #'outline-hide-entry "entry")
    ("l" #'outline-hide-leaves "leaves")
    ("d" #'outline-hide-subtree "subtree")
    ;; Show
    ("a" #'outline-show-all "all" :column "Show")
    ("e" #'outline-show-entry "entry")
    ("i" #'outline-show-children "children")
    ("k" #'outline-show-branches "branches")
    ("s" #'outline-show-subtree "subtree")
    ;; Move
    ("u" #'outline-up-heading "up" :column "Move")
    ("n" #'outline-next-visible-heading "next visible")
    ("p" #'outline-previous-visible-heading "previous visible")
    ("f" #'outline-forward-same-level "forward same level")
    ("b" #'outline-backward-same-level "backward same level")
    ("C-g" nil nil)
    ("RET" nil nil)))

(use-package flycheck
  :demand t
  ;; I have a separate spaceline segment to show Flycheck counts, so override Flycheck's
  ;; minor mode text since by default it would also be displayed here. I still want FlyC to show in
  ;; the minor mode (as opposed to diminishing it entirely) to verify flycheck is enabled.
  :diminish (flycheck-mode . "FlyC")
  :hook ((lsp-managed-mode . cc/lsp-flycheck-checker-setup)
         (emacs-lisp-mode . cc/flycheck-elisp-setup))
  :preface
  (defvar-local cc/flycheck-local-cache nil)

  (defun cc/flycheck-allow-remote-files (fn &rest args)
    "Trick flycheck into enabling itself even if a buffer is remote.
Flycheck maintainers refuse to allow this by default or even add an option for it."
    (cl-letf (((symbol-function 'file-remote-p) 'ignore))
      (apply fn args)))

  ;; Kludge to allow additional checkers alongisde lsp.
  ;; Based on https://github.com/flycheck/flycheck/issues/1762#issuecomment-750458442
  (defconst cc/flycheck-lsp-next-checkers
    '((sh-mode . (sh-shellcheck)))
    "Additional checkers to use with LSP for specific modes.")

  (defun cc/flycheck-checker-get (fn checker property)
    (or (alist-get property (alist-get checker cc/flycheck-local-cache))
        (funcall fn checker property)))

  (defun cc/lsp-flycheck-checker-setup ()
    (cl-loop
     for (mode . checkers) in cc/flycheck-lsp-next-checkers
     do (when (derived-mode-p mode)
          (message "Adding checker(s): %s" checkers)
          ;; CAVEAT: this should probably support merging instead of overwriting, but good enough.
          (setq cc/flycheck-local-cache `((lsp . ((next-checkers . ,checkers))))))))

  (defun cc/flycheck-elisp-setup ()
    "Make flycheck's byte-compilation checker work correctly for my emacs.d"
    (when (and buffer-file-name
               (file-in-directory-p buffer-file-name cc/user-source-repo))
      (setq-local flycheck-emacs-lisp-initialize-packages t)
      (setq-local flycheck-emacs-lisp-package-initialize-form
                  (flycheck-sexp-to-string
                   '(progn
                      ;; No clue why but seq has to be *directly* required here, not in cc-nix.
                      (require 'seq)
                      (require 'cc-nix)
                      (cc/nix--autoloads))))

      ;; Have flycheck check against source copy of my init instead of what's installed.
      (setq-local flycheck-emacs-lisp-load-path
                  (seq-map (lambda (dir)
                             (if (string-match-p (rx "/chasecaleb-emacs") dir)
                                 (expand-file-name "lisp" cc/user-source-repo)
                               dir))
                           load-path))))

  :config
  (advice-add 'flycheck-checker-get :around 'cc/flycheck-checker-get)
  (advice-add #'flycheck-may-enable-mode :around #'cc/flycheck-allow-remote-files)

  (setq flycheck-global-modes '(not exwm-mode)) ; Pointless, and clutters EXWM mode line.
  (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc))

  (global-flycheck-mode))

;; dump-jump implements an xref backend.
(use-package dumb-jump
  :demand t
  :config
  (add-to-list 'xref-backend-functions #'dumb-jump-xref-activate))

(use-package symbol-overlay
  :demand t
  :diminish
  :hook (prog-mode . symbol-overlay-mode)
  :bind (("C-c s" . #'cc/do-symbol-overlay-hydra))
  :preface
  (defhydra cc/symbol-overlay-hydra (:color pink)
    ("i" #'symbol-overlay-put                          "put" :column "Navigation")
    ("n" #'symbol-overlay-jump-next                    "go     (n)")
    ("p" #'symbol-overlay-jump-prev                    "       (p)")
    ("M-<" #'symbol-overlay-jump-first                 "       (first)")
    ("M->" #'symbol-overlay-jump-last                  "(last)")
    ("M-n" #'symbol-overlay-switch-forward             "switch (n)")
    ("M-p" #'symbol-overlay-switch-backward            "       (p)")
    ("k" #'symbol-overlay-remove-all                   "remove all")
    ("t" #'symbol-overlay-toggle-in-scope              "scope (toggle)")

    ("w" #'symbol-overlay-save-symbol                  "save" :column "Misc")
    ("c" #'symbol-overlay-count                        "count")
    ("s" #'symbol-overlay-isearch-literally            "isearch")
    ("r" #'symbol-overlay-rename                       "rename")
    ("M-r" #'symbol-overlay-query-replace              "query-replace")
    ("z" #'symbol-overlay-mode                         "mode (toggle)")
    ("C-g" nil)
    ("RET" nil nil))

  (defun cc/do-symbol-overlay-hydra ()
    (interactive)
    (symbol-overlay-mode)
    (cc/symbol-overlay-hydra/body))

  :config
  ;; Inherits from 'highlight by default, which made it hard to distinguish against an active
  ;; transient region.
  (set-face-attribute 'symbol-overlay-default-face nil :inherit 'lazy-highlight)
  ;; By default there are eight different faces, and some are hard to distinguish.
  (set-face-background 'symbol-overlay-face-1 "dodger blue")
  (set-face-background 'symbol-overlay-face-2 "orange")
  (set-face-background 'symbol-overlay-face-3 "yellow")
  (set-face-background 'symbol-overlay-face-4 "orchid")
  (set-face-background 'symbol-overlay-face-5 "red")
  (set-face-background 'symbol-overlay-face-6 "spring green")
  (setq symbol-overlay-faces '(symbol-overlay-face-1
                               symbol-overlay-face-2
                               symbol-overlay-face-3
                               symbol-overlay-face-4
                               symbol-overlay-face-5
                               symbol-overlay-face-6)))

(require 'yasnippet)
(use-package autoinsert
  :demand t
  :config
  (defun cc/autoinsert-yasnippet-boilerplate ()
    "Expand yasnippet named 'boilerplate' for current major mode.
Intended for use with `autoinsert'."
    (yas-expand-snippet
     (yas-lookup-snippet "boilerplate"))
    (when (and (derived-mode-p 'sh-mode)
               (fboundp 'sh-set-shell))
      ;; sh-mode's shell detection heuristic for new files is... not good. If the file ends with
      ;; ".sh" it assumes Bourne Shell regardless of `sh-shell-file' and `sh-shell'. No one in the
      ;; real world names bash scripts foo.bash like sh-mode seems to think.
      (sh-set-shell "bash")
      ;; sh-set-shell breaks `hl-todo-mode' somehow, so have to call it again.
      (when (fboundp 'hl-todo-mode)
        (hl-todo-mode))))

  (setq auto-insert-alist
        '((sh-mode . cc/autoinsert-yasnippet-boilerplate)
          (emacs-lisp-mode . cc/autoinsert-yasnippet-boilerplate)))
  (setq auto-insert-query nil)
  (auto-insert-mode))

(use-package visual-regexp
  :defer t
  :bind ("C-c r" . vr/query-replace))

;; usage, with examples: M-x sp-cheat-sheet
;; Also, remember that =C-q= `quoted-insert' can be used to bypass strict mode (e.g. =C-q "= to
;; insert a single quote instead of a matching pair).
;; https://github.com/Fuco1/smartparens/wiki
;; https://github.com/Fuco1/smartparens/wiki/Working-with-expressions#navigation-functions (and next section)
;; https://ebzzry.io/en/emacs-pairs/
(use-package smartparens
  :defer 1
  :diminish
  :hook ((prog-mode ielm-mode) . smartparens-strict-mode)
  ;; See `smartparens-strict-mode-map' for remapped manipulation commands (e.g. sp-kill-hybrid-sexp
  ;; as "c-k").
  ;;
  ;; TODO: Modify bindings for non-s-exp languages (e.g. change sp-transpose-sexp =>
  ;; sp-transpose-hybrid-sexp)?
  :bind (:map smartparens-mode-map
         ;;
         ;; Navigation commands (replacements for Emacs built-in versions).
         ;;
         ("C-M-f" . sp-forward-sexp)
         ("C-M-b" . sp-backward-sexp)
         ("M-f" . sp-forward-symbol)
         ("M-b" . sp-backward-symbol)

         ("C-M-n" . sp-next-sexp)
         ("C-M-p" . sp-previous-sexp)

         ("C-M-e" . sp-up-sexp)
         ("C-M-u" . sp-backward-up-sexp)
         ("C-M-d" . sp-down-sexp)
         ("C-M-a" . sp-backward-down-sexp)

         ("C-S-d" . sp-beginning-of-sexp)
         ("C-S-a" . sp-end-of-sexp)

         ;;
         ;; Manipulation commands
         ;;
         ("C-M-t" . sp-transpose-sexp)
         ("C-M-k" . sp-kill-sexp)
         ("C-M-w" . sp-copy-sexp)

         ("M-D" . sp-splice-sexp)
         ("C-S-<backspace>" . sp-splice-sexp-killing-around)

         ("C-<right>" . sp-forward-slurp-sexp)
         ("C-<left>" . sp-forward-barf-sexp)
         ("C-M-<left>" . sp-backward-slurp-sexp)
         ("C-M-<right>" . sp-backward-barf-sexp)

         ("C-]" . sp-select-next-thing-exchange)
         ("C-M-]" . sp-select-next-thing)


         ("M-i" . sp-change-inner)
         ("M-I" . sp-change-enclosing))
  :config
  (require 'smartparens-config)
  (show-smartparens-global-mode t)
  (set-face-attribute 'sp-show-pair-match-content-face nil :inherit 'lazy-highlight)
  (setq sp-navigate-interactive-always-progress-point t)
  (setq sp-navigate-comments-as-sexps nil))

(use-package avy
  :bind (([remap goto-line] . #'avy-goto-char-timer)
         ("M-g r" . #'avy-resume)
         ;; Not sure how much I'll use `avy-next'/`avy-prev' in reality, but in theory they're nice.
         ("M-g an" . #'avy-next)
         ("M-g ap" . #'avy-prev)
         ;; `avy-goto-line' can replace `goto-line' since it also accepts numeric line numbers.
         ("M-g l" . #'avy-goto-line)
         ("M-g h" . #'avy-org-goto-heading-timer)
         :map isearch-mode-map
         ;; Start an isearch, then use `avy-isearch' to jump to a specific match.
         ("M-g" . #'avy-isearch))
  :preface
  (defmacro cc/def-avy-action (name &rest body)
    "Define a function for use with `avy-dispatch-alist'.

- NAME should start with 'avy-action-' for compatibility with avy's help.
- Window and buffer excursion will be saved and BODY will be called with point at avy selection.
- Point arg will be bound to 'pt'."
    (declare (indent 1))
    `(defun ,name (pt)
       (save-window-excursion
         (save-excursion
           (goto-char pt)
           ,@body))))

  (cc/def-avy-action avy-action-kill-whole-line
    ;; Action-only equivalent of `avy-kill-whole-line'
    (kill-whole-line))

  (cc/def-avy-action avy-action-copy-whole-line
    ;; Action-only equivalent of `avy-copy-line'
    (cl-destructuring-bind (start . end)
        (bounds-of-thing-at-point 'line)
      (copy-region-as-kill start end)))

  (defun avy-action-yank-whole-line (pt)
    ;; defun, not `cc/def-avy-action', because this is a wrapper around another.
    (avy-action-copy-whole-line pt)
    (yank))

  (defun avy-action-teleport-whole-line (pt)
    ;; defun, not `cc/def-avy-action', because this is a wrapper around another.
    (let ((line-start-p (<= (point) (save-excursion
                                      (back-to-indentation)
                                      (point))))
          (start-pt (point)))
      (avy-action-kill-whole-line pt)
      (save-excursion (yank))
      (if line-start-p
          (goto-char start-pt) ; Go to end of inserted region
        (progn
          ;; Merge lines together cleanly
          (cycle-spacing 1)
          (end-of-line)
          (delete-indentation t)))))

  (defun avy-action-mark-to-char (pt)
    (activate-mark)
    (goto-char pt))

  :config
  (setq avy-timeout-seconds 0.35)
  ;; Single candidate jump prevents using dispatch actions with a single canidate. Also it
  ;; increases cognitive burden for marginal efficiency benefit.
  (setq avy-single-candidate-jump nil)
  ;; I have too many windows for this, and I can already switch quickly with ace-window.
  (setq avy-all-windows nil)

  (setq avy-keys '(?a ?s ?d ?f ?g ?h ?j ?k ?l))
  ;; Note: built-in avy actions like `avy-action-kill-stay', `avy-action-teleport' tend to operate
  ;; on sexp unless name/docstring indicates otherwise.
  (setq avy-dispatch-alist '((?x . avy-action-kill-stay)
                             (?X . avy-action-kill-whole-line)
                             ;; Teleport is basically kill + yank.
                             (?t . avy-action-teleport)
                             (?T . avy-action-teleport-whole-line)
                             (?m . avy-action-mark)
                             (?M . avy-action-mark-to-char)
                             (?w . avy-action-copy)
                             (?W . avy-action-copy-whole-line)
                             (?y . avy-action-yank)
                             (?Y . avy-action-yank-line)
                             (?i . avy-action-ispell)
                             (?z . avy-action-zap-to-char))))

;;; Version control/diff

(defconst cc/bug-reference-work-regexp (rx (group (group "NIPR-" (+ digit)))))
(defconst cc/bug-reference-work-url-format "https://jira.naic.org/browse/%s")

(use-package bug-reference
  :hook ((cc/source-code . bug-reference-prog-mode)
         (slack-mode . cc/bug-reference-slack-setup))
  :bind (:map bug-reference-map
         ("C-c C-o" . #'bug-reference-push-button))
  :preface
  (defun cc/bug-reference-slack-setup ()
    (setq-local bug-reference-bug-regexp cc/bug-reference-work-regexp)
    (setq-local bug-reference-url-format cc/bug-reference-work-url-format)
    (bug-reference-mode))
  :config
  (setq bug-reference-setup-from-vc-alist `((,(rx "gitlab.com" (+? anychar) "NAIC/nipr/")
                                             ,cc/bug-reference-work-regexp
                                             ;; This doesn't allow a string, so... lambda it is.
                                             (lambda (&rest _) cc/bug-reference-work-url-format)))))

(use-package diff-mode
  :bind (:map diff-mode-map
         ;; Conflicts with ace-window switching.
         ("M-o" . nil)))

(use-package magit
  :diminish magit-wip-mode
  :hook (git-commit-setup . cc/magit-commit-setup)
  :bind (:map magit-repolist-mode-map
         ("f" . cc/magit-repolist-git-fetch))
  :preface
  (defun cc/magit--read-string-branch-advice (args)
    (if-let ((ticket (with-temp-buffer
                       (cc/org-clock-insert-ticket "-ccc-")
                       (buffer-string))))
        ;; This is awkward, but it handles a list of length 1, unlike (setf (nth 1 ..) ..).
        (append (list (car args) ticket) (nthcdr 2 args))
      args))

  (defun cc/magit--interactive-branch-name-advice (args)
    "Awkward hack to set ticket as default name when creating a new branch.

This is further complicated due to the branch name prompt occuring in the `interactive' form."
    (interactive (lambda (old-spec)
                   (cc/with-advice #'magit-read-string-ns :filter-args #'cc/magit--read-string-branch-advice
                     (advice-eval-interactive-spec old-spec))))
    args)

  (dolist (fn (list #'magit-branch-create
                    #'magit-branch-and-checkout
                    #'magit-branch-spinoff
                    #'magit-branch-spinout))
    (advice-add fn :filter-args #'cc/magit--interactive-branch-name-advice))

  :config
  (defun cc/magit-commit-setup ()
    (setq fill-column 72)
    (unless (git-commit-buffer-message)
      (cc/org-clock-insert-ticket ": ")))

  ;; Taken from https://github.com/magit/magit/issues/2971#issuecomment-336644529
  (defun cc/magit-repolist-git-fetch ()
    "Fetch all remotes in repositories returned by `magit-list-repos'.
Fetching is done synchronously."
    (interactive)
    (run-hooks 'magit-credential-hook)
    (let* ((repos (magit-list-repos))
           (l (length repos))
           (i 0))
      (dolist (repo repos)
        (let* ((default-directory (file-name-as-directory repo))
               (msg (format "(%s/%s) Fetching in %s..."
                            (cl-incf i) l default-directory)))
          (message msg)
          ;; Hack to deal with caching bug. Caches are fun and easy, right? (n.b. no, they aren't)
          (cc/with-advice #'magit-get-upstream-branch :around
                          (lambda (fn &rest args)
                            (let ((magit--refresh-cache nil))
                              (apply fn args)))
            (magit-run-git "remote" "update" (magit-fetch-arguments)))

          (message (concat msg "done")))))
    (magit-refresh))

  (setq git-commit-style-convention-checks '(non-empty-second-line overlong-summary-line))
  (setq git-commit-summary-max-length 50)
  (setq git-commit-use-local-message-ring t)

  (setq magit-diff-refine-hunk 'all)
  ;; keep-foreground is a bit hard on the eyes (with spacemarks-dark).
  (setq magit-diff-unmarked-lines-keep-foreground nil)

  (setq magit-branch-prefer-remote-upstream '("master" "main"))
  (setq magit-branch-adjust-remote-upstream-alist '(("origin/master" . ("master" "main"))
                                                    ("origin/main" . ("master" "main"))))
  (setq magit-clone-set-remote.pushDefault t)
  ;; `quit-window' with t also kills the buffer, which I find helpful to avoid slowdown over time
  ;; when working with a ton of repos. Also not using `magit-mode-quit-window' because I don't like
  ;; how it potentially messes with my window layout.
  (setq magit-bury-buffer-function (lambda (_) (quit-window t)))
  (setq magit-repository-directories '(("~/code" . 2)
                                       ("~/nipr" . 2)
                                       ("~/" . 1)))
  (setq magit-status-goto-file-position t) ; This is amazing and should be a default.
  (setq magit-save-repository-buffers 'dontask) ; These prompts are annoying
  (magit-wip-mode t)

  ;; Show branches in status because I have the memory of a goldfish and forget about
  ;; outstanding/stale ones otherwise. Plus it's useful to keep track of what other devs are doing
  ;; in the same repo.
  ;; Note: there's also `magit-show-refs' (keybind: y), but showing it in status
  ;; buffer is nice.
  (dolist (status-fn (list #'magit-insert-local-branches #'magit-insert-remote-branches))
    (magit-add-section-hook 'magit-status-sections-hook status-fn nil t))
  (dolist (status-symbol '(local remote))
    (setf (alist-get status-symbol magit-section-initial-visibility-alist) 'hide))

  (setf (alist-get 'unpulled magit-section-initial-visibility-alist) 'show)

  (setq magit-repolist-columns '(("Name"    25 magit-repolist-column-ident nil)
                                 ("Branch"  15 magit-repolist-column-branch nil)
                                 ("St"       2 magit-repolist-column-stashes
                                  ((:help-echo "Number of stashes")
                                   (:right-align t)))
                                 ("F"        1 magit-repolist-column-flag
                                  ((:help-echo "Status flag (N=untracked, U=unstaged, S=staged)")
                                   (:right-align t)))
                                 ("B<U"      3 magit-repolist-column-unpulled-from-upstream
                                  ((:right-align t)
                                   (:help-echo "Upstream changes not in branch")))
                                 ("B>U"      3 magit-repolist-column-unpushed-to-upstream
                                  ((:right-align t)
                                   (:help-echo "Local changes not in upstream")))
                                 ("Path"    99 magit-repolist-column-path nil))))

(use-package magit-mode
  :config
  (setq magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(use-package ediff
  :config
  (setq ediff-window-setup-function #'ediff-setup-windows-plain))

(use-package magit-ediff
  :config
  (setq magit-ediff-dwim-show-on-hunks t))

(use-package magit-extras
  :config
  (setq magit-copy-revision-abbreviated t))

;; From https://github.com/dandavison/magit-delta (not part of magit itself)
;; Can be turned off temporarily via `magit-delta-mode' then `magit-refresh'.
(use-package magit-delta
  :hook (magit-mode . magit-delta-mode)
  :config
  ;; zenburn and Sublime Snazzy are also decent.
  (setq magit-delta-default-dark-theme "Monokai Extended")
  (setq magit-delta-hide-plus-minus-markers t))

(use-package forge
  :demand t
  :bind (:map magit-repolist-mode-map
         ("F" . cc/magit-repolist-forge-fetch)
         :map forge-repository-list-mode-map
         ("F" . cc/magit-repolist-forge-fetch)
         :map forge-topic-mode-map
         ("C-c t" . cc/forge-atlantis-comment))
  :preface
  (defun cc/magit-repolist-forge-fetch ()
    "Do `forge-pull' to update all known forge repos in `magit-list-repos'."
    (interactive)
    (run-hooks 'magit-credential-hook)
    (let* ((repos (magit-list-repos)))
      (dolist (repo repos)
        (let ((default-directory (file-name-as-directory repo)))
          (message "directory: %s" default-directory)
          (if (forge-get-repository nil)
              ;; It would be nice to have a way to tell when all these async `forge-pull' commands
              ;; finish in order to refresh to repo list afterwards, but oh well.
              (forge-pull)
            (message "Skipping repo: %s" repo))))))

  (defun cc/forge-global-list-pullreqs ()
    "Like `forge-list-pullreqs', but for all forges/repos."
    (interactive)
    (forge-topic-list-setup #'forge-pullreq-list-mode nil "All pullreqs"
                            forge-global-topic-list-columns
      (lambda ()
        (forge-sql [:select $i1
                    :from [pullreq repository]
                    :where (and (= pullreq:repository repository:id)
                                (isnull pullreq:closed))
                    :order-by [(asc repository:owner)
                               (asc repository:name)
                               (desc pullreq:number)]]
                   (forge--tablist-columns-vector 'pullreq)))))

  (defun cc/forge-global-list-authored-pullreqs ()
    "Like `forge-list-authored-pullreqs', but for all forges/repos."
    (interactive)
    (forge-topic-list-setup #'forge-pullreq-list-mode nil "Authored pullreqs"
                            forge-global-topic-list-columns
      (lambda ()
        (forge-sql [:select $i1
                    :from [pullreq repository]
                    :where (and (= pullreq:repository repository:id)
                                (isnull pullreq:closed)
                                (in pullreq:author $v2))
                    :order-by [(asc repository:owner)
                               (asc repository:name)
                               (desc pullreq:number)]]
                   (forge--tablist-columns-vector 'pullreq)
                   (vconcat (mapcar #'car forge-owned-accounts))))))

  (defun cc/forge-global-list-participating-pullreqs ()
    "List pullreqs in any forge/repo that I participated in but didn't author.

Note that this does NOT check 'reviewers', since Forge doesn't support that for Gitlab. See:
https://github.com/magit/forge/issues/243"
    (interactive)
    (forge-topic-list-setup #'forge-pullreq-list-mode nil "Participating pullreqs"
                            forge-global-topic-list-columns
      (lambda ()
        (forge-sql [:select :distinct $i1
                    :from [repository pullreq pullreq_assignee assignee pullreq_post]
                    :where (and (= pullreq:repository       repository:id)
                                (= pullreq_assignee:pullreq pullreq:id)
                                (= pullreq_assignee:id      assignee:id)
                                (= pullreq_post:pullreq     pullreq:id)
                                (isnull pullreq:closed)
                                (not (in pullreq:author $v2)) ; Using a separate view for my authored, so exclude.
                                (or (in assignee:login $v2)
                                    (in pullreq_post:author $v2)))
                    :order-by [(asc repository:owner)
                               (asc repository:name)
                               (desc pullreq:number)]]
                   (forge--tablist-columns-vector 'pullreq)
                   (vconcat (mapcar #'car forge-owned-accounts))))))

  (defun cc/forge-atlantis-comment ()
    "Add an Atlantis comment on a merge request."
    (interactive)
    (unless (derived-mode-p 'forge-topic-mode)
      (user-error "This only works when viewing a forge topic (aka merge request)"))
    (let* ((action (completing-read "Action: " '("plan" "apply")))
           (workspace (completing-read "Workspace: " (cc/forge-atlantis--workspaces)))
           (atlantis-user (if (string-match-p (rx (or "prod" "beta")) workspace)
                              "@atlantis-prod"
                            "@atlantis-nonprod"))
           (comment (format "%s %s -p %s" atlantis-user action workspace)))
      (forge-create-post)
      ;; Don't actually post the comment - let user confirm it.
      (insert comment)))

  (require 'yaml)
  (defun cc/forge-atlantis--workspaces ()
    ;; In magit/forge, `default-directory' is the project root.
    (let ((file (expand-file-name "atlantis.yaml")))
      (if (file-exists-p file)
          (progn
            (let* ((contents (with-temp-buffer
                               (insert-file-contents file)
                               (buffer-string)))
                   (parsed (yaml-parse-string contents)))
              (seq-map
               (lambda (project)
                 (gethash 'workspace project))
               (gethash 'projects parsed))))
        (user-error "Couldn't find atlantis config: %s" file))))

  :config
  (setq forge-database-file (expand-file-name "forge-database.sqlite" cc/user-cache))
  ;; Hide closed MRs initially, show with `forge-toggle-closed-visibility'
  (setq forge-topic-list-limit '(60 . -10))
  (setq forge-owned-accounts '(("chasecaleb") ("cchase"))))

(use-package git-timemachine
  :hook (git-timemachine-mode . cc/git-timemachine-setup)
  :preface
  ;; TODO Keep an eye on this gitlab issue - hopefully this becomes configurable.
  ;; https://gitlab.com/pidu/git-timemachine/-/issues/83
  (defun cc/git-timemachine-modify-id (&rest _args)
    (setq mode-line-buffer-identification
          (list (car mode-line-buffer-identification)
                "@"
                (nth 2 mode-line-buffer-identification)
                (nth 3 mode-line-buffer-identification) ; 3+4 are name
                (nth 4 mode-line-buffer-identification)
                (nth 5 mode-line-buffer-identification) ; 5 is whitespace
                ;; Remove redundant date from end - it's shown in minibuffer too.
                (replace-regexp-in-string
                 (rx " " (one-or-more (not ")")))
                 ""
                 (nth 6 mode-line-buffer-identification)))))
  :config
  (advice-add #'git-timemachine-show-revision :after #'cc/git-timemachine-modify-id)
  (defun cc/git-timemachine-setup ()
    ;; Shorten buffer name prefix.
    (rename-buffer (replace-regexp-in-string
                    (rx string-start "timemachine:")
                    "T:"
                    (buffer-name)))
    (setq-local spaceline-buffer-id-max-length 70))

  (setq git-timemachine-abbreviation-length 8))

(use-package git-link
  :config
  (setq git-link-open-in-browser t))

(defhydra cc/git-hydra (:color blue)
  ("ll" #'git-link "Line/region" :column "Links")
  ("lf" (let ((current-prefix-arg '-))
          (call-interactively #'git-link)) "File")
  ("lc" #'git-link-commit "Commit (hash)")
  ("lp" #'git-link-homepage "Project home")
  ("t" #'git-timemachine-toggle "Timemachine" :column "Buffer-specific")
  ("g" #'magit-file-dispatch "Magit: file dispatch")
  ("rm" #'magit-list-repositories "Magit: all" :column "Repos")
  ("rf" #'forge-list-repositories "Forge: all")
  ("pa" #'cc/forge-global-list-pullreqs "All":column "Pull Requests")
  ("pc" #'cc/forge-global-list-authored-pullreqs "Created by me")
  ("pp" #'cc/forge-global-list-participating-pullreqs "Participating")
  ("C-g" nil nil)
  ("RET" nil nil))
(global-set-key (kbd "C-c g") #'cc/git-hydra/body)

(use-package orgit
  :defer t)

(use-package smerge-mode
  :defer 1
  :after (hydra)
  :bind (:map smerge-mode-map
         ("C-c h" . cc/smerge-hydra/body))
  :hook (magit-diff-visit-file . cc/magit-diff-visit-smerge)
  :config
  (defun cc/magit-diff-visit-smerge ()
    (when smerge-mode
      (cc/smerge-hydra/body)))

  ;; Based on https://github.com/alphapapa/unpackaged.el#smerge-mode
  (defhydra cc/smerge-hydra
    (:color pink :hint nil :post (smerge-auto-leave))
    ("n" #'smerge-next "Next" :column "Move")
    ("p" #'smerge-prev "Prev")
    ("b" #'smerge-keep-base "Base" :column "Keep")
    ("u" #'smerge-keep-upper "Upper")
    ("l" #'smerge-keep-lower "Lower")
    ("a" #'smerge-keep-all "All")
    ("RET" #'smerge-keep-current "Current")
    ("<" #'smerge-diff-base-upper :column "Diff")
    ("=" #'smerge-diff-upper-lower)
    (">" #'smerge-diff-base-lower)
    ("R" #'smerge-refine)
    ("E" #'smerge-ediff)
    ("C" #'smerge-combine-with-next :column "Other")
    ("r" #'smerge-resolve)
    ("k" #'smerge-kill-current)
    ("Z" (lambda ()
           (interactive)
           (save-buffer)
           (bury-buffer))
     "Save and bury buffer" :color blue)
    ("C-g" nil nil))

  (set-face-attribute 'smerge-refined-changed nil :extend t)
  (set-face-attribute 'smerge-refined-removed nil :extend t)
  (set-face-attribute 'smerge-refined-added   nil :extend t))

(use-package diff-hl
  :demand 1
  :config
  (setq diff-hl-draw-borders nil)
  (global-diff-hl-mode)
  (diff-hl-flydiff-mode)
  (diff-hl-dired-mode)
  (add-hook 'magit-pre-refresh-hook 'diff-hl-magit-pre-refresh)
  (add-hook 'magit-post-refresh-hook 'diff-hl-magit-post-refresh))


;;; Communication

(use-package circe
  ;; `circe-chat-mode' applies to IRC queries/channels/DMs but not slack.
  :hook ((circe-chat-mode . cc/circe-chat-setup))
  :preface
  (defun cc/circe-chat-setup ()
    (setq wrap-prefix (make-string 19 ? ))
    (adaptive-wrap-prefix-mode -1) ;; This needs to be *off* for wrapping to work the way I want.
    (enable-circe-color-nicks))

  (defun cc/irc-start ()
    (interactive)
    ;; Defer this to avoid GPG prompt on startup
    (setq circe-network-options `(("Libera Chat"
                                   :tls t
                                   :nick "chasecaleb"
                                   :channels (:after-auth
                                              "#emacs"
                                              "#organice"
                                              "#nixos"
                                              "#nixos-emacs")
                                   :logging t
                                   :sasl-username "chasecaleb"
                                   :sasl-password ,(auth-source-pick-first-password
                                                    :host "irc.libera.chat"
                                                    :user "chasecaleb"
                                                    :user "cchase@nipr.com"))))
    (circe "Libera Chat"))

  :config
  ;; General circe config documentation: https://github.com/emacs-circe/circe/wiki/Configuration
  (setq circe-reduce-lurker-spam t)
  (setq circe-active-users-timeout (* 30 60))
  ;; Align nicks and messages
  (setq circe-format-say "{nick:+16s} | {body}")
  (setq circe-format-self-say circe-format-say)
  (dolist (channel '("#emacs")) ;; Don't ignore #organice, it's a small channel.
    ;; Don't care about tracking messages unless I'm mentioned specifically.
    ;; Add to list, not setq, because Slack also modifies tracking-ignored-buffers.
    (add-to-list 'tracking-ignored-buffers `(,(concat channel)
                                             circe-highlight-nick-face)))

  ;; I don't care about displaying lag time, but I still want `circe-lagmon-mode' for the sake of
  ;; auto-reconnecting in case of network failure.
  (circe-lagmon-mode))

(use-package lui
  :hook ((lui-mode . cc/lui-mode-setup))
  :preface
  (defun cc/lui-mode-setup ()
    ;; https://github.com/jorgenschaefer/circe/wiki/Configuration#fluid-width-windows
    (setq right-margin-width 19)
    (setq fringes-outside-margins t)
    (unless (local-variable-p 'wrap-prefix)
      (setq wrap-prefix ""))
    (setf (cdr (assoc 'continuation fringe-indicator-alist)) '(right-arrow nil))
    ;; Whitespace after fringe line continuation indicator for readability.
    (setq left-margin-width 1)
    (visual-line-mode))

  :config
  (setq lui-logging-directory (expand-file-name "circe-logs" cc/user-cache))

  (setq lui-time-stamp-format "[%a %m/%d %I:%M%p]")
  (setq lui-time-stamp-position 'right-margin)
  ;; See my lui hook - using wrapping instead.
  (setq lui-fill-type nil))

(use-package lui-track
  :config
  ;; May want to change this from bar to fringe, because bar is fairly obnoxious. TBD.
  ;; (setq lui-track-indicator 'fringe)
  (enable-lui-track))

(use-package circe-color-nicks
  :hook (circe-channel-mode . enable-circe-color-nicks)
  :config
  (setq circe-color-nicks-everywhere t)
  (setq circe-color-nicks-min-contrast-ratio 4.5)
  (setq circe-color-nicks-everywhere t))

(use-package alert
  :defer t
  :config
  (setq alert-default-style 'libnotify))


;;; Major modes/languages

;;; Major mode: Org
(require 'cc-org-utils)
(use-package org
  :bind (:map org-mode-map
         ("C-c M-." . org-time-stamp-inactive)
         ;; This is supposed to be the default org-force-cycle-archived, but it's broken.
         ("C-c <C-tab>" . org-force-cycle-archived)
         ([remap outline-up-heading] . #'cc/org-back-or-up-heading))
  :hook (org-mode . cc/org-hook)
  :preface
  (defun cc/org-back-or-up-heading (arg)
    (interactive "p")
    (if (org-at-heading-p)
        (outline-up-heading arg)
      (org-back-to-heading)))

  (defun cc/org-insert-todo-heading ()
    (interactive)
    (org-insert-todo-heading '(4)))

  (defun cc/org-insert-todo-heading-respect-content ()
    "Equivalent to calling `org-insert-todo-heading-respect-content' with one prefix argument.

Or in other words, inserts a new todo heading with initial 'todo'
state instead of copying the state of the current heading."
    (interactive)
    ;; '(4) is equivalent to calling with c-u.
    (org-insert-todo-heading-respect-content '(4)))

  (defun cc/org-link-make-description (link description)
    "Used with `org-link-make-description-function'"
    (if (equal description nil)
        (cond
         ((string-match (rx "jira.naic.org/browse/" (group (>= 4 (any alphanumeric "-")))) link)
          (match-string 1 link))
         ((string-match (rx "https://" (group (or "gitlab" "github")) ".com/" (group (+ anychar))) link)
          (format "%s:%s" (match-string 1 link) (match-string 2 link)))
         (t description))
      description))
  ;; Need to autoload org-load-modules-maybe for org-store-link to work.
  :commands org-load-modules-maybe
  :bind (("C-c l" . #'org-store-link)
         ([remap org-insert-todo-heading] . cc/org-insert-todo-heading)
         ([remap org-insert-todo-heading-respect-content] .
          cc/org-insert-todo-heading-respect-content))
  :config
  ;; Unbind org-agenda-file-to-front and org-remove-file to avoid modifying
  ;; org-agenda-files.
  (unbind-key "C-c [" org-mode-map)
  (unbind-key "C-c ]" org-mode-map)
  ;; Also unbind word wrap, since I use soft wrapping. For some reason that I can't figure out,
  ;; using unbind-key doesn't work for M-q. I realize that sounds insane, but I just spent 15
  ;; minutes trying to figure it out and got nowhere.
  (bind-key "M-q" #'ignore org-mode-map)

  (defun cc/org-hook ()
    (visual-line-mode t)
    (set-face-foreground 'org-headline-done "slate grey")
    (set-face-attribute 'org-level-1 nil :weight 'normal :height 1.2)
    (set-face-attribute 'org-level-2 nil :weight 'normal :height 1.1)
    (set-face-attribute 'org-level-3 nil :weight 'normal :height 1.0))

  (setq org-directory (expand-file-name "~/org-files"))

  ;; IMPORTANT NOTES for todo keywords:
  ;; - Order matters for e.g. agenda's todo-state-up/-down sorting.
  ;; - Make sure to keep TODO as the first of its sequence, otherwise I need to make sure that
  ;;   `org-todo-repeat-to-state' is set as well as who knows how many other similar variables.
  ;; - Todo keywords are also set as file-local variables using "#+TODO:" in my GTD org files,
  ;;   because that's the only way supported by organice. However I still want this global variable
  ;;   set for the sake of agenda usage, etc. I'm not sure how necessary this is, but I don't want
  ;;   to figure it out. So KEEP FILE-LOCAL VALUES IN SYNC.
  (setq org-todo-keywords
        '((sequence
           "TODO(t)" "WORKING(w)" "BLOCKED(b)" "PENDING(p)" "|"
           "DONE(d)" "CANCELED(c)")
          ;; "|" so that BACKLOG isn't treated as a completed keyword.
          (sequence "BACKLOG(l)" "|")))
  (setq org-todo-keyword-faces
        '(("TODO" . (:inherit org-priority)) ; org-priority is org-todo without the background
          ("BACKLOG" . (:foreground "slate gray" :weight bold))
          ("WORKING" . (:foreground "green" :weight bold))
          ("PENDING" . (:foreground "yellow" :weight bold))
          ("BLOCKED" . (:foreground "red" :weight bold))
          ("DONE" . (:foreground "slate gray" :weight bold))
          ("CANCELED" . (:foreground "slate gray" :weight bold))))
  (setq org-fontify-done-headline t) ; Because spacemacs-theme annoyingly changes this.
  (setq org-hierarchical-todo-statistics nil) ; nil = compute recursively
  (setq org-enforce-todo-dependencies t)
  (setq org-enforce-todo-checkbox-dependencies nil) ; Use this at one point but decided against it.
  (setq org-treat-insert-todo-heading-as-state-change nil)
  (setq org-log-done 'time)
  (setq org-log-into-drawer t)

  (setq org-tag-persistent-alist
        (append cc/org-tags-context-alist
                '(("urgent" . ?u)
                  ("delegated" . ?d)
                  ("bug" . ?b)
                  ("feature" . ?f)
                  ("maintenance" . ?m)
                  ("finances" . ?p)
                  ("web" . ?w)
                  ("family" . ?F))))

  (setq org-use-speed-commands t)
  (setq org-use-fast-tag-selection t)
  (setq org-fast-tag-selection-single-key nil)

  (setq org-priority-lowest 67)
  (setq org-priority-default org-priority-lowest)

  (setq org-global-properties
        '(("effort_ALL" . "0:05 0:15 0:30 1:00 2:00 4:00 8:00 16:00 24:00")))
  (setq org-effort-property "effort")

  (setq org-plain-list-ordered-item-terminator ?.)

  (setq org-startup-folded 'content)
  (setq org-blank-before-new-entry nil)
  (setq org-yank-adjusted-subtrees t)
  (setq org-catch-invisible-edits 'show-and-error)
  (setq org-ctrl-k-protect-subtree t)
  (setq org-adapt-indentation t)

  (setq org-columns-default-format "%TODO %3PRIORITY %80ITEM %TAGS")

  (setq org-confirm-babel-evaluate nil)
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (shell . t)
     (kubectl . t)
     (http . t)
     (awk . t)))

  (setq org-link-make-description-function #'cc/org-link-make-description)
  (dolist (type '("http" "https"))
    (org-link-set-parameters type :face '(:inherit org-link :foreground "medium sea green")))
  (org-link-set-parameters
   "file"
   :face (lambda (path)
           (cond
            ((file-remote-p path) '(:inherit org-link :foreground "dark orange"))
            ((not (file-exists-p (expand-file-name path))) '(:inherit org-link :foreground "red"))
            (t 'org-link))))
  (org-link-set-parameters "id" :face '(:inherit org-link :slant italic))

  (add-to-list 'org-modules 'org-habit))

(use-package org-habit
  :config
  (setq org-habit-graph-column 80))

(require 'all-the-icons)
(use-package org-agenda
  :demand t
  :hook ((org-agenda-finalize . cc/org-agenda-finalize)
         (org-agenda-mode . cc/org-agenda-maybe-filter-backlog-initially))
  :bind (("C-c a" . org-agenda)
         ("C-c p" . cc/org-gtd-process-start)
         :map org-agenda-mode-map
         ("i" . cc/org-agenda-switch-to-indirect)
         ("C-c b" . cc/org-agenda-filter-backlog))
  :preface
  (defun cc/clocktable-formatter-group-by-prop (ipos all-tables params)
    "Group by first :prop (e.g. category) of PARAMS and promote archived tasks to display normally."
    ;; TODO: refactor this
    ;;  - Split into multiple functions:
    ;;     1. grouping by prop (and associated label/display modifications)
    ;;     2. promoting/dealing with archived tasks
    ;;  - Consider adding getters (`defun') or getters + setters (`define-inline'? or `defun' +
    ;;    `gv-define-setter'?) for task/table lists.
    ;;         https://nullprogram.com/blog/2018/02/14/
    (let* ((result-table (make-hash-table :test #'equal))
           (maxlevel (plist-get params :maxlevel))
           (result-list nil)
           ;; Need to track archive state of previous task to distinguish the root "Archive" heading
           ;; from its children (which are the actual archived tasks).
           (prev-archive-p nil))
      (dolist (table all-tables)
        (dolist (task (nth 2 table)) ; task is '(level headline tags timestamp time properties)
          (let* ((task-key (cdr (car (car (last task)))))
                 (task-level (car task))
                 (task-tags (nth 2 task))
                 (task-time (nth 4 task))
                 (existing (gethash task-key result-table (list task-key 0 nil)))
                 (existing-time (nth 1 existing))
                 ;; If multiple top-level records (i.e. files) are merged, their times need to be
                 ;; combined as well. Example: if grouping by category and multiple files share the
                 ;; same category.
                 (merged-time (if (equal 1 task-level)
                                  (+ existing-time task-time)
                                existing-time))
                 (archive-heading-p (member org-archive-tag task-tags))
                 (archive-root-p (and (not prev-archive-p)
                                      archive-heading-p)))
            (when archive-heading-p
              ;; Promote to appear as a normal (unarchived) task.
              (cl-decf (car task)))
            (unless (or archive-root-p
                        (and maxlevel (> task-level maxlevel)))
              (puthash task-key
                       (list
                        task-key
                        merged-time
                        (cons task (car (last existing))))
                       result-table))
            (setq prev-archive-p archive-heading-p))
          result-table))
      ;; Reverse the list of tasks, since cons created it backwards. This is apparently the
      ;; idiomatic way to build a list from front to back in elisp, since cons is O(1) whereas
      ;; append is O(n) *for each operation*. Thus the whole runtime is O(n) for cons vs O(n^2) for
      ;; append. Of course worrying about big-O is absurd in a case like this where n will be fairly
      ;; small, but it's still the idiomatic way.
      (maphash (lambda (_key value)
                 (let ((last-index (1- (safe-length value))))
                   (setf (nth last-index value) (nreverse (nth last-index value)))
                   (push value result-list)))
               result-table)
      (let ((formatter (or org-clock-clocktable-formatter
                           #'org-clocktable-write-default))
            (group-name (capitalize (car (plist-get params :properties)))))
        ;; KLUDGE WARNING:
        ;; :properties and :tags are used by this formatter and not intended for display. This is a
        ;; heavy handed solution, but it works for my current use case. This may bite me in the
        ;; future though.
        (setf (plist-get params :properties) nil)
        (setf (plist-get params :tags) nil)
        (funcall formatter ipos result-list params)
        ;; "File" isn't the correct column title, so adjust it.
        ;; Reminder: Org tables are 1-indexed, just to make life confusing and painful.
        (org-table-put 1 1 group-name)
        (org-table-analyze) ;; Sets `org-table-dlines' (and others). Dynamic vars are fun.
        (seq-doseq (dl org-table-dlines)
          (when dl
            ;; dlines are 0-indexed, but org-table-goto-* functions are 1-indexed... *barfs*.
            (let ((line (1+ dl))
                  (col 2))
              (org-table-goto-line line)
              (org-table-goto-column col)
              (let ((field (org-table-get-field)))
                (when (string-match-p
                       (rx (any space) "*File time*" (any space))
                       field)
                  (org-table-put line col (format "*%s time*" group-name)))))))
        (org-table-align))))

  (defun cc/clocktable-maxlevel-args-advice (args)
    "Hack to adjust :maxlevel param for `cc/clocktable-formatter-group-by-prop'.

`cc/clocktable-formatter-group-by-prop' promotes archived sibling tasks to a
higher level to merge them in the clock report table, so it needs to receive an
extra level of tasks."
    (let* ((props (cadr args))
           (maxlevel (plist-get props :maxlevel)))
      (if (and maxlevel
               (equal (plist-get props :formatter) #'cc/clocktable-formatter-group-by-prop))
          (let ((ret (list (car args)
                           (plist-put (copy-sequence props) :maxlevel (1+ maxlevel)))))
            (message "new maxlevel: %s" (plist-get (cadr ret) :maxlevel))
            ret)
        args)))
  (advice-add #'org-clock-get-table-data :filter-args #'cc/clocktable-maxlevel-args-advice)

  (defun cc/org-agenda-filter-backlog ()
    "Toggle regexp filter to hide BACKLOG items."
    (interactive)
    (let* ((filter "-^ *BACKLOG")
           (filter-active (memq filter org-agenda-regexp-filter)))
      (if filter-active
          ;; I can't figure out how to clear just the one BACKLOG filter while keeping others, since
          ;; the agenda doesn't update with the ways that I've tried. Fortunately I don't use regexp
          ;; filters for other things frequently, so clearing them all is fine.
          (org-agenda-filter-show-all-re)
        (push filter org-agenda-regexp-filter)
        (org-agenda-filter-apply org-agenda-regexp-filter 'regexp))
      (message "Backlog: %s" (if filter-active "visible" "hidden"))))

  (defvar-local cc/org-agenda-filter-backlog-initially nil)
  (defun cc/org-agenda-maybe-filter-backlog-initially ()
    "Filter backlog when view is initially shown. Can be cleared like normal (e.g. '|' key).

You would think `org-agenda-regexp-filter-preset' would work like this, but it can't be cleared."
    (when cc/org-agenda-filter-backlog-initially
      (cc/org-agenda-filter-backlog)
      ;; So that filter isn't re-added when e.g. rebuilding view via "g"
      (setq-local cc/org-agenda-filter-backlog-initially nil)))

  (defun cc/org-agenda-finalize ()
    (org-agenda-forward-block)
    (org-agenda-next-item 1))

  (defun cc/org-agenda-switch-to-indirect (arg)
    "Remember follow mode (keybind: F) is also a thing in agenda view."
    (interactive "P")
    (funcall-interactively #'org-agenda-tree-to-indirect-buffer arg)
    (select-window (get-buffer-window org-last-indirect-buffer)))

  :config
  ;; It seems like there should be a way to do this that doesn't involve directly parsing the output
  ;; of find myself, but I couldn't figure anything out.
  (cc/when-org-files
   (dolist (dir (butlast ; last element is empty because find outputs "foo\nbar\n"
                 (split-string
                  (shell-command-to-string
                   "find ~/org-files/gtd -type d -not -wholename '*/.git*'")
                  "\n")))
     (add-to-list 'org-agenda-files dir)))

  (cl-flet ((get-icon (icon face)
                      (list (all-the-icons-material icon :face face :height 1.0))))
    (setq org-agenda-category-icon-alist
          `(("work" ,(get-icon "work" 'all-the-icons-blue-alt))
            ("professional" ,(get-icon "code" 'all-the-icons-lsilver))
            ("hobbies" ,(get-icon "favorite" 'all-the-icons-lred))
            ("misc" ,(get-icon "assignment" 'all-the-icons-yellow))
            ("journal" ,(get-icon "bookmark" 'all-the-icons-lgreen))
            ;; Empty string category is used for e.g. placeholder lines in time grid.
            (,(rx string-start string-end) (space . (:width (20))))
            ;; Key is a regexp, so this is the default for any other categories. Need to exclude
            ;; empty string because that's used as category for e.g. empty time-grid lines.
            (,(rx (one-or-more anything)) ,(get-icon "remove" nil)))))
  (setq org-agenda-prefix-format
        '((agenda . " %i %?-12t% s")
          (todo . " %i [%5e] ")
          (tags . " %i ")
          (search . " %i ")))
  ;; Add separator between days in agenda for readability.
  (setq org-agenda-format-date (lambda (date)
                                 (concat "\n"
                                         (make-string (window-width) 9472)
                                         "\n"
                                         (org-agenda-format-date-aligned date))))
  (setq org-agenda-clockreport-parameter-plist
        '(:link t :maxlevel 2 :fileskip0 t :emphasize t :compact nil :narrow nil :formula %
          :properties ("CATEGORY") :tags t ; props + tags needed by my formatter.
          :formatter cc/clocktable-formatter-group-by-prop))
  (setq org-agenda-block-separator nil) ; redundant since date has a separator now.
  (setq org-agenda-confirm-kill nil) ; This prompts gets old after the 100th time or so.
  (setq org-agenda-bulk-custom-functions '((?k org-agenda-kill)))

  (setq org-agenda-window-setup 'current-window)
  (setq org-agenda-restore-windows-after-quit nil)
  (setq org-agenda-start-with-follow-mode nil)
  (setq org-agenda-follow-indirect t)
  (setq org-agenda-start-with-log-mode nil)
  (setq org-agenda-start-on-weekday nil) ; Start today, not a constant day of the week
  (setq org-agenda-compact-blocks nil)

  (setq org-agenda-timegrid-use-ampm t)
  (setq org-agenda-todo-keyword-format "%-10s") ; fixed-width todo column
  (setq org-stuck-projects '("+LEVEL=1-TODO={.+}-CATEGORY=\"journal\""
                             ("TODO" "WORKING")
                             nil
                             "^* Other$"))

  (setq org-agenda-span 1)
  (setq org-agenda-skip-deadline-if-done t)
  (setq org-agenda-skip-scheduled-if-done t)
  (setq org-agenda-skip-scheduled-if-deadline-is-shown t)
  (setq org-deadline-warning-days 5)
  ;; ignore-scheduled: applies to todo views, not agenda. integer means ignore >= x days in future.
  (setq org-agenda-todo-ignore-scheduled nil)

  (setq org-agenda-sorting-strategy
        '((agenda habit-down time-up todo-state-down priority-down category-up)
          (todo todo-state-down priority-down category-up)
          (tags todo-state-down priority-down category-up)
          (search todo-state-down priority-down category-up))))

(use-package org-clock
  :demand t
  :bind ("s-c" . #'cc/org-clock-hydra)
  :hook (org-after-todo-state-change . cc/org-clock-in-if-working)
  :preface
  (defun cc/org-clock-goto-agenda-overview ()
    (interactive)
    (org-agenda nil "o")
    (org-agenda-clock-goto))

  (cc/dynamic-hydra cc/org-clock-hydra (:color blue :post (cc/file-auto-save-do-save)) ""
    (t . ("i" (funcall-interactively #'org-clock-in-last '(4)) "Clock in (recent)" :column "Clock changes"))
    ((derived-mode-p 'org-mode 'org-agenda-mode) .
     ("M-i"
      (if (derived-mode-p 'org-agenda-mode)
          (org-agenda-clock-in)
        (org-clock-in))
      "         (point)"))
    ((org-clocking-p) . ("o" #'org-clock-out "Clock out"))
    ((org-clocking-p) . ("x" #'org-clock-cancel "Cancel clock"))
    (t . ("r" #'org-resolve-clocks "Resolve (adjust) clock"))
    ((org-clocking-p) . ("e" #'org-clock-modify-effort-estimate "Modify effort"))
    (t . ("jr" (funcall-interactively #'org-clock-goto '(4)) "Goto: recent" :column "View"))
    ((org-clocking-p) . ("jj"  #'org-clock-goto "Goto: current (file)"))
    ((org-clocking-p) . ("ja" #'cc/org-clock-goto-agenda-overview "Goto: current (agenda)"))
    ((derived-mode-p 'org-mode) . ("d" #'org-clock-display "Display summaries"))
    ((cc/org-clock-ticket-p) . ("tt" #'cc/org-clock-insert-ticket-url "Insert (URL)" :column "Ticket"))
    ((cc/org-clock-ticket-p) . ("ti" #'cc/org-clock-insert-ticket "       (ID)")))

  (defun cc/org-clock-in-if-working ()
    (if (string= (org-get-todo-state) "WORKING")
        (org-clock-in)))

  (defun cc/org-clock-advice (fn &rest args)
    "Because showing a notification immediately when clocking in is a nuisance"
    (cc/with-advice #'org-notify :override (lambda (&rest _notify-args))
      (apply fn args)))

  :config
  (advice-add #'org-clock-in :around #'cc/org-clock-advice)
  (setq org-clock-persist-file (expand-file-name "org-clock-save.el" cc/user-cache))
  (setq org-clock-persist 'history)
  (setq org-clock-history-length 20)
  (setq org-clock-out-remove-zero-time-clocks t)
  (setq org-clock-report-include-clocking-task t)
  (setq org-clock-idle-time 15)
  (org-clock-persistence-insinuate))

(use-package org-duration
  :config
  ;; `org-duration-format' is used by e.g. clock report tables. I want to show durations exclusively
  ;; in hours, because trying to mentally multiply/divide by 24 when long durations are shown in
  ;; days (as is the default) is painful. I don't think of long durations in days, nor do most
  ;; people. Also, using "days" for durations really doesn't make sense because it could mean so
  ;; many things. 24 hours? 16ish waking hours? 8ish working hours? Exactly.
  (setq org-duration-format (remove '("d" . nil) org-duration-format)))

(use-package org-archive
  :bind (:map org-mode-map
         ;; Seems like using the default function should be the (ahem) default keybinding.
         ([remap org-archive-subtree] . #'org-archive-subtree-default)
         :map org-agenda-mode-map
         ([remap org-agenda-archive] . #'org-agenda-archive-default))
  :config
  ;; Archiving to a separate file makes it nearly impossible to work with historic data (e.g. in
  ;; agenda views and clock report tables). Of particular note:
  ;; - There isn't a built-in way to create a matching outline hierarchy in archive file (but it can
  ;;   at least be done).
  ;; - The ARCHIVE_OLPATH property is infuriatingly useless due to ambiguity with "/", which could
  ;;   either be a path separator or a literal slash character in the heading text.
  ;; - The archive file obviously won't be kept in sync with any edits to outline paths in the
  ;;   original file.
  ;; - There isn't a (sane) way to merge tasks from a flat archive file into a clockreport table.
  ;;   This could be mitigated by recreating the outline path hierarchy in the outline file, but
  ;;   then the previous problems apply here too.
  ;;
  ;; So TLDR: org mode is simultaneously awesome and infuriating, as usual.
  (setq org-archive-default-command #'org-archive-to-archive-sibling))

(use-package org-contrib
  :after (org)
  :config
  (add-to-list 'org-modules 'ol-man)
  (add-to-list 'org-modules 'org-expiry)
  (add-to-list 'org-modules 'org-checklist))

(use-package org-expiry
  :hook ((org-after-todo-state-change . cc/org-created-date-hook)
         (org-capture-prepare-finalize . cc/org-created-date-hook))
  :config
  (defun cc/org-created-date-hook ()
    "Adds created property EXCEPT during org-roam capture.

Don't want to add created date during org-roam capture because it creates a new file which may not
have a heading, so there's no way to add a heading property consistently. Also I just don't care
about this property for org roam files, since the creation date is included in the filename and I
can also check git history trivially, whereas using git history to figure out a todo creation is
painful due to how many times the todo item may have been refiled."
    (unless (and (fboundp 'org-roam-capture-p)
                 (org-roam-capture-p))
      (org-expiry-insert-created)))

  (setq org-expiry-inactive-timestamps t))

;; https://github.com/alphapapa/org-super-agenda
(use-package org-super-agenda
  :demand t
  :preface
  (defun cc/org-super-agenda-auto-category-path (item)
    (org-super-agenda--when-with-marker-buffer (org-super-agenda--get-marker item)
      (seq-reduce (lambda (a b) (concat a "/" b))
                  (org-get-outline-path)
                  (capitalize (org-get-category)))))

  (defun cc/org-super-agenda-future-habit-p (item)
    (org-super-agenda--when-with-marker-buffer (org-super-agenda--get-marker item)
      (and (org-is-habit-p)
           (when-let ((scheduled (org-entry-get (point) "SCHEDULED")))
             (> (round (org-time-string-to-seconds scheduled))
                (time-convert nil 'integer))))))

  (defun cc/cmp-date-property (prop)
    "Compare two `org-mode' agenda entries, `A' and `B', by some date property.

If a is before b, return -1. If a is after b, return 1. If they
are equal return nil.

From https://emacs.stackexchange.com/questions/26351/custom-sorting-for-agenda"
    #'(lambda (a b)
        (let* ((a-pos (get-text-property 0 'org-marker a))
               (b-pos (get-text-property 0 'org-marker b))
               (a-date (or (org-entry-get a-pos prop)
                           (format "<%s>" (org-read-date t nil "now"))))
               (b-date (or (org-entry-get b-pos prop)
                           (format "<%s>" (org-read-date t nil "now"))))
               (cmp (compare-strings a-date nil nil b-date nil nil)))
          (if (eq cmp t) nil (cl-signum cmp)))))

  (defun cc/org-agenda-time-prop (prop)
    ;; This is without a doubt the most ridiculous way to display a date in a different format of
    ;; any programming language I've seen.
    (if-let* ((created-str (org-entry-get (point) prop t))
              (created (org-read-date nil t created-str nil)))
        (format-time-string "%b %e, %Y" created)
      ""))

  (defun cc/org-agenda-create-project-views ()
    ;; Remove existing project views to be idempotent.
    (cl-delete-if (lambda (it) (string-prefix-p "p" (car it)))
                  org-agenda-custom-commands)
    (push '("p" . "Projects") org-agenda-custom-commands)
    (push '("p." "All projects (planning)" todo ""
            ((org-super-agenda-groups
              `((:discard (:category "inbox"))
                (:discard (:pred cc/org-super-agenda-future-habit-p))
                (:auto-map cc/org-super-agenda-auto-category-path)))
             (cc/org-agenda-filter-backlog-initially t)))
          org-agenda-custom-commands)
    (cc/when-org-files
     (save-window-excursion
       (with-no-new-buffers
        (dolist (file (sort (cc/get-org-files "gtd/areas") #'string-greaterp))
          (with-current-buffer (find-file-existing file)
            (goto-char 0)
            (let* ((category (org-get-category))
                   ;; Warning: naively assumes every category starts with a unique letter.
                   (key (concat "p" (substring category 0 1)))
                   (title (concat "Projects: " category)))
              (push `(,key ,title tags-todo ,(format "CATEGORY=\"%s\"" category)
                           ((org-super-agenda-groups
                             '((:discard (:pred cc/org-super-agenda-future-habit-p))
                               (:auto-parent t)))
                            (org-agenda-overriding-header ,(concat "Projects: " category))
                            (cc/org-agenda-filter-backlog-initially t)))
                    org-agenda-custom-commands))))))))

  (defun cc/org-agenda-create-historic-views ()
    (cl-delete-if (lambda (it) (string-prefix-p "h" (car it)))
                  org-agenda-custom-commands)
    (push '("h" . "Historic Logs") org-agenda-custom-commands)
    ;; Yeah I know, a month != 30 days, but it works well enough. Unfortunately I can't use an
    ;; actual 'month span here, since I need to calculate the start day offset.
    (dolist (config '((:name "month" :key "m" :span 30)
                      (:name "week" :key "w" :span 7)
                      (:name "day" :key "h" :span 1)))
      (let ((key (concat "h" (plist-get config :key)))
            (name (concat "Historic Log: " (plist-get config :name)))
            (start-day (format "-%sd" (1- (plist-get config :span)))))
        (push `(,key ,name agenda ""
                     ((org-agenda-overriding-header ,name)
                      (org-super-agenda-groups
                       '((:name "Journal" :category "journal")
                         (:name "Closed" :log closed)
                         (:name "Clocked" :log clocked)
                         ;; State changes only shown after "vL" agenda keybind
                         (:name "State changes" :log t)
                         (:discard (:anything t))))
                      (org-agenda-start-with-log-mode '(closed clock))
                      (org-agenda-start-with-clockreport-mode t)
                      (org-agenda-archives-mode t)
                      (org-agenda-span ,(plist-get config :span))
                      (org-agenda-start-day ,start-day)))
              org-agenda-custom-commands))))

  (defun cc/org-actionable-todo-p (&optional additional-states)
    (let ((state (org-get-todo-state)))
      (and
       (seq-some (lambda (s) (string= state s))
                 (cons "TODO" additional-states))
       (not (org-is-habit-p)))))

  (defun cc/org-count-prev-sibling-todos ()
    (let ((count 0))
      (save-excursion
        (while (org-goto-sibling t)
          (when (cc/org-actionable-todo-p)
            (cl-incf count))))
      count))

  (defun cc/org-prev-sibling-todos-p ()
    (let ((found nil))
      (save-excursion
        (while (and (not found)
                    (org-goto-sibling t))
          (when (cc/org-actionable-todo-p)
            (setq found t))))
      found))

  (defun cc/org-agenda-skip-gtd-siblings ()
    (unless (and (cc/org-actionable-todo-p (remove "BACKLOG" org-not-done-keywords))
                 (not (cc/org-prev-sibling-todos-p)))
      (or (outline-next-heading)
          (goto-char (point-max)))))

  :config
  (setq org-super-agenda-groups nil) ; Don't use for normal agenda views.

  ;; `org-agenda-custom-commands' format is... fun. Note that there are two different places
  ;; to modify variables for individual parts of a composite view vs the entire set, as
  ;; explained in the docstring and illustrated here:
  ;; https://orgmode.org/list/C2B9E4DB-138E-4E3E-83C6-57E7ADD2D5F7@gmail.com/
  (setq org-agenda-custom-commands
        '(("o" "Overview"
           ((alltodo "" ((org-super-agenda-groups
                          '((:name "Urgent"
                             :and (:tag "urgent" :not (:scheduled future)))
                            (:name "WIP"
                             :todo "WORKING")
                            (:name "Idle"
                             :and (:todo ("BLOCKED" "PENDING") :not (:scheduled future)))
                            (:discard (:anything t))))))
            (agenda "" ((org-agenda-overriding-header "")
                        (org-agenda-span 3)
                        (org-super-agenda-groups
                         '((:discard (:pred cc/org-super-agenda-future-habit-p))
                           (:name "Habits"
                            :order 2
                            :habit t)
                           (:name "Overdue"
                            :order 0
                            :deadline past
                            :scheduled past)
                           (:name "Day"
                            :order 1
                            :time-grid t
                            :deadline today
                            :scheduled today)
                           (:name "Upcoming"
                            :order 3
                            :deadline future
                            :scheduled future)
                           (:name "Other"
                            :order 4
                            :anything t)))))))
          ("u" "Upcoming" todo ""
           ((org-super-agenda-groups
             `((:name "Deadlines: Overdue" :deadline past )
               (:name "Deadlines: Today" :deadline today)
               (:name "Scheduled: Overdue" :scheduled past)
               (:name "Scheduled: Today" :scheduled today)
               (:auto-planning)
               (:discard (:anything t))))))
          ("n" . "Next")
          ("nn" "Next: project heads (GTD style)" todo ""
           ((org-super-agenda-groups
             `((:discard (:category "inbox"))
               (:auto-map cc/org-super-agenda-auto-category-path)))
            (org-agenda-skip-function #'cc/org-agenda-skip-gtd-siblings)))
          ("n@" "Next: @context" todo ""
           ((org-super-agenda-groups
             `((:discard (:category "inbox"))
               (:discard (:pred cc/org-super-agenda-future-habit-p))
               ;; Dynamically generate @context tag groups
               ,@(seq-map
                  (lambda (next)
                    (let ((context (if (stringp next)
                                       next
                                     (car next))))
                      `(:name ,context :tag ,context)))
                  cc/org-tags-context-alist)))
            (cc/org-agenda-filter-backlog-initially t)))
          ("nc" "Next: category" todo ""
           ((org-super-agenda-groups
             `((:discard (:category "inbox"))
               (:discard (:pred cc/org-super-agenda-future-habit-p))
               (:auto-category t)))
            (cc/org-agenda-filter-backlog-initially t)))
          ("ne" "Next: effort" todo ""
           ((org-super-agenda-groups
             `((:discard (:category "inbox"))
               (:discard (:pred cc/org-super-agenda-future-habit-p))
               ,@(seq-map
                  (lambda (value)
                    ;; +1 because there isn't an effort-less-than-or-equal selector.
                    `(:name ,(format "<= %s minutes" value)
                      :effort< ,(number-to-string (1+ value))))
                  '(5 15 30 60 120 240))))
            (cc/org-agenda-filter-backlog-initially t)))
          ("r" "Review completed"
           ((todo "DONE|CANCELED"
                  ((org-agenda-cmp-user-defined (cc/cmp-date-property "CLOSED"))
                   (org-agenda-sorting-strategy '(user-defined-up))
                   (org-agenda-prefix-format '((todo . " %i %(cc/org-agenda-time-prop \"CLOSED\") [%5e] ")))
                   (org-agenda-todo-list-sublevels nil)
                   (org-super-agenda-groups
                    `((:auto-category t)))))))
          ("c" "By created date" todo ""
           ((org-agenda-cmp-user-defined (cc/cmp-date-property "CREATED"))
            (org-agenda-sorting-strategy '(user-defined-down))
            (org-agenda-prefix-format '((todo . " %i %(cc/org-agenda-time-prop \"CREATED\") ")))
            (org-super-agenda-groups '((:name "Created date" :anything t)))))
          ("b" . "Buffer (occur trees)")
          ("bd" "Buffer: done/canceled" occur-tree "^\\*+ +\\(DONE\\|CANCELED\\)")
          ("bl" "Buffer: backlog" occur-tree "^\\*+ +BACKLOG")
          ("bb" "Buffer: active" occur-tree "^\\*+ +\\(TODO\\|WORKING\\|BLOCKED\\|PENDING\\)")))

  ;; Re-run this to regenerate if a category is added/removed.
  (cc/org-agenda-create-project-views)
  (cc/org-agenda-create-historic-views)
  (org-super-agenda-mode))

(use-package org-list
  :defer t
  :preface
  (defun cc/org-toggle-item-fix-point (fn &rest args)
    "Fix quirk with `org-toggle-item': prevent going to beginning of line if item is empty."
    (cl-flet ((at-bol-p () (equal (point) (point-at-bol))))
      (let ((start-at-bol (at-bol-p)))
        (apply fn args)
        (when (and (not start-at-bol)
                   (at-bol-p))
          (goto-char (point-at-eol))))))
  :config
  (advice-add #'org-toggle-item :around #'cc/org-toggle-item-fix-point)

  (setq org-list-allow-alphabetical t))

(use-package org-refile
  :defer t
  :preface
  (require 'seq)
  (defun cc/org-refile-sanitize-history (&rest ignored)
    "Strip trailing slashes in `org-refile-history' to prevent duplicates.

Related bug (see whole thread):
https://www.mail-archive.com/emacs-orgmode@gnu.org/msg124695.html"
    (setq org-refile-history
          (seq-map (lambda (item)
                     (replace-regexp-in-string (rx (1+ "/") string-end) "" item))
                   org-refile-history)))
  :config
  (advice-add #'org-refile :before #'cc/org-refile-sanitize-history)
  ;; For more complex refile targeting, use `org-refile-target-verify-function'.
  (cc/when-org-files
   (setq org-refile-targets
         `((,(cc/get-org-files "gtd") . (:todo . "fake-todo-force-top-level"))
           (,(cc/get-org-files "gtd/areas") . (:maxlevel . 1)))))
  ;; 'file is the only way to allow refiling as a top-level item.
  (setq org-refile-use-outline-path 'file)
  (setq org-outline-path-complete-in-steps nil))

(use-package doct
  :defer t
  :config
  ;; Use doct with `org-roam-capture-templates'
  ;; https://github.com/progfolio/doct/issues/16#issuecomment-633822107
  (defun +doct-org-roam (groups)
    (let (converted)
      (dolist (group groups)
        (let* ((props (nthcdr (if (= (length group) 4) 2 5) group))
               (roam-properties (plist-get (plist-get props :doct) :org-roam)))
          (push `(,@group ,@roam-properties) converted)))
      (setq doct-templates (nreverse converted))))
  (setq doct-after-conversion-functions '(+doct-org-roam)))

(use-package org-capture
  :bind (("C-c c" . org-capture))
  :preface
  ;; Stop org-capture from messing with my window layout.
  ;; https://stackoverflow.com/questions/54192239/open-org-capture-buffer-in-specific-window
  (defun cc/org-capture-place-template-fix-windows (fn &rest args)
    (cl-letf (((symbol-function 'delete-other-windows) 'ignore))
      (apply fn args)))

  (defun cc/org-capture-prepare-finalize ()
    "Cleanup formatting/align after capture."
    (indent-region (buffer-end 0) (buffer-end 1))
    (org-align-tags t))

  (defun cc/org-capture-after-finalize ()
    "Hack to fix org quirk with `auto-revert-mode'.

Auto revert mode triggers after capture as expected, except that the newly
capture heading won't be visible if the previous heading is folded and has a
:PROPERTIES: section. This is probably an org bug that should be reported,
but... one day.

This was reverse-engineered from deep within `org-capture-goto-last-stored'. Of
particular interest are the calls to `bookmark-jump' and (via hook)
`org-bookmark-jump-unhide'."
    (when-let* ((buffer (marker-buffer org-capture-last-stored-marker)))
      (when (buffer-live-p buffer)
        (with-current-buffer buffer
          (goto-char (marker-position org-capture-last-stored-marker))
          (org-show-context)))))

  :config
  (advice-add 'org-capture-place-template
              :around #'cc/org-capture-place-template-fix-windows)

  (setq org-capture-templates
        (doct
         `((:group "Inbox"
            :file "~/org-files/gtd/inbox.org"
            :prepare-finalize cc/org-capture-prepare-finalize
            :after-finalize cc/org-capture-after-finalize
            :children
            (("Task"
              :keys "t"
              :template ("* TODO %?"))
             ("[Work] Jira ticket"
              :keys "j"
              :template ("* TODO [[https://jira.naic.org/browse/%^{Ticket}][%\\1]]: %? :ticket:"))))
           ("[Work] Meeting"
            :keys "m"
            :id "b920c2a8-1ef0-44a6-8e67-c3f4a1ff41b3"
            :jump-to-captured t
            :template ("* %?\n  %T"))))))

(use-package org-attach
  :config
  (setq org-attach-id-dir (expand-file-name "org-attach" org-directory))
  (setq org-attach-dir-relative t)
  (setq org-attach-store-link-p 'attached))

(use-package org-src
  :defer t
  :config
  (setq org-src-window-setup 'split-window-below))

(use-package ob-async
  :demand t)

;; https://www.orgroam.com/manual/
(use-package org-roam
  :demand t
  :bind ((("C-c n f" . org-roam-node-find)
          ("C-c n c" . org-roam-capture)
          ("C-c n g" . org-roam-graph)
          ("C-c n i" . org-roam-node-insert)
          ("C-c n l" . org-roam-buffer-toggle)))
  :preface
  (defun cc/org-roam-title-to-slug (title)
    "Advice for `org-roam-node-slug' to use hyphens instead of underscores. Yay advice."
    (replace-regexp-in-string "_" "-" title))
  :config
  (setq org-roam-directory "~/org-files/roam")
  ;; Don't want db file in my org directory, which is synced across computers.
  (setq org-roam-db-location (expand-file-name "org-roam.db" cc/user-cache))
  (advice-add #'org-roam-node-slug :filter-return #'cc/org-roam-title-to-slug)

  ;; TODO convert to doct
  (setq org-roam-capture-templates
        '(("d" "default" plain "%?"
           :if-new (file+head "%<%Y%m%d%H%M%S>-${slug}.org"
                              "#+title: ${title}\nSee also:")
           :unnarrowed t)))

  (setq org-roam-node-display-template "${title}")

  ;; Only files should be org-roam nodes (not headings with an ID property).
  (setq org-roam-db-node-include-function
        (lambda ()
          (equal nil (org-get-previous-line-level))))

  ;; Activate after init to avoid crashing during Nix build's byte compilation.
  (add-hook 'after-init-hook #'org-roam-db-autosync-mode))

(use-package org-roam-graph
  :demand t
  :config
  (setq org-roam-graph-extra-config '(("rankdir" . "RL")))
  ;; The graph is wayyyy too noisy if these are shown. I want to see relationships between notes,
  ;; not all the things in the outside world that I reference/link to.
  (setq org-roam-graph-link-hidden-types '("file" "http" "https" "help" "info" "man")))

(use-package org-roam-protocol
  :demand t)

(use-package org-id
  :demand t
  :after (org-roam)
  :config
  (setq org-id-link-to-org-use-id 'create-if-interactive)
  (setq org-id-locations-file (expand-file-name "org-id-locations" cc/user-cache))
  (cc/when-org-files
   (setq org-id-extra-files (directory-files-recursively org-roam-directory ".org$\\|.org.gpg$"))))

;; Reminder: use M-x `calendar' or `org-journal-search' to go to/view specific journal entry dates.
;; Enhancement: see `org-journal-enable-cache' *only if* calendar performance becomes an issue.
(use-package org-journal
  :demand t
  :bind (("C-c j j" . org-journal-new-entry)
         ("C-c j s" . org-journal-search))
  :preface
  (defun cc/org-journal-after-entry ()
    (save-excursion
      (forward-line)
      (newline)
      (insert "   " (format-time-string (org-time-stamp-format t)))
      (newline))
    (org-set-tags-command))

  (defun cc/org-journal-add-to-agenda ()
    "Add most recent journal files to agenda (for when I forget what I was thinking a minute ago).

Similar to `org-journal-enable-agenda-integration', but doesn't require visiting/saving a journal
file to activate."
    ;; Remove any journal files from agenda. Not strictly necessary, but prevents inconsistent
    ;; results/bloated list if e.g. Emacs stays open for several days.
    (setq org-agenda-files
          (seq-filter
           (lambda (element)
             (or (file-directory-p element)
                 (not (file-in-directory-p element org-journal-dir))))
           org-agenda-files))

    ;; Add most recent non-future journal files (e.g. today and yesterday, typically).
    (when (and (file-directory-p org-journal-dir) (file-exists-p org-journal-dir))
      (let* ((limit 2) ;; Limit to most recent N files
             (today (org-journal--get-entry-path)) ;; No sane way around using an internal function.
             (all-files (directory-files org-journal-dir t (rx (or ".org" ".org.gpg") string-end) t))
             (non-future (seq-filter
                          (lambda (f)
                            (or (string-equal f today)
                                (string-lessp f today)))
                          all-files)))
        (dolist (agenda-file (seq-take (seq-sort #'string-greaterp non-future) limit))
          (add-to-list 'org-agenda-files agenda-file)))))

  (defun cc/org-journal-setup ()
    (add-hook 'after-save-hook #'cc/org-journal-add-to-agenda))

  :hook ((org-journal-after-entry-create . cc/org-journal-after-entry)
         (org-journal-mode . cc/org-journal-setup))
  :init
  (setq org-journal-prefix-key "C-c j ") ; Has to be set during init
  :config
  ;; org-journal binds =C-c C-j= globally, which is supposed to be reserved for major modes.
  (let* ((key "C-c C-j")
         (command (lookup-key (current-global-map) (kbd key))))
    (when (eq command #'org-journal-new-entry)
      (global-unset-key (kbd key))))

  ;; I have a .dir-locals setting journal-specific tags
  (put 'org-current-tag-alist 'safe-local-variable #'listp)

  (setq org-journal-dir "~/org-files/journal")
  (setq org-journal-file-format "%Y%m%d.org")
  (setq org-journal-date-format "%A, %x")
  (setq org-journal-time-format "") ; Using an actual org timestamp after header instead.
  (setq org-journal-find-file 'find-file)

  ;; Don't want to wait for journal save hook to trigger the first time.
  (cc/org-journal-add-to-agenda))

(use-package todoist
  :defer t ; To avoid GPG prompt from auth-source until necessary
  :config
  (setq todoist-token (auth-source-pick-first-password
                       :host "todoist.com")))

(use-package ob-http
  :config
  (setq ob-http:remove-cr t))

;;; LSP (Language Server Protocol)
;; https://emacs-lsp.github.io/lsp-mode/
(use-package lsp-mode
  :defer t
  :hook (lsp-configure . cc/lsp-mode-config)
  :preface
  (defun cc/lsp-mode-config ()
    (lsp-headerline-breadcrumb-mode))

  (setq lsp-keymap-prefix "s-;")
  :config
  (setq lsp-session-file (expand-file-name "lsp-session" cc/user-cache))
  (setq lsp-server-install-dir (expand-file-name "lsp-server" cc/user-cache))
  (setq lsp-semantic-tokens-enable t)
  (setq lsp-auto-execute-action nil))

(use-package lsp-eslint
  :defer t
  :config
  ;; Wrapper script in my nixos-config, since the eslint server is a vscode extension that's hard to
  ;; locate otherwise (it's in the package's /share so it doesn't get linked to
  ;; /run/current-system/sw).
  (setq lsp-eslint-server-command (list "node-eslint-wrapper" "--stdio")))

(use-package lsp-modeline
  :after lsp-mode
  :config
  (setq lsp-modeline-diagnostics-scope :project))

(use-package lsp-ui
  :after lsp-mode
  :bind (:map lsp-ui-mode-map
         ([remap xref-find-definitions] . lsp-ui-peek-find-definitions)
         ([remap xref-find-references] . lsp-ui-peek-find-references)
         ;; glance doesn't seem to work.
         ([remap lsp-ui-doc-glance] . lsp-ui-doc-show))
  :config
  (setq lsp-ui-doc-show-with-cursor nil) ; Activate manually via `lsp-ui-doc-show' instead.
  (setq lsp-ui-doc-position 'at-point))

;;; Major mode: PDF
;; https://github.com/politza/pdf-tools/
;; M-x `pdf-tools-help', but of particular note:
;; - "SPC"/"S-SPC" or "n"/"p" for next/prev page.
;; - Zoom: "P" to fit to page, "+" and "-" to zoom in/out
;; - Outline: imenu or "o" for outline in separate buffer
;; - Search with occur
(use-package pdf-tools
  :config
  (pdf-tools-install t))

(use-package org-pdftools
  :hook (org-mode . org-pdftools-setup-link))

;;; Major modes: Java/Kotlin
;; TODO: lsp-java

(use-package kotlin-mode
  :defer t
  :hook (kotlin-mode . lsp-deferred))


;;; Major mode: Go (aka Golang)
(use-package go-mode
  :hook (go-mode . cc/go-mode-setup)
  ;; TODO need to (require 'lsp-go) to fix this. Should extract all my LSP language config to a
  ;; separate file, e.g. cc-lsp-config.el.
  :defines lsp-go-hover-kind lsp-go-analyses
  :preface
  (defun cc/go-mode-setup ()
    (lsp-deferred)
    (add-hook 'before-save-hook #'gofmt-before-save nil t))
  :config
  ;; goimports adjusts imports plus does gofmt.
  (setq gofmt-command "goimports")

  ;; Despite misleading name, `lsp-go-hover-kind' also affects other documentation commands such as
  ;; `lsp-describe-thing-at-point' and `lsp-ui-doc-show'
  (setq lsp-go-hover-kind "FullDocumentation")
  ;; For analyzer info, see:
  ;; - https://github.com/golang/tools/blob/master/gopls/doc/settings.md#analyses
  ;; - https://github.com/golang/tools/blob/master/gopls/doc/analyzers.md
  ;; This is combined with the default analyzer config, so anything not specified here will use its
  ;; default. So basically I'm adding additional analyzers here.
  (setq lsp-go-analyses '((nilness . t)
                          (shadow . t)
                          (unusedparams . t)
                          (unusedwrite . t)
                          (implementmissing . t))))

;;; Major mode: JavaScript/TypeScript

;; LSP doesn't have syntax highlighting, so typescript-mode takes care of that. Syntax highlighting
;; seems like it would be a fundamental part of the LSP protocol, but apparently not. Oh well.
(use-package typescript-mode
  :defer t
  :mode ("\\.ts\\'" "\\.tsx\\'")
  :hook (typescript-mode . lsp-deferred))

;; js/jsx mode
(with-eval-after-load 'js
  (defvar js-mode-map nil) ; Byte compiler is dumb
  ;; js keymap is useless to me, because it e.g. rebinds M-. which I use with lsp.
  (setq js-mode-map (make-sparse-keymap)))
(add-hook 'js-mode-hook #'lsp-deferred)

;; This is https://github.com/jscheid/prettier.el, not the official-but-inferior prettier-js.
(use-package prettier
  :defer t
  :diminish
  :hook ((web-mode . cc/prettier-mode-maybe)
         (css-mode . cc/prettier-mode-maybe)
         (typescript-mode . cc/prettier-mode-maybe)
         (js-mode . cc/prettier-mode-maybe))
  :preface
  (defun cc/prettier-mode-maybe ()
    "Prettier kills performance when editing org src blocks in org buffer.

Editing them in a src block buffer, on the other hand, is fine."
    ;; Org src blocks don't have a file name. Probably a better way to figure this out but idk how
    ;; and don't want to spend any more time on it.
    (when buffer-file-name
      (prettier-mode))))

;;; Major mode: HTML
(use-package web-mode
  :mode "\\.html?\\'"
  :config
  (setq web-mode-attr-indent-offset 4))

;;; Major modes: YAML/Docker/Kubernetes/related
(use-package yaml-mode
  :mode ((rx ".yml" eos) (rx "Kptfile" eos))
  :config
  ;; Include non-alphabetic entries (e.g. .hidden-anchors) in imenu.
  ;; Taken from this PR (ignored by maintainer):
  ;; https://github.com/yoshiki/yaml-mode/pull/89
  (setq yaml-imenu-generic-expression
        '((nil  "^\\(:?[^{}(),\s \n]+\\):" 1)
          ("*Anchors*" "^\\s *[^{}(),\s \n]+: \\&\\([^{}(),\s \n]+\\)" 1)))

  ;; `yaml-indent-line' does atrocious things when applied to a region/buffer.
  (add-to-list 'cc/indent-region-disabled-modes 'yaml-mode))

;; json-mode is a dependency of dockerfile-mode (and maybe others), but I also use it directly some.
;; json files is the obvious use case, but I also use it with org-babel source blocks for api
;; request/response data.
(use-package json-mode)

;; TODO: k8s-mode (k8s yaml)

;; Kubel doesn't have an info page, but doc is here: https://github.com/abrochard/kubel
(use-package kubel
  :bind (("C-c k" . cc/hydra-kubel/body))
  :preface
  (defun cc/aws-sso ()
    (interactive)
    (start-process-shell-command "aws-sso" nil
                                 "aws sso login"))
  :config
  (defhydra cc/hydra-kubel (:color blue)
    ("k" kubel "Kubel" :column "Kubel")
    ("r" kubel-set-resource        "Resource")
    ("c" kubel-set-context         "Context")
    ("n" kubel-set-namespace       "Namespace")
    ("l"  cc/aws-sso               "AWS login" :column "Misc"))
  ;; kubel-use-namespace-list's default value ('auto) doesn't work quite right and likely won't be
  ;; fixed anytime soon due to kubectl edge cases. See:
  ;; https://github.com/abrochard/kubel/pull/44
  (setq kubel-use-namespace-list 'on))

(use-package docker
  :bind ("C-c d" . docker)
  :config
  (setq docker-run-as-root t))

(use-package dockerfile-mode
  :mode ("Dockerfile\\'" . dockerfile-mode))

(use-package terraform-mode
  :hook (terraform-mode . lsp-deferred))

;;; Major mode: conf
(use-package conf-mode
  :hook (conf-mode . cc/conf-mode-hook)
  :config
  (defun cc/conf-mode-hook ()
    (setq-local tab-width 4)))

;;; Major mode: log files
(progn
  (add-to-list 'auto-mode-alist '("\\.log\\'" . auto-revert-tail-mode))
  (add-to-list 'auto-mode-alist '("\\.log\\'" . read-only-mode)))

;;; Major mode: markdown
;; Book/guide to markdown-mode: https://leanpub.com/markdown-mode/read
(use-package markdown-mode
  :mode ("\\.md\\'" . gfm-mode)
  :hook (markdown-mode . auto-fill-mode)
  :config
  (setq markdown-command '("pandoc" "--quiet" "--from=gfm" "--to=html5"))
  (setq markdown-live-preview-delete-export 'delete-on-export)
  (setq markdown-ordered-list-enumeration nil)
  (setq markdown-italic-underscore t)
  (setq markdown-asymmetric-header t))

;;; Major mode: shell

(progn
  (add-to-list 'auto-mode-alist '("\\.holoscript\\'" . sh-mode))
  (add-to-list 'auto-mode-alist '("/PKGBUILD$" . sh-mode))
  (add-to-list 'auto-mode-alist '("\\.install\\'" . sh-mode)))

(use-package sh-script
  :defer 1
  :hook (sh-mode . cc/sh-mode-hook)
  :config
  (defun cc/sh-mode-hook ()
    (lsp-deferred) ; Uses bash-language-server (system package)
    (sh-electric-here-document-mode -1) ; This is a horrible default thanks to e.g. <<< syntax
    (add-hook 'after-save-hook #'executable-make-buffer-file-executable-if-script-p nil 'local))

  ;; I use ZSH for interactive terminal session, but always script in Bash.
  (setq sh-shell-file "/usr/bin/bash")
  (setq sh-indent-after-continuation 'always))

;;; Major mode: nix
(use-package nix-mode
  :hook (nix-mode . lsp-deferred)
  :bind (:map nix-mode-map
         ("C-c h" . nix-flake)
         ("<f12>" . lsp-format-buffer)))

(with-eval-after-load 'lsp-mode
  (add-to-list 'lsp-language-id-configuration '(nix-mode . "nix"))
  (lsp-register-client
   (make-lsp-client :new-connection (lsp-stdio-connection '("rnix-lsp"))
                    :major-modes '(nix-mode)
                    :server-id 'nix)))

(provide 'cc-kitchen-sink)
