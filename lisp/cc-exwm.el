;;; -*- lexical-binding: t; byte-compile-warnings: (not docstrings)-*-
;;; Disable docstring warnings because of Hydra.
(require 'subr-x)
(require 'exwm)
(require 'exwm-randr)
(require 'exwm-edit)
(require 'hydra)
(require 'cc-exwm-dunst)
(require 'cc-panel)
(require 'cc-utils)
(require 'cc-appearance)
(require 'cc-exwm-utils)

(defun cc/exwm-init ()
  (add-hook 'exwm-mode-hook #'cc/exwm--mode-setup)
  (add-hook 'exwm-manage-finish-hook #'cc/exwm--simulation-keys)
  (add-hook 'exwm-update-title-hook #'cc/exwm--rename-buffer)
  (add-hook 'exwm-init-hook #'cc/exwm--init-callback)
  (add-hook 'exwm-randr-screen-change-hook #'cc/randr--change)
  (add-hook 'after-init-hook #'cc/randr--change)

  (cc/define-key-list exwm-mode-map
                      `(("C-q" .     ,#'exwm-input-send-next-key)
                        ("C-c '" .   ,#'exwm-edit--compose)
                        ("C-c C-'" . ,#'exwm-edit--compose)))
  (global-set-key [remap save-buffers-kill-terminal] #'cc/exwm-exit-hydra/body)

  (cc/csetq exwm-input-global-keys
            `((,(kbd "s-w") . exwm-workspace-switch)
              (,(kbd "s-k") . exwm-workspace-delete)
              (,(kbd "s-f") . exwm-layout-toggle-fullscreen)
              (,(kbd "s-M-f") . exwm-floating-toggle-floating)
              (,(kbd "s-SPC") . exwm-input-toggle-keyboard)
              (,(kbd "s-h") . previous-buffer)
              (,(kbd "s-l") . next-buffer)
              (,(kbd "s-M-h") . winner-undo)
              (,(kbd "s-M-l") . winner-redo)
              (,(kbd "s-b") . cc/hydra-exwm-apps/body)
              (,(kbd "s-a") . cc/hydra-web-apps/body)
              ,@(mapcar (lambda (it)
                          `(,(kbd (car it)) .
                            (lambda ()
                              (interactive)
                              (start-process-shell-command "launcher" nil ,(cdr it)))))
                        '(("<XF86MonBrightnessDown>" . "light -U 3")
                          ("<XF86MonBrightnessUp>" . "light -A 3")))

              (,(kbd "s-.") . cc/dunst-hydra/body)
              ;; Additional notification close keybind, because this is quicker than going through hydra.
              (,(kbd "C-s-.") . (lambda ()
                                  (interactive)
                                  (shell-command "dunstctl close")))
              (,(kbd "C-s-h") . ,(kbd "<left>"))
              (,(kbd "C-s-l") . ,(kbd "<right>"))
              (,(kbd "C-s-j") . ,(kbd "<down>"))
              (,(kbd "C-s-k") . ,(kbd "<up>"))))
  (cc/csetq exwm-input-prefix-keys
            ;; TODO translate these
            `(?\M-o ?\M-x ?\M-: ?\C-x ?\C-u ?\C-h ?\C-\; ?\C-\M-\; ?\s-m  ?\s-s ?\s-q
                    ,(kbd "M-y")
                    ,(kbd "<s-return>")
                    ,(kbd "s-v")
                    ,(kbd "s-g")
                    ,(kbd "s-c")))
  (cc/csetq exwm-workspace-show-all-buffers t)
  (cc/csetq exwm-layout-show-all-buffers t)
  (cc/csetq exwm-manage-configurations
            '(((string-match-p "Cypress" exwm-class-name)
               managed t floating nil)
              ;; Fix IntelliJ popups - https://github.com/ch11ng/exwm/issues/680
              ;; This might be too broad of a match, but whatever.
              ((equal exwm-instance-name "sun-awt-X11-XDialogPeer")
               managed t floating t)
              ;; Firefox "picture-in-picture" video pop-outs
              ((and (string-match-p cc/firefox-class-regex exwm-class-name)
                    (equal exwm-title "Picture-in-Picture"))
               managed t floating nil)))

  ;; https://github.com/agzam/exwm-edit/issues/15
  (setq exwm-edit-bind-default-keys nil)
  ;; https://github.com/agzam/exwm-edit/issues/16
  (setq exwm-edit-last-kill nil)

  (exwm-enable)
  (exwm-randr-enable))

(defun cc/exwm--mode-setup ()
  ;; EXWM mode lines a few things hidden, so more room for names.
  (setq-local spaceline-buffer-id-max-length 55))

(defun cc/exwm--init-callback ()
  ;; Emacs-ception: need daemon for e.g. $EDITOR=emacsclient.
  (unless (server-running-p) (server-start))
  (cc/panel-enable)
  (start-process-shell-command "signal-desktop" nil "signal-desktop --start-in-tray"))

(defvar cc/exwm--kill-emacs-hook-command nil
  "Optional shell command to run as the (hopefully last) hook for `kill-emacs-hook'")
(defun cc/exwm--kill-emacs-do-command ()
  (when cc/exwm--kill-emacs-hook-command
    (shell-command cc/exwm--kill-emacs-hook-command)))

(defun cc/exwm-exit (command)
  (setq cc/exwm--kill-emacs-hook-command command)
  ;; Attempt to make sure my post-kill command runs at the very end of the hook, since it will do
  ;; e.g. shutdown or reboot.
  (remove-hook 'kill-emacs-hook #'cc/exwm--kill-emacs-do-command)
  (add-hook 'kill-emacs-hook #'cc/exwm--kill-emacs-do-command t)
  ;; No confirmation prompt - that's handled via `confirm-kill-emacs' (which exwm sets).
  (save-buffers-kill-emacs))

(defhydra cc/exwm-exit-hydra (:color blue)
  "EXWM Exit"
  ("s" (shell-command "systemctl suspend") "Suspend")
  ("l" (start-process-shell-command "lock" nil "cc-lock") "Lock")
  ("C-M-l" (cc/exwm-exit "loginctl terminate-session self") "Logout")
  ("C-M-p" (cc/exwm-exit "systemctl poweroff") "Poweroff")
  ("C-M-r" (cc/exwm-exit "systemctl reboot") "Reboot"))

;; I also use https://github.com/philc/vimium/ to provide avy-like link jumping.
;; Configuration:
;; unmapAll
;; map <c-o> LinkHints.activateMode
;; map <c-O> LinkHints.activateModeToOpenInNewTab
(defun cc/exwm--simulation-keys ()
  "Set `exwm-input-set-local-simulation-keys' for current application."
  (let ((common `(;; Navigation
                  (,(kbd "M-<") . ,(kbd "<home>"))
                  (,(kbd "M->") . ,(kbd "<end>"))
                  (,(kbd "C-v") . ,(kbd "<down><down><down><down><down>"))
                  (,(kbd "M-v") . ,(kbd "<up><up><up><up><up>"))
                  ;; Text manipulation (but prefer using `exwm-edit' primarily).
                  (,(kbd "C-w") . ,(kbd "C-x"))
                  (,(kbd "M-w") . ,(kbd "C-c"))
                  (,(kbd "C-y") . ,(kbd "C-v"))
                  (,(kbd "C-/") . ,(kbd "C-z")))))
    (if (cc/firefox-buffer-p)
        (exwm-input-set-local-simulation-keys
         `(,@common
           ;; Navigation
           (,(kbd "C-n") . ,(kbd "<down>"))
           (,(kbd "C-p") . ,(kbd "<up>"))
           (,(kbd "C-f") . ,(kbd "<right>"))
           (,(kbd "C-b") . ,(kbd "<left>"))
           (,(kbd "C-s") . ,(kbd "C-f"))
           ;; Tab/window management
           (,(kbd "C-k") . ,(kbd "C-w")) ; close
           (,(kbd "C-t") . ,(kbd "C-n")) ; open new window (not tab)
           (,(kbd "C-S-t") . ,(kbd "C-S-n")) ; restore window
           (,(kbd "M-l") . ,(kbd "M-<left>")) ; history back
           (,(kbd "M-r") . ,(kbd "M-<right>"))))
      (exwm-input-set-local-simulation-keys common)))) ; history forward

(defvar-local cc/exwm--firefox-id nil)
(defun cc/exwm--firefox-next-id ()
  (let ((ff-buffers (seq-filter #'cc/firefox-buffer-p (buffer-list))))
    (1+ (seq-max (cons -1 (seq-map (lambda (it)
                                     (with-current-buffer it
                                       (or cc/exwm--firefox-id -1)))
                                   ff-buffers))))))

(defun cc/exwm--rename-buffer ()
  ;; `exwm-title' may not be set immediately (at least once upon a time for Firefox).
  (let ((name (or exwm-title exwm-class-name)))
    (exwm-workspace-rename-buffer
     (cond
      ((cc/firefox-buffer-p)
       (unless cc/exwm--firefox-id
         (setq-local cc/exwm--firefox-id (cc/exwm--firefox-next-id)))
       (let* ((replaced (replace-regexp-in-string
                         ;; I can't persuade Emacs to save a Unicode em-dash properly, so
                         ;; punctuation class it is. Also this is more robust.
                         (rx  (+ " ") (+ punctuation) (+ " ") "Mozilla Firefox" string-end)
                         ""
                         name))
              (prefix (format "[%03d]" (mod cc/exwm--firefox-id 1000))))
         (concat prefix " " replaced)))
      (t name)))))

(defun cc/exwm--buffers (class-regex)
  "Get matching EXWM buffers."
  (seq-filter (lambda (b)
                (with-current-buffer b
                  (and
                   (equal major-mode 'exwm-mode)
                   (not (null exwm-class-name)) ; xev spawns a window with no class.
                   (string-match-p class-regex exwm-class-name))))
              (buffer-list)))

(defun cc/exwm-run-or-switch (class-regex start-cmd)
  "Switch to first EXWM buffer of CLASS or start one if none exists.

Will always start a new instance if called with prefix argument."
  (let ((existing (cc/exwm--buffers class-regex)))
    (if (or current-prefix-arg
            (not existing))
        (start-process-shell-command (car (split-string start-cmd)) nil start-cmd)
      (switch-to-buffer (car existing)))))

(defun cc/exwm-screenshot ()
  "Take screenshot and save filename to kill-ring."
  (interactive)
  (message "Taking screenshot...")
  (let* ((default-directory temporary-file-directory)
         (cmd "scrot --select --exec 'echo -n \"$f\"' 2>/dev/null")
         (result (shell-command-to-string cmd)))
    (if (string-empty-p result)
        (message "Screenshot cancelled")
      (let ((absolute-file (expand-file-name result)))
        (kill-new absolute-file)
        (message "Saved screenshot: %s" absolute-file)))))

(defhydra cc/hydra-exwm-apps (:color blue)
  ("f" (cc/exwm-run-or-switch cc/firefox-class-regex "firefox --new-window") "Firefox")
  ("s" (cc/exwm-run-or-switch (rx bos "Signal" eos) "signal-desktop --start-in-tray") "Signal")
  ("d" (cc/exwm-run-or-switch (rx bos "discord" eos) "discord") "Discord")
  ("y" (cc/exwm-run-or-switch (rx bos "Yubico Authenticator" eos) "yubioath-desktop") "Yubioath")
  ("/" cc/exwm-screenshot "Screenshot")
  ("C-g" nil nil)
  ("RET" nil nil))

(defun cc/exwm-web-app (url title-regexp)
  (let ((existing (seq-filter (lambda (b)
                                (with-current-buffer b
                                  (string-match-p title-regexp exwm-title)))
                              (cc/exwm--buffers "firefox"))))
    (if existing
        (switch-to-buffer (car existing))
      ;; This will break if url has single quotes, but it's my own config so whatever.
      (start-process-shell-command "exwm-web-app" nil (format "firefox --new-window '%s'" url)))))

(defhydra cc/hydra-web-apps (:color blue)
  ;; The trailing hyphens are the separator between page title and "- Mozilla Firefox" suffix.
  ("m" (cc/exwm-web-app "https://messages.google.com" "Messages for web") "SMS")
  ;; Assumption: @gmail.com is the default Google account (e.g. first account signed in)
  ("g" (cc/exwm-web-app "https://mail.google.com/mail/u/0" "- Gmail -") "Gmail")
  ("G" (cc/exwm-web-app "https://mail.google.com/mail/u/1" "- chasecaleb Mail -") "Gmail (chasecaleb)")
  ("t" (cc/exwm-web-app "https://todoist.com/app" ": Todoist") "Todoist")
  ("o" (cc/exwm-web-app "https://outlook.office365.com" "Outlook") "Outlook"))

(defun cc/randr--change ()
  (start-process-shell-command "xrandr" nil "xrandr --output \"$(xrandr | grep primary | awk '{print $1}')\" --auto")

  ;; This is quite the hack, but it's the least crazy way I could think of to automatically switch
  ;; font size on my work laptop (2019 Macbook Pro 16") install, which runs in a VM. Unfortunately
  ;; due to running in a VM there isn't a way to tell which display is actually in use, but I can
  ;; make a reasonable guess based on screen resolution. This doesn't work if the VM isn't
  ;; full-screen, but fortunately I live inside the VM 99% of the time so that's not a big issue.
  ;;
  ;; Now if only Firefox was this easy to change programmatically...
  (when (shell-command-to-string "pacman -Qqs chasecaleb-work-mac")
    (let* ((mac-resolution "3072x1920") ; Internal display set to "looks like 1536x960"
           (actual-resolution (replace-regexp-in-string (rx "\n" string-end) ""
                                                        (shell-command-to-string
                                                         (concat "xrandr --listmonitors | "
                                                                 "awk '/Virtual/ {print $3}' | "
                                                                 "awk -F '[/x]' '{print $1 \"x\" $3}'"))))
           (large-font-p (string= mac-resolution actual-resolution)))
      (cc/appearance-font-size-adjust large-font-p))))

(provide 'cc-exwm)
