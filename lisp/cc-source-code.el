;;; -*- lexical-binding: t -*-

(defvar cc/source-code-hook nil
  "Called for all source code major modes, including config modes like yaml.")

(defun cc/source-code-init ()
  (dolist (hook '(prog-mode-hook
                  conf-mode-hook
                  yaml-mode-hook))
    (add-hook hook #'cc/source-code--run-hook)))

(defun cc/source-code--run-hook ()
  (run-hooks 'cc/source-code-hook))

(provide 'cc-source-code)
