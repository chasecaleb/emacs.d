;;; -*- lexical-binding: t -*-
(require 'hl-line)
(require 'hl-todo)
(require 'pulsar)
(require 'rainbow-delimiters)
(require 'spacemacs-dark-theme)
(require 'cc-utils)

(defvar cc/font-height-default 100)

(defun cc/appearance-init ()
  (cc/appearance--pulsar)
  (add-hook 'visual-line-mode-hook #'adaptive-wrap-prefix-mode)
  (add-hook 'cc/source-code-hook #'cc/appearance--source-code-setup)
  (set-face-attribute 'default nil
                      :family "Hack"
                      :height cc/font-height-default
                      :weight 'normal
                      :width 'normal)
  (set-face-attribute 'fixed-pitch-serif nil
                      :family "Hack")
  ;; For some reason (possibly Emacs 28 bug) italic face is underline without this.
  (set-face-attribute 'italic nil :slant 'italic :underline nil)
  (set-fontset-font t 'symbol "Noto Color Emoji")
  (global-set-key (kbd "s--") #'cc/appearance-font-size-adjust)
  (load-theme 'spacemacs-dark t)

  (setq scroll-preserve-screen-position t)
  (setq scroll-error-top-bottom t)
  (setq-default fill-column 100)
  (setq-default display-fill-column-indicator t)

  (setq hl-todo-keyword-faces
        '(("TODO" . "orange")
          ("FIXME" . "red")
          ("NEXT" . "yellow")))
  (global-hl-line-mode)
  (global-prettify-symbols-mode))

(defun cc/appearance--source-code-setup ()
  (setq-local show-trailing-whitespace t)
  (display-line-numbers-mode)
  (display-fill-column-indicator-mode)
  (diminish 'page-break-lines-mode)
  (page-break-lines-mode)
  (hl-todo-mode)
  (rainbow-delimiters-mode))

(defun cc/appearance-font-size-adjust (&optional arg)
  "Adjust font size between two presets.

Switches to large size if called with C-u or if ARG is t, extra large with C-u
C-u, or default otherwise."
  (interactive "p")
  (let ((height (pcase arg
                  ((or 't 4) 120)
                  (16 140)
                  (_ cc/font-height-default))))
    ;; Change only current frame to avoid affecting things like corfu (which may only be necessary
    ;; because of my kludge to make Corfu's completion child frame into a top-level frame so that it
    ;; draws on top of EXWM). Whatever.
    (set-face-attribute 'default (selected-frame) :height height)))

(defun cc/appearance--pulsar ()
  (add-hook 'consult-after-jump-hook #'pulsar-recenter-middle)
  (add-hook 'consult-after-jump-hook #'pulsar-reveal-entry)
  (global-set-key (kbd "s-p") #'cc/appearance-pulse-line-long)
  (setq pulsar-iterations 5)
  (setq pulsar-face 'pulsar-red)
  (cc/csetq pulsar-pulse-functions
            '(;; Additional added by me
              pop-to-buffer
              aw-switch-to-window
              previous-buffer
              next-buffer
              ;; Defaults
              recenter-top-bottom
              move-to-window-line-top-bottom
              reposition-window
              bookmark-jump
              other-window
              delete-window
              delete-other-windows
              forward-page
              backward-page
              scroll-up-command
              scroll-down-command
              windmove-right
              windmove-left
              windmove-up
              windmove-down
              windmove-swap-states-right
              windmove-swap-states-left
              windmove-swap-states-up
              windmove-swap-states-down
              tab-new
              tab-close
              tab-next
              org-next-visible-heading
              org-previous-visible-heading
              org-forward-heading-same-level
              org-backward-heading-same-level
              outline-backward-same-level
              outline-forward-same-level
              outline-next-visible-heading
              outline-previous-visible-heading
              outline-up-heading))
  (pulsar-global-mode))

(defun cc/appearance-pulse-line-long ()
  (interactive)
  (let ((pulsar-iterations 40))
    (pulsar-pulse-line)))

(provide 'cc-appearance)
