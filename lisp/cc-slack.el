;;; -*- lexical-binding: t -*-

;; This needs to be set *before* anything loads slack-file.el, because the custom var initializer
;; tries to create a directory within `user-emacs-directory' otherwise... which is a remarkably
;; horrible place for a side effect.
(eval-and-compile
  (setq slack-file-dir "/tmp/slack/"))

(require 'emojify) ;; Used by slack
(require 'slack)
(require 'auth-source)
(require 'use-package)
(require 'hydra)
(require 'cc-user-directories)
(require 'cc-utils)

(defvar cc/slack-reaction-keymap
  (let ((map (make-keymap)))
    (define-key map (kbd "r") #'slack-message-add-reaction)
    (define-key map (kbd "M-r") #'slack-message-remove-reaction)
    map))

(defmacro cc/slack--reaction-keybind (key reaction &optional name)
  (let ((fn (make-symbol (concat "cc/slack-reaction-" (or name reaction)))))
    `(progn
       (defun ,fn ()
         (:documentation ,reaction)
         (interactive)
         (if-let* ((buf slack-current-buffer)
                   (team (slack-buffer-team buf)))
             (slack-buffer-add-reaction-to-message buf ,reaction (slack-get-ts))))
       (define-key cc/slack-reaction-keymap (kbd ,key) #',fn))))

(cc/slack--reaction-keybind "y" "+1")
(cc/slack--reaction-keybind "n" "-1")
(cc/slack--reaction-keybind "1" "100")
(cc/slack--reaction-keybind "e" "eyes")
(cc/slack--reaction-keybind "d" "dumpsterfire")
(cc/slack--reaction-keybind "ty" "thank-you")
(cc/slack--reaction-keybind "td" "tada")
;; mnemonic: "s" for snark.
(cc/slack--reaction-keybind "sr" "rolling_on_the_floor_laughing" "rofl")
(cc/slack--reaction-keybind "se" "face_with_rolling_eyes" "eye-roll")

;;;###autoload
(defvar cc/slack-keymap
  (let ((map (make-keymap "Slack commands")))
    ;; Reminder: autoload on a defvar just copies the whole form into the autoloads file. It *does
    ;; not* do the same lazy loading that happens with a function. Because of that, I can't use
    ;; `cc/define-key-list' without an annoying amount of extra work.
    (dolist (it `(("m"    . ,#'slack-select-rooms)
                  ("s-m"  . ,#'slack-select-rooms)
                  ("u"    . ,#'slack-select-unread-rooms)
                  ("c"    . ,#'slack-channel-select)
                  ("i"    . ,#'slack-im-select)
                  ("M-i"  . ,#'slack-im-open)
                  ("t"    . ,#'slack-all-threads)
                  ("*"    . ,#'slack-stars-list)
                  ("."    . ,#'cc/slack-register-and-start)
                  ("q"    . ,#'slack-ws-close)))
      (define-key map (kbd (car it)) (cdr it)))
    map))

;;;###autoload
(defun cc/slack-register-and-start ()
  "Register slack team (if not already registered) and connect.

Slack team is registered lazy because my token is in a
    gpg-encrypted authinfo file, and I don't want to be prompted
    to unlock it immediately on startup."
  (interactive)
  (unless slack-current-team ; register team if not already done
    (let ((nipr-token (auth-source-pick-first-password
                       :host "naic-nipr.slack.com"
                       :user "cchase@nipr.com")))
      (if (null nipr-token)
          (user-error "Slack token not found, cannot register team")
        (slack-register-team
         :name "NAIC-NIPR"
         :default t
         :token nipr-token
         :full-and-display-names t
         :modeline-enabled t
         ;; Reminder: gifs are only animated when point is on the message. Nice.
         :animate-image t))))
  ;; Start/connect if team was registered. This is a separate conditional from registration
  ;; because this function may be called multiple times, in which case it should register + start
  ;; the first time and then start (without registering) afterwards.
  (when slack-current-team
    (slack-start)))

(defmacro cc/with-slack-buffer-create (allow-create-p &rest body-forms)
  (declare (indent 1))
  `(let ((slack-buffer-create-on-notify
          (and slack-buffer-create-on-notify
               ,allow-create-p)))
     ,@body-forms))

(defun cc/tracking-toggle-ignore ()
  "Toggle ignoring current buffer via `tracking-ignored-buffers'."
  (interactive)
  (when (not (derived-mode-p 'lui-mode))
    (user-error "Can't ignore this buffer"))
  (if-let ((ignored (tracking-ignored-p (current-buffer) nil)))
      (setq tracking-ignored-buffers (remove ignored tracking-ignored-buffers))
    (add-to-list 'tracking-ignored-buffers (concat "^" (regexp-quote (buffer-name)) "$"))))

(defun cc/slack-embed-mention ()
  (interactive)
  (call-interactively #'slack-message-embed-mention)
  (insert " "))

(defun cc/slack-embed-channel ()
  (interactive)
  (call-interactively #'slack-message-embed-channel)
  (insert " "))

(defun cc/slack-truncate-modeline (buf-name)
  (if (string-match-p "\\*Slack " buf-name)
      (thread-last buf-name
                   (replace-regexp-in-string "\\*Slack - .*? : " "")
                   ;; Need to use capture group instead of just typing it out to preserve font locking.
                   (replace-regexp-in-string "\\(Thread\\) - [0-9]+\\.[0-9]+$" "\\1"))
    buf-name))

(defun cc/tracking-remove-buffer-if-visible-advice (fn buffer)
  "Prevent tracking removing buffer if it was merely previewed."
  (unless (active-minibuffer-window)
    (funcall fn buffer)))

(defun cc/slack-event-update-buffer-advice (fn event message team)
  "Don't open/track buffer just because of a reaction."
  (cc/with-slack-buffer-create (not (obj-of-class-p event 'slack-reaction-event))
    (funcall fn event message team)))

(defun cc/slack-room-update-buffer-advice (fn this team msg replace)
  "Don't open/track room when a thread within the room is received.

Tracking both the room *and* the thread is redundant and annoying."
  (let ((slack-buffer-create-on-notify
         (and slack-buffer-create-on-notify
              ;; thread-ts but no subtype = thread msg that isn't shared with room.
              (or (slot-boundp msg 'subtype)
                  (null (oref msg thread-ts))))))
    (funcall fn this team msg replace)))

;; Main slack keybind is autoloaded from my main init file, so do config on load.
(progn
  (dolist (map (list slack-mode-map slack-buffer-mode-map))
    (cc/define-key-list map `(("C-M-n"      . ,#'slack-buffer-goto-next-message)
                              ("C-M-p"      . ,#'slack-buffer-goto-prev-message)
                              ("C-c @"      . ,#'slack-message-embed-mention)
                              ("C-c #"      . ,#'slack-message-embed-channel)
                              ("C-c C-e"    . ,#'slack-insert-emoji)
                              ("C-c C-l"    . ,#'slack-file-upload)
                              ("C-c C-w"    . ,#'slack-message-share)
                              ("C-c M-C-W"  . ,#'slack-message-copy-link)
                              ("C-c '"      . ,#'slack-message-edit)
                              ("C-c C-k"    . ,#'slack-message-delete)
                              ("C-c C-f"    . ,#'slack-message-follow)
                              ("C-c M-C-f"  . ,#'slack-message-unfollow)
                              ("C-c C-t"    . ,#'slack-thread-show-or-create)
                              ("C-c C-z"    . ,#'cc/tracking-toggle-ignore)
                              ("C-C C-r"    . ,cc/slack-reaction-keymap))))

  (advice-add 'powerline-buffer-id :filter-return 'cc/slack-truncate-modeline)
  (advice-add 'tracking-remove-buffer :around 'cc/tracking-remove-buffer-if-visible-advice)
  (advice-add 'slack-event-update-buffer :around 'cc/slack-event-update-buffer-advice)
  (advice-add 'slack-room-update-buffer :around 'cc/slack-room-update-buffer-advice)

  (setq slack-prefer-current-team t)
  (setq slack-display-team-name nil)
  ;; Buffers need to be open for tracking (e.g. C-c SPC), so open them automatically.
  ;; More info: https://github.com/yuya373/emacs-slack/issues/46
  (setq slack-buffer-create-on-notify t)
  (setq slack-buffer-function #'switch-to-buffer)
  ;; No-op lambda because by default slack shows a notification for every deleted message
  ;; in every channel (including ones that I haven't even joined).
  (setq slack-message-custom-delete-notifier (lambda (_msg _room _team)))
  (setq slack-render-image-p t)

  (setq slack-buffer-emojify t)
  (setq emojify-emojis-dir (expand-file-name "emojis/" cc/user-cache))

  ;; Improved slack buffer display logic.
  (add-to-list 'display-buffer-alist
               `((lambda (buffer _actions)
                   (with-current-buffer buffer
                     (derived-mode-p 'slack-mode 'slack-buffer-mode)))
                 (display-buffer-reuse-mode-window display-buffer-same-window)
                 (mode . (slack-mode
                          slack-buffer-mode
                          slack-thread-message-buffer-mode))))
  ;; Need to use `pop-to-buffer' in order for `display-buffer-alist' to be relevant.
  (setq slack-buffer-function #'pop-to-buffer))

(provide 'cc-slack)
