;;; -*- lexical-binding: t -*-

(defconst cc/firefox-class-regex (rx bos (or "firefox" "Firefox")))

(defun cc/firefox-buffer-p (&optional buffer)
  (with-current-buffer (or buffer (current-buffer))
    (and (derived-mode-p 'exwm-mode)
         (bound-and-true-p exwm-class-name)
         (string-match-p cc/firefox-class-regex  exwm-class-name))))

(provide 'cc-exwm-utils)
