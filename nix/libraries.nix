{ pkgs, emacs, inputs }:
let
  buildPackage = import ./build-package.nix { inherit pkgs emacs; };

  # Is this a gross hack or a stroke of elegance? Both, probably. But it's a simple solution,
  # it's reliable, and I like doing this within the package at issue instead of hidden
  # somewhere in my init lisp.
  lispSetString = var: value: ''
    echo "(setq ${var} \"${value}\")" >> ./*-autoloads.el
  '';

  # Alternative solution to deal with paths for packages that don't provide a variable, but
  # keep in mind that this:
  #  - Is not idempotent
  #  - Affects all of Emacs and subprocesses thereof (so almost everything in userland, since
  #    Emacs is my window manager)
  shimExecPath = package: ''
    echo "(add-to-list 'exec-path \"${package}/bin\")" >> ./*-autoloads.el
  '';
in
# Keep a blank line between each library so that I can format via M-x sort-paragraphs
rec {

  ace-window = buildPackage {
    name = "ace-window";
    src = inputs.ace-window;
    lispInputs = [ avy ];
  };

  adaptive-wrap = buildPackage {
    name = "adaptive-wrap";
    src = inputs.adaptive-wrap;
  };

  aio = buildPackage {
    name = "aio";
    src = inputs.aio;
    # I don't use contrib and it has extra dependencies.
    preBuild = "rm aio-contrib.el";
  };

  alert = buildPackage {
    name = "alert";
    src = inputs.alert;
  };

  all-the-icons = buildPackage {
    name = "all-the-icons";
    src = inputs.all-the-icons;
  };

  async = buildPackage {
    name = "async";
    src = inputs.async;
  };

  avy = buildPackage {
    name = "avy";
    src = inputs.avy;
  };

  buttercup = buildPackage {
    name = "buttercup";
    src = inputs.buttercup;
  };

  circe = buildPackage {
    name = "circe";
    src = inputs.circe;
  };

  closql = buildPackage {
    name = "closql";
    src = inputs.closql;
    lispInputs = [ emacsql ];
  };

  consult-flycheck = buildPackage {
    name = "consult-flycheck";
    src = inputs.consult-flycheck;
    lispInputs = [ consult dash flycheck ];
  };

  consult-projectile = buildPackage {
    name = "consult-projectile";
    src = inputs.consult-projectile;
    lispInputs = [ consult projectile ];
  };

  consult = buildPackage {
    name = "consult";
    src = inputs.consult;
    lispInputs = [ org ];
  };

  consult-yasnippet = buildPackage {
    name = "consult-yasnippet";
    src = inputs.consult-yasnippet;
    lispInputs = [ consult yasnippet ];
  };

  corfu-doc = buildPackage {
    name = "corfu-doc";
    src = inputs.corfu-doc;
    lispInputs = [ corfu ];
  };

  corfu = buildPackage {
    name = "corfu";
    src = inputs.corfu;
  };

  dash = buildPackage {
    name = "dash";
    src = inputs.dash;
  };

  deferred = buildPackage {
    name = "deferred";
    src = inputs.deferred;
  };

  diff-hl = buildPackage {
    name = "diff-hl";
    src = inputs.diff-hl;
  };

  diminish = buildPackage {
    name = "diminish";
    src = inputs.diminish;
  };

  docker = buildPackage {
    name = "docker";
    src = inputs.docker;
    lispInputs = [ aio dash json-mode json-snatcher s tablist ];
  };

  dockerfile-mode = buildPackage {
    name = "dockerfile-mode";
    src = inputs.dockerfile-mode;
  };

  doct = buildPackage {
    name = "doct";
    src = inputs.doct;
  };

  dumb-jump = buildPackage {
    name = "dumb-jump";
    src = inputs.dumb-jump;
    lispInputs = [ dash popup-el s ];
  };

  emacsql = buildPackage {
    name = "emacsql";
    src = inputs.emacsql;
    buildInputs = [ pkgs.sqlite ];
    preBuild = ''
      # Don't care about postgresql functionality and don't want the dependency
      rm emacsql-pg.el
      # I do need sqlite however (for forge and a bunch of others).
      cd sqlite
      make
      cd -
    '';
    preInstall = shimExecPath pkgs.sqlite;
  };

  embark = buildPackage {
    name = "embark";
    src = inputs.embark;
    lispInputs = [ avy consult ];
  };

  emojify = buildPackage {
    name = "emojify";
    src = inputs.emojify;
    lispInputs = [ dash ht ];
  };

  epl = buildPackage {
    name = "epl";
    src = inputs.epl;
  };

  exwm = buildPackage {
    name = "exwm";
    src = inputs.exwm;
    lispInputs = [ xelb ];
  };

  exwm-edit = buildPackage {
    name = "exwm-edit";
    src = inputs.exwm-edit;
    lispInputs = [ exwm xelb ];
  };

  f = buildPackage {
    name = "f";
    src = inputs.f;
    lispInputs = [ s dash ];
  };

  fancy-battery = buildPackage {
    name = "fancy-battery";
    src = inputs.fancy-battery;
  };

  flycheck = buildPackage {
    name = "flycheck";
    src = inputs.flycheck;
    preBuild = "rm -r flycheck-ert.el flycheck-buttercup.el maint/";
    lispInputs = [ dash ];
  };

  forge = buildPackage {
    name = "forge";
    src = inputs.forge;
    lispInputs = [ closql dash emacsql ghub magit markdown-mode treepy with-editor yaml ];
  };

  ghub = buildPackage {
    name = "ghub";
    src = inputs.ghub;
    lispInputs = [ treepy ];
  };

  git-link = buildPackage {
    name = "git-link";
    src = inputs.git-link;
  };

  git-modes = buildPackage {
    name = "git-modes";
    src = inputs.git-modes;
  };

  git-timemachine = buildPackage {
    name = "git-timemachine";
    src = inputs.git-timemachine;
  };

  go-mode = buildPackage {
    name = "go-mode";
    src = inputs.go-mode;
  };

  hcl-mode = buildPackage {
    name = "hcl-mode";
    src = inputs.hcl-mode;
  };

  hl-todo = buildPackage {
    name = "hl-todo";
    src = inputs.hl-todo;
  };

  ht = buildPackage {
    name = "ht";
    src = inputs.ht;
    lispInputs = [ dash ];
  };

  hydra = buildPackage {
    name = "hydra";
    src = inputs.hydra;
    # org seems like a weird dependency, but it's because of hydra-ox.el
    lispInputs = [ org ];
  };

  iter2 = buildPackage {
    name = "iter2";
    src = inputs.iter2;
  };

  json-mode = buildPackage {
    name = "json-mode";
    src = inputs.json-mode;
    lispInputs = [ json-snatcher ];
  };

  json-snatcher = buildPackage {
    name = "json-snatcher";
    src = inputs.json-snatcher;
  };

  kind-icon = buildPackage {
    name = "kind-icon";
    src = inputs.kind-icon;
    lispInputs = [ svg-lib ];
  };

  kotlin-mode = buildPackage {
    name = "kotlin-mode";
    src = inputs.kotlin-mode;
  };

  kubel = buildPackage {
    name = "kubel";
    src = inputs.kubel;
    # Trigger warning: evil mode is evil.
    preBuild = "rm kubel-evil.el";
    lispInputs = [ dash s yaml-mode ];
  };

  let-alist = buildPackage {
    name = "let-alist";
    src = inputs.let-alist;
  };

  lsp-mode = buildPackage {
    name = "lsp-mode";
    src = inputs.lsp-mode;
    lispInputs = [ dash f ht hydra markdown-mode s spinner ];
    preBuild = "rm -r use-package/lsp-use-package.el scripts examples";
  };

  lsp-ui = buildPackage {
    name = "lsp-ui";
    src = inputs.lsp-ui;
    lispInputs = [ dash f ht hydra lsp-mode markdown-mode s spinner ];
  };

  magit = buildPackage {
    name = "magit";
    src = inputs.magit;
    lispInputs = [ dash transient with-editor ];
    # Magit crashes during byte-compilation without pkgs.git... as do any packages which
    # load it, thus git needs to be propagated.
    propagatedBuildInputs = [ pkgs.git ];
    preBuild = ''rm ./lisp/magit-libgit.el'';
  };

  magit-delta = buildPackage {
    name = "magit-delta";
    src = inputs.magit-delta;
    lispInputs = [ dash magit xterm-color with-editor ];
    preInstall = lispSetString "magit-delta-delta-executable" "${pkgs.delta}/bin/delta";
  };

  marginalia = buildPackage {
    name = "marginalia";
    src = inputs.marginalia;
  };

  markdown-mode = buildPackage {
    name = "markdown-mode";
    src = inputs.markdown-mode;
  };

  memoize = buildPackage {
    name = "memoize";
    src = inputs.memoize;
  };

  nix-mode = buildPackage {
    name = "nix-mode";
    src = inputs.nix-mode;
    lispInputs = [ dash magit ];
    preBuild = "rm nix-company.el nix-mode-mmm.el";
  };

  nvm = buildPackage {
    name = "nvm";
    src = inputs.nvm;
    lispInputs = [ dash f s ];
  };

  ob-async = buildPackage {
    name = "ob-async";
    src = inputs.ob-async;
    lispInputs = [ async dash org ];
  };

  ob-http = buildPackage {
    name = "ob-http";
    src = inputs.ob-http;
    lispInputs = [ org s ];
  };

  ob-kubectl = buildPackage {
    name = "ob-kubectl";
    src = inputs.ob-kubectl;
    lispInputs = [ org yaml-mode ];
  };

  orderless = buildPackage {
    name = "orderless";
    src = inputs.orderless;
  };

  org = buildPackage {
    name = "org";
    src = inputs.org;
    preBuild = ''
      # The way targets.mk determines ORGVERSION is broken (or at least not working for me),
      # which causes various packages like org-journal to throw an error... so need to do it
      # myself.
      org_version=$(grep -E "^;;\s*Version:\s+" lisp/org.el | awk '{print $NF}')
      flags=("prefix=$out/share" "ORGVERSION=$org_version")
      # Verify prefix config worked (in case e.g. Org changes their make files)
      # P.S. the double single quotes escape the $... super intuitive, right?
      make "''${flags[@]}" config | grep "lispdir\s*=\s* $out/share/emacs/site-lisp/org"
      make "''${flags[@]}"
      # Make autoloads file match my naming convention so that I can load it the same way.
      # However still keep the original too, just in case it's needed for... whatever.
      cp ./lisp/org-{loaddefs,autoloads}.el
    '';
  };

  org-contrib = buildPackage {
    name = "org-contrib";
    src = inputs.org-contrib;
    # This will actually compile *without* org as an input, but that's because it will end
    # up relying on the grossly outdated version of org built into Emacs. Someday I should
    # look into removing the built-in version of org from Emacs.
    lispInputs = [ org ];
  };

  org-journal = buildPackage {
    name = "org-journal";
    src = inputs.org-journal;
    lispInputs = [ org ];
  };

  org-noter = buildPackage {
    name = "org-noter";
    src = inputs.org-noter;
    lispInputs = [ org ];
    # Circular dependency of org-noter <-> org-pdftools, but fortunately I don't actually
    # need this. P.S. circular dependencies are evil.
    preBuild = "rm other/org-noter-integration.el";
  };

  org-pdftools = buildPackage {
    name = "org-pdftools";
    src = inputs.org-pdftools;
    lispInputs = [ org org-noter pdf-tools tablist ];
    preBuild = "rm org-noter-pdftools.el && export HOME=/tmp";
  };

  org-roam = buildPackage {
    name = "org-roam";
    src = inputs.org-roam;
    lispInputs = [ dash emacsql magit org ];
    preBuild = "make docs";
    preInstall = lispSetString "org-roam-graph-executable" "${pkgs.graphviz}/bin/dot";
  };

  org-super-agenda = buildPackage {
    name = "org-super-agenda";
    src = inputs.org-super-agenda;
    lispInputs = [ dash ht org s ts ];
  };

  orgit = buildPackage {
    name = "orgit";
    src = inputs.orgit;
    lispInputs = [ dash magit org with-editor ];
  };

  ov = buildPackage {
    name = "ov";
    src = inputs.ov;
  };

  page-break-lines = buildPackage {
    name = "page-break-lines";
    src = inputs.page-break-lines;
  };

  pdf-tools = buildPackage {
    name = "pdf-tools";
    src = inputs.pdf-tools;
    lispInputs = [ org tablist ];
    nativeBuildInputs = [ pkgs.autoconf pkgs.automake pkgs.pkg-config ];
    buildInputs = [ pkgs.libpng pkgs.zlib pkgs.poppler ];
    preBuild = "(cd server && ${pkgs.bash}/bin/bash ./autobuild)";
  };

  peg = buildPackage {
    name = "peg";
    src = inputs.peg;
  };

  pkg-info = buildPackage {
    name = "pkg-info";
    src = inputs.pkg-info;
    lispInputs = [ epl ];
  };

  popup-el = buildPackage {
    name = "popup-el";
    src = inputs.popup-el;
  };

  powerline = buildPackage {
    name = "powerline";
    src = inputs.powerline;
  };

  prettier = buildPackage {
    name = "prettier";
    src = inputs.prettier;
    preInstall = ''
      ${shimExecPath pkgs.nodejs}
      ${shimExecPath pkgs.nodePackages.prettier}
    '';
    lispInputs = [ dash f iter2 nvm s ];
  };

  projectile = buildPackage {
    name = "projectile";
    src = inputs.projectile;
  };

  pulsar = buildPackage {
    name = "pulsar";
    src = inputs.pulsar;
  };

  rainbow-delimiters = buildPackage {
    name = "rainbow-delimiters";
    src = inputs.rainbow-delimiters;
  };

  request = buildPackage {
    name = "request";
    src = inputs.request;
    lispInputs = [ deferred ];
  };

  s = buildPackage {
    name = "s";
    src = inputs.s;
  };

  slack = buildPackage {
    name = "slack";
    src = inputs.slack;
    # Slack will still compile without emojify, but I have it here because it's an optional
    # dependency.
    lispInputs = [ alert circe dash emojify request websocket ];
    preBuild = "export HOME=/tmp";
  };

  smartparens = buildPackage {
    name = "smartparens";
    src = inputs.smartparens;
    lispInputs = [ dash ];
  };

  spaceline = buildPackage {
    name = "spaceline";
    src = inputs.spaceline;
    lispInputs = [ dash powerline s ];
  };

  spacemacs-theme = buildPackage {
    name = "spacemacs-theme";
    src = inputs.spacemacs-theme;
  };

  spinner = buildPackage {
    name = "spinner";
    src = inputs.spinner;
  };

  svg-lib = buildPackage {
    name = "svg-lib";
    src = inputs.svg-lib;
  };

  symbol-overlay = buildPackage {
    name = "symbol-overlay";
    src = inputs.symbol-overlay;
  };

  tablist = buildPackage {
    name = "tablist";
    src = inputs.tablist;
  };

  terraform-mode = buildPackage {
    name = "terraform-mode";
    src = inputs.terraform-mode;
    lispInputs = [ dash hcl-mode ];
  };

  todoist = buildPackage {
    name = "todoist";
    src = inputs.todoist;
    lispInputs = [ dash org ];
  };

  transient = buildPackage {
    name = "transient";
    src = inputs.transient;
  };

  transpose-frame = buildPackage {
    name = "transpose-frame";
    src = inputs.transpose-frame;
  };

  treepy = buildPackage {
    name = "treepy";
    src = inputs.treepy;
  };

  ts = buildPackage {
    name = "ts";
    src = inputs.ts;
    lispInputs = [ dash s ];
  };

  typescript = buildPackage {
    name = "typescript";
    src = inputs.typescript;
  };

  use-package = buildPackage {
    name = "use-package";
    src = inputs.use-package;
  };

  vertico = buildPackage {
    name = "vertico";
    src = inputs.vertico;
  };

  visual-regexp = buildPackage {
    name = "visual-regexp";
    src = inputs.visual-regexp;
  };

  vterm = buildPackage {
    name = "vterm";
    src = inputs.vterm;
    dontConfigure = false;
    nativeBuildInputs = [ pkgs.cmake ];
    buildInputs = [ pkgs.libvterm-neovim ];
    preBuild = ''
      make
      # Make script changes to ./build subdirectory, which breaks my build logic.
      cd ../
      # Cleanup build so that it doesn't get packaged by my install script.
      rm -r ./build
    '';
  };

  web-mode = buildPackage {
    name = "web-mode";
    src = inputs.web-mode;
  };

  websocket = buildPackage {
    name = "websocket";
    src = inputs.websocket;
  };

  wgrep = buildPackage {
    name = "wgrep";
    src = inputs.wgrep;
    preBuild = '' rm wgrep-test.el wgrep-subtest.el '';
  };

  with-editor = buildPackage {
    name = "with-editor";
    src = inputs.with-editor;
  };

  xelb = buildPackage {
    name = "xelb";
    src = inputs.xelb;
  };

  xterm-color = buildPackage {
    name = "xterm-color";
    src = inputs.xterm-color;
  };

  yaml = buildPackage {
    name = "yaml";
    src = inputs.yaml;
  };

  yaml-mode = buildPackage {
    name = "yaml-mode";
    src = inputs.yaml-mode;
  };

  yasnippet = buildPackage {
    name = "yasnippet";
    src = inputs.yasnippet;
    preBuild = "rm yasnippet-tests.el";
  };

}
