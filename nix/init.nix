{ pkgs, emacs, libraries }:
let
  buildPackage = import ./build-package.nix { inherit pkgs emacs; };

  # Surprisingly Nix doesn't have a string regexp replace builtin, but this works.
  initName = file: builtins.replaceStrings [ ".el" ] [ "" ] file;
  initPath = file: ./.. + "/lisp/${file}";

  # right is module files, wrong is entrypoint and tests.
  lispFiles = builtins.partition (it: it != "cc-init.el" && !pkgs.lib.hasSuffix "-test.el" it)
    (builtins.attrNames (builtins.readDir ./../lisp));

  parseRequires = file: result:
    let
      contents = builtins.readFile (./.. + "/lisp/${file}");
      lines = pkgs.lib.splitString "\n" contents;
      # Regexp match needs to handle both (require 'cc-foo) and (cc/log-require 'cc-foo).
      matches = builtins.map
        (builtins.match "[^;]+require[[:space:]]+'(cc-[^) ]+).*") # Yay currying.
        lines;
      requires = pkgs.lib.flatten (builtins.filter (it: it != null) matches);
      # WARNING: this naive filename assumption will break if I ever use
      # multiple/sub-directories for my init.
      transitives = (pkgs.lib.flatten (builtins.map (it: parseRequires "${it}.el" [ ]) requires));
    in
    requires ++ transitives;

  mapToAttrs = fn: attrs: builtins.listToAttrs (builtins.map fn attrs);

  initModules = mapToAttrs
    (f: {
      name = initName f;
      value = buildPackage {
        name = initName f;
        src = [ (initPath f) ];
        lispInputs = (builtins.attrValues libraries) ++
          (builtins.map
            (it: builtins.getAttr it initModules)
            (parseRequires f [ ]));
        byteCompileErrorOnWarn = true;
        extraArgs = {
          # src subdirectory awkwardness is to prevent Nix from packaging env-vars file - see
          # nixpkg file pkgs/stdenv/generic/setup.sh.
          sourceRoot = "./src";
          preUnpack = ''mkdir src'';
          unpackCmd = ''cp $curSrc ./src/$(stripHash $curSrc)'';
        };
      };
    })
    lispFiles.right;
in
initModules // {
  # Skip byte compiling my main (entry-point) init file, because:
  # 1. It's expensive to compile, since Emacs has to load *all* my other init files.
  # 2. There's virtually zero performance benefit, since it just calls a few functions.
  # 3. It would have to be recompiled every time I change any other file.
  #
  # TLDR: by skipping this I'm able to cut compilation down when making a change to a single
  # file from something like 20s to 5s -- worth it.
  cc-init = buildPackage {
    name = "cc-init";
    src = builtins.map initPath lispFiles.wrong;
    # lispInputs isn't actually necessary for this derivation (since I'm skipping compilation), but
    # I want "nix build" to rebuild all the things.
    lispInputs = (builtins.attrValues initModules) ++ (builtins.attrValues libraries);
    extraArgs =
      let
        destDir = "share/emacs/site-lisp/cc-init";
      in
      {
        dontBuild = true;
        sourceRoot = "./src";
        preUnpack = ''mkdir -p src/${destDir}'';
        unpackCmd = ''cp $curSrc ./src/${destDir}/$(stripHash $curSrc)'';

        # Run all tests together (instead of attempting to associate tests with their respective
        # modules) because:
        # 1. This is easier and more robust.
        # 2. The tests themselves are fast, whereas starting up Emacs to run the tests is not as
        #    fast... so it's faster to run the tests all at once than run an Emacs process for each
        #    file.
        # 3. It's possible for a change to cause a seemingly-unrelated test to fail, so I want to be
        #    sure all my tests pass.
        #
        # There is one minor downside: the load path of each test file is not isolated
        # (unless buttercup does something on its own), but I'm not too concerned about that.
        doCheck = true;
        checkPhase = ''
           "${emacs}/bin/emacs" -Q -nw --batch --load "${./builder.el}" --funcall cc/nix-test
        '';
      };
  };
}
