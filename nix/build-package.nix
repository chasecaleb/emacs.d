{ pkgs, emacs }:
{ name
, src ? [ ]
, lispInputs ? [ ]
, buildInputs ? [ ]
, propagatedBuildInputs ? [ ]
  # Disable configure because I don't want magic happening unexpectedly.
, dontConfigure ? true
, nativeBuildInputs ? [ ]
, preBuild ? ""
, preInstall ? ""
, byteCompileErrorOnWarn ? false
, extraArgs ? { }
}: pkgs.stdenv.mkDerivation ({
  inherit name src dontConfigure nativeBuildInputs propagatedBuildInputs;
  buildInputs = [ emacs pkgs.texinfo ] ++ buildInputs ++ lispInputs;
  # Prefixing arguments used by my builder.el lisp logic with "arg*" for my sanity to help
  # me remember what they're used for.
  argLibs = lispInputs;
  argByteCompileErrorOnWarn = if byteCompileErrorOnWarn then "true" else null;
  buildPhase = ''
    ${preBuild}
    "${emacs}/bin/emacs" -Q -nw --batch --load "${./builder.el}" --funcall cc/nix-build "${name}"
  '';
  installPhase = ''
    ${preInstall}
    ${./installer.sh}
  '';
} // extraArgs)
