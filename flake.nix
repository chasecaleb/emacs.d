{
  description = "Emacs nix configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    emacsOverlay.url = "github:nix-community/emacs-overlay";
    emacsOverlay.inputs.nixpkgs.follows = "nixpkgs";
    flakeUtils.url = "github:numtide/flake-utils";

    # Everything below here: lisp packages (format then sort alphabetically via M-x sort-lines).
    ace-window.flake = false;
    ace-window.url = "github:abo-abo/ace-window";
    adaptive-wrap.flake = false;
    adaptive-wrap.url = "github:emacsmirror/adaptive-wrap";
    aio.flake = false;
    aio.url = "github:skeeto/emacs-aio";
    alert.flake = false;
    alert.url = "github:jwiegley/alert";
    all-the-icons.flake = false;
    all-the-icons.url = "github:domtronn/all-the-icons.el";
    async.flake = false;
    async.url = "github:jwiegley/emacs-async";
    avy.flake = false;
    avy.url = "github:abo-abo/avy";
    buttercup.flake = false;
    buttercup.url = "github:jorgenschaefer/emacs-buttercup";
    circe.flake = false;
    circe.url = "github:jorgenschaefer/circe";
    closql.flake = false;
    closql.url = "github:emacscollective/closql";
    consult-flycheck.flake = false;
    consult-flycheck.url = "github:minad/consult-flycheck";
    consult-projectile.flake = false;
    consult-projectile.url = "gitlab:OlMon/consult-projectile";
    consult.flake = false;
    consult.url = "github:minad/consult";
    consult-yasnippet.flake = false;
    consult-yasnippet.url = "github:mohkale/consult-yasnippet";
    corfu-doc.flake = false;
    corfu-doc.url = "github:galeo/corfu-doc";
    corfu.flake = false;
    corfu.url = "github:minad/corfu";
    dash.flake = false;
    dash.url = "github:magnars/dash.el";
    deferred.flake = false;
    deferred.url = "github:kiwanami/emacs-deferred";
    diff-hl.flake = false;
    diff-hl.url = "github:dgutov/diff-hl";
    diminish.flake = false;
    diminish.url = "github:myrjola/diminish.el";
    docker.flake = false;
    docker.url = "github:Silex/docker.el";
    dockerfile-mode.flake = false;
    dockerfile-mode.url = "github:spotify/dockerfile-mode";
    doct.flake = false;
    doct.url = "github:progfolio/doct";
    dumb-jump.flake = false;
    dumb-jump.url = "github:jacktasia/dumb-jump";
    emacsql.flake = false;
    emacsql.url = "github:skeeto/emacsql";
    embark.flake = false;
    embark.url = "github:oantolin/embark";
    emojify.flake = false;
    emojify.url = "github:iqbalansari/emacs-emojify";
    epl.flake = false;
    epl.url = "github:cask/epl";
    # TODO waiting for PR: https://github.com/agzam/exwm-edit/pull/23
    exwm-edit.flake = false;
    exwm-edit.url = "github:chasecaleb/exwm-edit?ref=fix-utf8";
    exwm.flake = false;
    exwm.url = "github:ch11ng/exwm";
    f.flake = false;
    f.url = "github:rejeep/f.el";
    fancy-battery.flake = false;
    fancy-battery.url = "github:emacsorphanage/fancy-battery";
    flycheck.flake = false;
    flycheck.url = "github:flycheck/flycheck";
    forge.flake = false;
    forge.url = "github:magit/forge";
    ghub.flake = false;
    ghub.url = "github:magit/ghub";
    git-link.flake = false;
    git-link.url = "github:sshaw/git-link";
    git-modes.flake = false;
    git-modes.url = "github:magit/git-modes";
    git-timemachine.flake = false;
    git-timemachine.url = "gitlab:pidu/git-timemachine";
    go-mode.flake = false;
    go-mode.url = "github:dominikh/go-mode.el";
    hcl-mode.flake = false;
    hcl-mode.url = "github:purcell/emacs-hcl-mode";
    hl-todo.flake = false;
    hl-todo.url = "github:tarsius/hl-todo";
    ht.flake = false;
    ht.url = "github:Wilfred/ht.el";
    hydra.flake = false;
    hydra.url = "github:abo-abo/hydra";
    iter2.flake = false;
    iter2.url = "github:doublep/iter2";
    json-mode.flake = false;
    json-mode.url = "github:joshwnj/json-mode";
    json-snatcher.flake = false;
    json-snatcher.url = "github:Sterlingg/json-snatcher";
    kind-icon.flake = false;
    kind-icon.url = "github:jdtsmith/kind-icon";
    kotlin-mode.flake = false;
    kotlin-mode.url = "github:Emacs-Kotlin-Mode-Maintainers/kotlin-mode";
    kubel.flake = false;
    kubel.url = "github:abrochard/kubel";
    let-alist.flake = false;
    let-alist.url = "github:emacsmirror/let-alist";
    lsp-mode.flake = false;
    lsp-mode.url = "github:emacs-lsp/lsp-mode";
    lsp-ui.flake = false;
    lsp-ui.url = "github:emacs-lsp/lsp-ui";
    magit-delta.flake = false;
    magit-delta.url = "github:dandavison/magit-delta";
    magit.flake = false;
    magit.url = "github:magit/magit";
    marginalia.flake = false;
    marginalia.url = "github:minad/marginalia";
    markdown-mode.flake = false;
    markdown-mode.url = "github:jrblevin/markdown-mode";
    memoize.flake = false;
    memoize.url = "github:skeeto/emacs-memoize";
    nix-mode.flake = false;
    nix-mode.url = "github:nixos/nix-mode";
    nvm.flake = false;
    nvm.url = "github:rejeep/nvm.el";
    ob-async.flake = false;
    ob-async.url = "github:astahlman/ob-async";
    ob-http.flake = false;
    ob-http.url = "github:zweifisch/ob-http";
    ob-kubectl.flake = false;
    ob-kubectl.url = "github:ifitzpat/ob-kubectl";
    orderless.flake = false;
    orderless.url = "github:oantolin/orderless";
    org-contrib.flake = false;
    org-contrib.url = "git+https://git.sr.ht/~bzg/org-contrib";
    org-journal.flake = false;
    org-journal.url = "github:bastibe/org-journal";
    org-noter.flake = false;
    org-noter.url = "github:weirdNox/org-noter";
    org-pdftools.flake = false;
    org-pdftools.url = "github:fuxialexander/org-pdftools";
    org-roam.flake = false;
    org-roam.url = "github:jethrokuan/org-roam";
    org-super-agenda.flake = false;
    org-super-agenda.url = "github:alphapapa/org-super-agenda";
    org.flake = false;
    org.url = "git+https://git.savannah.gnu.org/git/emacs/org-mode?ref=bugfix";
    orgit.flake = false;
    orgit.url = "github:magit/orgit";
    ov.flake = false;
    ov.url = "github:emacsorphanage/ov";
    page-break-lines.flake = false;
    page-break-lines.url = "github:purcell/page-break-lines";
    pdf-tools.flake = false;
    pdf-tools.url = "github:vedang/pdf-tools";
    peg.flake = false;
    peg.url = "github:emacsmirror/peg";
    pkg-info.flake = false;
    pkg-info.url = "github:emacsorphanage/pkg-info";
    popup-el.flake = false;
    popup-el.url = "github:auto-complete/popup-el";
    powerline.flake = false;
    powerline.url = "github:milkypostman/powerline";
    prettier.flake = false;
    prettier.url = "github:jscheid/prettier.el?ref=release";
    projectile.flake = false;
    projectile.url = "github:bbatsov/projectile";
    pulsar.flake = false;
    pulsar.url = "gitlab:protesilaos/pulsar";
    rainbow-delimiters.flake = false;
    rainbow-delimiters.url = "github:Fanael/rainbow-delimiters";
    request.flake = false;
    request.url = "github:tkf/emacs-request";
    s.flake = false;
    s.url = "github:magnars/s.el";
    slack.flake = false;
    slack.url = "github:yuya373/emacs-slack";
    smartparens.flake = false;
    smartparens.url = "github:Fuco1/smartparens";
    spaceline.flake = false;
    spaceline.url = "github:TheBB/spaceline";
    spacemacs-theme.flake = false;
    spacemacs-theme.url = "github:nashamri/spacemacs-theme";
    spinner.flake = false;
    spinner.url = "github:Malabarba/spinner.el";
    svg-lib.flake = false;
    svg-lib.url = "github:rougier/svg-lib";
    symbol-overlay.flake = false;
    symbol-overlay.url = "github:wolray/symbol-overlay";
    tablist.flake = false;
    tablist.url = "github:politza/tablist";
    terraform-mode.flake = false;
    terraform-mode.url = "github:emacsorphanage/terraform-mode";
    todoist.flake = false;
    todoist.url = "github:abrochard/emacs-todoist";
    transient.flake = false;
    transient.url = "github:magit/transient";
    transpose-frame.flake = false;
    transpose-frame.url = "github:emacsorphanage/transpose-frame";
    treepy.flake = false;
    treepy.url = "github:volrath/treepy.el";
    ts.flake = false;
    ts.url = "github:alphapapa/ts.el";
    typescript.flake = false;
    typescript.url = "github:emacs-typescript/typescript.el";
    use-package.flake = false;
    use-package.url = "github:jwiegley/use-package";
    vertico.flake = false;
    vertico.url = "github:minad/vertico";
    visual-regexp.flake = false;
    visual-regexp.url = "github:benma/visual-regexp.el";
    vterm.flake = false;
    vterm.url = "github:akermu/emacs-libvterm";
    web-mode.flake = false;
    web-mode.url = "github:fxbois/web-mode";
    websocket.flake = false;
    websocket.url = "github:ahyatt/emacs-websocket";
    wgrep.flake = false;
    wgrep.url = "github:mhayashi1120/Emacs-wgrep";
    with-editor.flake = false;
    with-editor.url = "github:magit/with-editor";
    xelb.flake = false;
    xelb.url = "github:ch11ng/xelb";
    xterm-color.flake = false;
    xterm-color.url = "github:atomontage/xterm-color";
    yaml-mode.flake = false;
    yaml-mode.url = "github:yoshiki/yaml-mode";
    yaml.flake = false;
    yaml.url = "github:zkry/yaml.el";
    yasnippet.flake = false;
    yasnippet.url = "github:joaotavora/yasnippet";
  };

  outputs = { self, nixpkgs, flakeUtils, emacsOverlay, ... }@inputs:
    flakeUtils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          system = system;
          overlays = [ emacsOverlay.overlay ];
        };
        emacs = (pkgs.emacsGcc.overrideAttrs (attrs: {
          # Strip out built-in org to make sure it isn't accidentally loaded. Unfortunately
          # modifying this means I don't get to benefit from public binary cache for Emacs... but
          # the emacs-overlay derivations aren't in the main NixOS binary cache so that isn't
          # relevant.
          postInstall = (attrs.postInstall or "") + ''
            rm -r $out/share/emacs/${attrs.version}/lisp/org
          '';
          # Override site-start.el to fix load path. https://github.com/NixOS/nixpkgs/pull/161445
        })).override { siteStart = ./nix/site-start.el; };

        buildPackage = import ./nix/build-package.nix { inherit pkgs emacs; };

        libraries = import ./nix/libraries.nix { inherit pkgs emacs inputs; };

        init = import ./nix/init.nix { inherit pkgs emacs libraries; };

      in
      rec {
        packages = {
          inherit emacs;
          default = init.cc-init;
        } // libraries // init;

        nixosModule = { ... }: {
          environment.systemPackages = (pkgs.lib.attrValues packages) ++ [
            pkgs.pandoc # For markdown-mode
            pkgs.nodePackages.prettier
            pkgs.ripgrep # For consult, projectile, etc.
          ];
          services.xserver.displayManager.session = [{
            manage = "window";
            name = "emacs";
            start = ''
              xsetroot -cursor_name left_ptr
              export ENABLE_EXWM=true
              ${packages.emacs}/bin/emacs &
              waitPID=$!
            '';
          }];
        };
      });
}
