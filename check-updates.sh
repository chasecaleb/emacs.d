#!/usr/bin/env bash
# Update emacs.d flake inputs. This is a standalone script because I don't want to put my GitHub
# token in /etc/nix/nix.conf. The token has zero privileges and exists just to avoid API rate
# limiting, but still.

set -Eeuo pipefail
die() { echo -e "$0" ERROR: "$@" >&2; exit 1; }
export -f die
# shellcheck disable=2154
trap 's=$?; die "line $LINENO - $BASH_COMMAND"; exit $s' ERR

get_input_nodes() {
    nix flake metadata --json \
        | jq --compact-output --exit-status '.locks.nodes | to_entries'
}

get_input() {
    # shellcheck disable=SC2016 # $name is a jq arg.
    jq_get --arg name "$1" '.[] | select(.key == $name) | .value'
}
export -f get_input

jq_get() {
    jq --exit-status --raw-output "$@"
}
export -f jq_get

fetch_package() {
    name=$1
    new_input=$(get_input "$name" <<< "$new_inputs")

    type=$(jq_get ".locked.type" <<< "$new_input")
    if [[ "$type" == "github" || "$type" == "gitlab" ]]; then
        owner=$(jq_get ".locked.owner" <<< "$new_input")
        repo_name=$(jq_get ".locked.repo" <<< "$new_input")

        url="git@$type.com:$owner/$repo_name"
    elif [[ "$type" == "git" ]]; then
        url=$(jq_get ".locked.url" <<< "$new_input")
    else
        die "Unexpected type $type: $new_input"
    fi

    # Checkout location based on flake input name, *not* repo name. Barring unlikely-but-possible
    # edge cases such as multiple flake inputs with the same repo name, either one would work just
    # fine. However, basing it on the flake name matches how I use them in Nix and where they get
    # packaged/installed.
    checkout_dir=$destination_all/$name
    echo ""
    if [[ ! -e "$checkout_dir" ]]; then
        echo "Cloning:  $checkout_dir"
        git clone "$url" "$checkout_dir"
    elif [[ "$do_fetch" == "true" ]]; then
        echo "Fetching: $checkout_dir"
        (cd "$checkout_dir" && git fetch)
    fi

    new_revision=$(jq_get ".locked.rev" <<< "$new_input")
    old_revision=$(get_input "$name" <<< "$old_inputs" | jq_get ".locked.rev")
    (
        cd "$checkout_dir"
        # If branch isn't specified in flake lock file, then use the default.
        if ! branch_ref=$(jq_get '.locked.ref' <<< "$new_input"); then
            branch_ref=$(git rev-parse --abbrev-ref origin/HEAD | sed 's%origin/%%')
        fi
        git checkout "$branch_ref"
        git reset --hard "$old_revision"
        # Marker branch in case origin/HEAD doesn't match the flake lock file revision. This could
        # potentially happen if new commits are pushed in the brief period between updating the lock
        # file and cloning/fetching the repo. Or more likely, it couuld happen if I mess with the
        # repo myself (e.g. while working on a PR).
        git branch --force "cc/new" "$new_revision"
    )
    if [[ "$new_revision" != "$old_revision" ]]; then
        ln -s "$checkout_dir" "$destination_updates/$name"
    fi
}
export -f fetch_package

main() {
    if [[ -e "$destination_updates" ]]; then
        # Recreate from scratch so that only current updates are included.
        rm -rf "$destination_updates"
    fi
    mkdir -p "$destination_all" "$destination_updates"
    export destination_all destination_updates

    cd ~/code/emacs.d
    if [[ "$(git status --porcelain flake.lock)" ]]; then
        git stash push --quiet --message "[auto-created] Emacs updates WIP" -- flake.lock
        old_inputs=$(get_input_nodes)
        git stash pop --quiet
    else
        old_inputs=$(get_input_nodes)
    fi

    if [[ "$do_fetch" == "true" ]]; then
        token=$(pass tokens/api.github.com/nix-unprivileged)
        nix flake lock --recreate-lock-file --option access-tokens "github.com=$token"
    fi
    new_inputs=$(get_input_nodes)
    export old_inputs new_inputs

    # Exclude nixpkgs because it's *enormous* and I don't care about changes to it in the context of my
    # emacs.d repo, since it gets overriden by my nixos-config's version of nixpkgs.
    input_names=$(jq_get '.[] | select(.key == "root") | .value.inputs | values[]' <<< "$new_inputs" \
        | grep -v "nixpkgs")
    parallel --bar fetch_package <<< "$input_names"
}

destination=~/emacs-sources
destination_all=$destination/all
destination_updates=$destination/updates

do_fetch=""
if [[ "${1:-}" == "fetch" ]]; then
    do_fetch="true"
elif [[ "${1:-}" != "view" ]]; then
    die "Invalid argument. \$1 must be fetch or view"
fi
export do_fetch
main

read -r -d '' lisp_cmd << EOF || true
(let* ((update-dirs '(("$destination_updates" . 1)))
       (magit-repository-directories update-dirs))
  (with-current-buffer (magit-list-repositories)
    (setq-local magit-repository-directories update-dirs)
    ;; Make sure correct repos are shown for uh... reasons. Not sure why but the let-binding above
    ;; doesn't seem to work consistently.
    (revert-buffer)))
EOF
emacsclient --eval "$lisp_cmd"
