# My Emacs Config

Built for [https://nixos.org/](NixOS) and used with my
[nixos-config](https://gitlab.com/chasecaleb/nixos-config/) project.
